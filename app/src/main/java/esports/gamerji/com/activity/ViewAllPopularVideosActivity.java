package esports.gamerji.com.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import esports.gamerji.com.R;
import esports.gamerji.com.adapter.ViewAllPopularVideosAdapter;
import esports.gamerji.com.model.ViewAllPopularVideosModel;
import esports.gamerji.com.utils.AppConstants;
import esports.gamerji.com.utils.Common;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.SessionManager;
import esports.gamerji.com.webServices.APIClient;
import esports.gamerji.com.webServices.APIInterface;
import esports.gamerji.com.webServices.WebFields;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllPopularVideosActivity extends AppCompatActivity {

    private RelativeLayout mRelativeMain;
    private ImageView mImageBack;
    private Button mButtonSubscribe;
    private TextView mTextChannelName, mTextTotalVideosCount;
    private SwipeRefreshLayout mSwipeRefreshView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private ArrayList<ViewAllPopularVideosModel.Data.VideosData> mArrViewAllPopularVideos;
    private ViewAllPopularVideosAdapter mAdapterViewAllPopularVideos;
    private SessionManager mSessionManager;
    private String mChannelId, mChannelURL;

    // Load More Listener Variables
    private int pageIndex = 10, page = 1, pastVisibleItems, visibleItemCount, totalItemCount;
    private boolean isLastPage = false, isLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_popular_videos);
        mSessionManager = new SessionManager();
        mArrViewAllPopularVideos = new ArrayList<>();
        mLayoutManager = new GridLayoutManager(ViewAllPopularVideosActivity.this, 2);

        getBundle();
        getIds();
        setRegListeners();
        setAdapterData();
        callToVideosAPI(page);
    }

    /**
     * Get the keys from the Popular Videos Activity
     */
    private void getBundle() {
        try {
            mChannelId = getIntent().getStringExtra(AppConstants.BUNDLE_VIEW_ALL_VIDEOS_CHANNEL_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layout
            mRelativeMain = findViewById(R.id.relative_view_all_popular_videos_main_view);

            // Buttons
            mButtonSubscribe = findViewById(R.id.button_view_all_popular_videos_channel_subscribe);

            // Image View
            mImageBack = findViewById(R.id.image_view_all_popular_videos_back);

            // Text View
            mTextChannelName = findViewById(R.id.text_view_all_popular_videos_channel_name);
            mTextTotalVideosCount = findViewById(R.id.text_view_all_popular_videos_total_count);

            // Swipe Refresh View
            mSwipeRefreshView = findViewById(R.id.swipe_refresh_view_all_popular_videos);

            // Recycler View
            mRecyclerView = findViewById(R.id.recycler_view_all_popular_videos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mImageBack.setOnClickListener(clickListener);
            mButtonSubscribe.setOnClickListener(clickListener);

            // ToDo: Swipe Refresh Click Listener
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_view_all_popular_videos_back:
                    finish();
                    break;

                case R.id.button_view_all_popular_videos_channel_subscribe:
                    redirectToYoutubeSubscribe();
                    break;
            }
        }
    };

    /**
     * This method redirects you to the Youtube Channel Subscribe Page
     */
    private void redirectToYoutubeSubscribe() {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mChannelURL));
            intent.setPackage("com.google.android.youtube");
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            page = 1;
            isLastPage = false;
            if (mArrViewAllPopularVideos != null)
                mArrViewAllPopularVideos.clear();
            if (mAdapterViewAllPopularVideos != null)
                mAdapterViewAllPopularVideos.notifyDataSetChanged();
            callToVideosAPI(page);
        }
    };

    /**
     * This should get all the videos of the Influencer as well as Host
     */
    private void callToVideosAPI(int page) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ViewAllPopularVideosActivity.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            String mUserId = mSessionManager.getUserId(ViewAllPopularVideosActivity.this);

            Call<ViewAllPopularVideosModel> callRepos = APIClient.getClient().create(APIInterface.class).getViewAllPopularVideos(WebFields.GET_VIEW_ALL_POPULAR_VIDEOS.MODE, mUserId, mChannelId,
                    Constants.VIDEOS_POPULAR, String.valueOf(page), String.valueOf(pageIndex));
            callRepos.enqueue(new Callback<ViewAllPopularVideosModel>() {
                @Override
                public void onResponse(@NonNull Call<ViewAllPopularVideosModel> call, @NonNull Response<ViewAllPopularVideosModel> response) {
                    isLoading = false;
                    mSwipeRefreshView.setRefreshing(false);
                    progressDialog.dismiss();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);

                        String mStatus = jsonObject.getString(WebFields.STATUS);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);

                        if (mStatus.equalsIgnoreCase(Constants.API_SUCCESS)) {

                            setsUI(response);
                            ArrayList<ViewAllPopularVideosModel.Data.VideosData> videos = (ArrayList<ViewAllPopularVideosModel.Data.VideosData>) response.body().getData().getVideosData();

                            if (response.body().getData().getVideosCount() > 0) {
                                if (mArrViewAllPopularVideos != null && mArrViewAllPopularVideos.size() > 0 &&
                                        mAdapterViewAllPopularVideos != null) {
                                    mArrViewAllPopularVideos.addAll(videos);
                                    mAdapterViewAllPopularVideos.notifyDataSetChanged();
                                } else {
                                    mArrViewAllPopularVideos = videos;
                                    setAdapterData();
                                    setLoadMoreClickListener();
                                }
                            } else {
                                isLastPage = true;
                            }
                        } else {
                            Constants.SnakeMessageYellow(mRelativeMain, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ViewAllPopularVideosModel> call, @NonNull Throwable t) {
                    mSwipeRefreshView.setRefreshing(false);
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setsUI(Response<ViewAllPopularVideosModel> response) {
        try {
            mTextChannelName.setSelected(true);
            mTextChannelName.setText(response.body().getData().getYTChannelName());
            mTextTotalVideosCount.setText(response.body().getData().getVideosTotalCount() > 1 ?
                    response.body().getData().getVideosTotalCount() + " " + getResources().getString(R.string.text_videos) :
                    response.body().getData().getVideosTotalCount() + " " + getResources().getString(R.string.text_video));
            mChannelURL = response.body().getData().getYTChannelLink();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());

            mAdapterViewAllPopularVideos = new ViewAllPopularVideosAdapter(ViewAllPopularVideosActivity.this, mArrViewAllPopularVideos);
            mRecyclerView.setAdapter(mAdapterViewAllPopularVideos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!isLastPage && !isLoading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            isLoading = true;
                            Common.insertLog("Last Item Wow !");
                            callToVideosAPI(++page);
                        }
                    }
                }
            }
        });
    }
}
