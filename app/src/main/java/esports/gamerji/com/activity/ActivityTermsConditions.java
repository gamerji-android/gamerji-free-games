package esports.gamerji.com.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;

import esports.gamerji.com.R;
import esports.gamerji.com.model.TCModel;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.webServices.APIClient;
import esports.gamerji.com.webServices.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityTermsConditions extends AppCompatActivity {
    TextView txt_text, txt_title;
    APIInterface apiInterface;
    String from;
    RelativeLayout parent_layout;
    ImageView img_back;
    AdView adView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activiy_terms_and_conditions);
        adView = new AdView(this, getString(R.string.fb_placement_id), AdSize.BANNER_HEIGHT_50);
        LinearLayout adContainer = findViewById(R.id.banner_container);
        adContainer.addView(adView);
        adView.loadAd();
        from = getIntent().getStringExtra("from");
        txt_text = findViewById(R.id.txt_text);
        txt_title = findViewById(R.id.txt_title);
        parent_layout = findViewById(R.id.parent_layout);
        img_back = findViewById(R.id.img_back);
        txt_title.setText(from);
        txt_text.setMovementMethod(new ScrollingMovementMethod());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if (from.equalsIgnoreCase("Terms and Conditions"))
            getData(Constants.GET_TERMS);
        else
            getData(Constants.GET_PRIVACY);
        img_back.setOnClickListener(v -> finish());

        adView.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {

            }

            @Override
            public void onAdLoaded(Ad ad) {
                adContainer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });
    }

    private void getData(String getPrivacy) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<TCModel> tcModelCall;

        tcModelCall = apiInterface.getTerms(getPrivacy);

        tcModelCall.enqueue(new Callback<TCModel>() {
            @Override
            public void onResponse(@NonNull Call<TCModel> call, @NonNull Response<TCModel> response) {
                progressDialog.dismiss();
                TCModel tcModel = response.body();
                assert tcModel != null;
                if (tcModel.status.equalsIgnoreCase("success")) {
                    Spanned htmlAsSpanned = Html.fromHtml(tcModel.data);
                    txt_text.setText(htmlAsSpanned);
                } else {
                    Constants.SnakeMessageYellow(parent_layout, tcModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TCModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroy() {
        System.gc();
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }
}