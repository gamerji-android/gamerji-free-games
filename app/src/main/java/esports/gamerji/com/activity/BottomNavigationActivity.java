package esports.gamerji.com.activity;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import esports.gamerji.com.R;
import esports.gamerji.com.fragments.HomeFragment;
import esports.gamerji.com.fragments.LeaderBoardFragment;
import esports.gamerji.com.fragments.MoreFragment;
import esports.gamerji.com.fragments.ProfileFragment;
import esports.gamerji.com.fragments.VideosFragment;
import esports.gamerji.com.interfaces.ProfileGameInfoClickListener;
import esports.gamerji.com.model.GamerProfileModel;
import esports.gamerji.com.utils.Common;

public class BottomNavigationActivity extends AppCompatActivity implements ProfileGameInfoClickListener {

    private static Fragment fragment;
    private LinearLayout mLinearHome, mLinearVideos, mLinearProfile, mLinearLeaderBoard, mLinearMore, mLinearBottomAds;
    public static ViewPager mViewPagerSliderAds;
    private TextView mTextHome, mTextVideos, mTextLeaderBoard, mTextMore;
    private ImageView mImageHome, mImageVideos, mImageProfile, mImageLeaderBoard, mImageMore, mImageBottomSheetHowToClose,
            mImageBottomSheetProfileBadgeClose, mImageBottomSheetProfileBadge;
    public static int currentTabPosition = 1;
    private long mBackPressed;
    private String mTabName;
    private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
    private View mViewBottomSheetBackground;
    private BottomSheetBehavior mBottomSheetHowTo, mBottomSheetProfileBadges;
    private TextView mTextBottomSheetProfileBadgeMessage, mTextBottomSheetProfileBadgePoints;
    private AdView mAdView;
    private LinearLayout mLinearAdContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);

        getIds();
        setAdView();
        setRegListeners();
        setData();
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Linear Layouts
            mLinearHome = findViewById(R.id.linear_tab_home);
            mLinearVideos = findViewById(R.id.linear_tab_videos);
            mLinearProfile = findViewById(R.id.linear_tab_profile);
            mLinearLeaderBoard = findViewById(R.id.linear_tab_leader_board);
            mLinearMore = findViewById(R.id.linear_tab_more);
            mLinearBottomAds = findViewById(R.id.linear_bottom_ads);
            mLinearAdContainer = findViewById(R.id.linear_bottom_ads_container);

            // View Pager
            mViewPagerSliderAds = findViewById(R.id.slider_pager_ads);

            // Text Views
            mTextHome = findViewById(R.id.text_tab_home);
            mTextVideos = findViewById(R.id.text_tab_videos);
            mTextLeaderBoard = findViewById(R.id.text_tab_leader_board);
            mTextMore = findViewById(R.id.text_tab_more);
            mTextBottomSheetProfileBadgeMessage = findViewById(R.id.txt_badge_msg);
            mTextBottomSheetProfileBadgePoints = findViewById(R.id.txt_points);

            // Image Views
            mImageHome = findViewById(R.id.image_tab_home);
            mImageVideos = findViewById(R.id.image_tab_videos);
            mImageProfile = findViewById(R.id.image_tab_profile);
            mImageLeaderBoard = findViewById(R.id.image_tab_leader_board);
            mImageMore = findViewById(R.id.image_tab_more);
            mImageBottomSheetHowToClose = findViewById(R.id.img_close);
            mImageBottomSheetProfileBadgeClose = findViewById(R.id.img_close_badges);
            mImageBottomSheetProfileBadge = findViewById(R.id.img_badge);

            // View
            mViewBottomSheetBackground = findViewById(R.id.view_my_contests_background);

            // Relative Layouts
            RelativeLayout mRelativeBottomSheetHowTo = findViewById(R.id.bottom_how_to);
            RelativeLayout mRelativeBottomSheetProfileBadge = findViewById(R.id.bottom_badge);

            // Bottom Sheet Behaviours
            mBottomSheetHowTo = BottomSheetBehavior.from(mRelativeBottomSheetHowTo);
            mBottomSheetProfileBadges = BottomSheetBehavior.from(mRelativeBottomSheetProfileBadge);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the Ad View
     */
    private void setAdView() {
        try {
            mAdView = new AdView(BottomNavigationActivity.this, getResources().getString(R.string.fb_placement_id), AdSize.BANNER_HEIGHT_50);
            mLinearAdContainer.addView(mAdView);
            mAdView.loadAd();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mLinearHome.setOnClickListener(clickListener);
            mLinearVideos.setOnClickListener(clickListener);
            mLinearProfile.setOnClickListener(clickListener);
            mLinearLeaderBoard.setOnClickListener(clickListener);
            mLinearMore.setOnClickListener(clickListener);
            mImageProfile.setOnClickListener(clickListener);
            mImageBottomSheetHowToClose.setOnClickListener(clickListener);
            mImageBottomSheetProfileBadgeClose.setOnClickListener(clickListener);

            // ToDo: Facebook Ad Listener
            mAdView.setAdListener(adListener);

            // ToDo: How To Bottom Sheet Call Back
            mBottomSheetHowTo.setBottomSheetCallback(howToBottomSheetCallback);
            mBottomSheetProfileBadges.setBottomSheetCallback(profileBadgesBottomSheetCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            mTabName = getResources().getString(R.string.menu_home);
            currentTabPosition = 1;
            setTab();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.linear_tab_home:
                    currentTabPosition = 1;
                    setTab();
                    break;

                case R.id.linear_tab_videos:
                    currentTabPosition = 2;
                    setTab();
                    break;

                case R.id.linear_tab_profile:
                    currentTabPosition = 3;
                    setTab();
                    break;

                case R.id.linear_tab_leader_board:
                    currentTabPosition = 4;
                    setTab();
                    break;

                case R.id.linear_tab_more:
                    currentTabPosition = 5;
                    setTab();
                    break;

                case R.id.image_tab_profile:
                    currentTabPosition = 3;
                    setTab();
                    break;

                case R.id.img_close:
                    mBottomSheetHowTo.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    mViewBottomSheetBackground.setVisibility(View.GONE);
                    break;

                case R.id.img_close_badges:
                    mBottomSheetProfileBadges.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    mViewBottomSheetBackground.setVisibility(View.GONE);
                    break;
            }
        }
    };

    /**
     * Call back listener (How To)
     */
    BottomSheetBehavior.BottomSheetCallback howToBottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            switch (newState) {
                case BottomSheetBehavior.STATE_HIDDEN:
                    break;

                case BottomSheetBehavior.STATE_EXPANDED:
                    mViewBottomSheetBackground.setVisibility(View.VISIBLE);
                    break;

                case BottomSheetBehavior.STATE_COLLAPSED:
                    mViewBottomSheetBackground.setVisibility(View.GONE);
                    break;

                case BottomSheetBehavior.STATE_DRAGGING:
                    break;

                case BottomSheetBehavior.STATE_SETTLING:
                    break;

            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    /**
     * Call back listener (Profile Badges)
     */
    BottomSheetBehavior.BottomSheetCallback profileBadgesBottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            switch (newState) {
                case BottomSheetBehavior.STATE_HIDDEN:
                    break;

                case BottomSheetBehavior.STATE_COLLAPSED:
                    mViewBottomSheetBackground.setVisibility(View.GONE);
                    break;

                case BottomSheetBehavior.STATE_EXPANDED:
                    mViewBottomSheetBackground.setVisibility(View.VISIBLE);
                    mViewBottomSheetBackground.bringToFront();
                    break;

                case BottomSheetBehavior.STATE_DRAGGING:
                    break;
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    /**
     * Facebook Ad listeners
     */
    private AdListener adListener = new AdListener() {
        @Override
        public void onError(Ad ad, AdError adError) {

        }

        @Override
        public void onAdLoaded(Ad ad) {
            mLinearAdContainer.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAdClicked(Ad ad) {

        }

        @Override
        public void onLoggingImpression(Ad ad) {

        }
    };

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    /**
     * Sets the tab
     */
    private void setTab() {
        switch (currentTabPosition) {
            case 1:
                setHomeTab();
                break;

            case 2:
                setVideosTab();
                break;

            case 3:
                setProfileTab();
                break;

            case 4:
                setLeaderBoardTab();
                break;

            case 5:
                setMoreTab();
                break;
        }
    }

    /**
     * Sets the Home tab data
     */
    private void setHomeTab() {
        try {
            setDefaultTabs();
            mLinearBottomAds.setVisibility(View.VISIBLE);
            setTabColor(getResources().getDrawable(R.drawable.ic_selected_home), mImageHome);
            setTextColor(mTextHome);
            fragment = new HomeFragment();
            mTabName = getResources().getString(R.string.menu_home);
            changeFragment(fragment, mTabName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the Videos tab data
     */
    private void setVideosTab() {
        try {
            setDefaultTabs();
            mLinearBottomAds.setVisibility(View.GONE);
            setTabColor(getResources().getDrawable(R.drawable.ic_video_selected), mImageVideos);
            setTextColor(mTextVideos);
            fragment = new VideosFragment();
            mTabName = getResources().getString(R.string.menu_videos);
            changeFragment(fragment, mTabName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the Profile tab data
     */
    private void setProfileTab() {
        try {
            setDefaultTabs();
            mLinearBottomAds.setVisibility(View.VISIBLE);
            fragment = new ProfileFragment();
            mTabName = getResources().getString(R.string.menu_profile);
            changeFragment(fragment, mTabName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the leader board tab data
     */
    private void setLeaderBoardTab() {
        try {
            setDefaultTabs();
            mLinearBottomAds.setVisibility(View.VISIBLE);
            setTabColor(getResources().getDrawable(R.drawable.ic_leaderboard_new_selected), mImageLeaderBoard);
            setTextColor(mTextLeaderBoard);
            fragment = new LeaderBoardFragment();
            mTabName = getResources().getString(R.string.menu_leader_board);
            changeFragment(fragment, mTabName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the more tab data
     */
    private void setMoreTab() {
        try {
            setDefaultTabs();
            mLinearBottomAds.setVisibility(View.VISIBLE);
            setTabColor(getResources().getDrawable(R.drawable.ic_selected_more), mImageMore);
            setTextColor(mTextMore);
            fragment = new MoreFragment();
            mTabName = getResources().getString(R.string.menu_more);
            changeFragment(fragment, mTabName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the tab image and text color as per the layout clicked
     */
    private void setTabColor(Drawable image, ImageView mImageView) {
        mImageView.setBackground(image);
    }

    /**
     * Sets the tab image and text color as per the layout clicked
     */
    private void setTextColor(TextView mTextView) {
        mTextView.setTextColor(getResources().getColor(R.color.bg_text_color));
    }

    /**
     * Change the fragment
     *
     * @param fragment - Fragment
     * @param title    - Title
     */
    public void changeFragment(Fragment fragment, String title) {
        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        Fragment currentFragment = mFragmentManager.getPrimaryNavigationFragment();
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment);
        }

        Fragment fragmentTemp = mFragmentManager.findFragmentByTag(title);
        if (fragmentTemp == null) {
            fragmentTemp = fragment;
            fragmentTransaction.add(R.id.frame_container, fragmentTemp, title);
        } else {
            fragmentTransaction.show(fragmentTemp);
        }

        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp);
        fragmentTransaction.setReorderingAllowed(true);
        fragmentTransaction.commitNowAllowingStateLoss();
    }

    /**
     * Sets the default tab
     */
    private void setDefaultTabs() {
        try {
            // Default Image Background Image
            mImageHome.setBackground(getResources().getDrawable(R.drawable.ic_noun_home));
            mImageVideos.setBackground(getResources().getDrawable(R.drawable.ic_video));
            mImageLeaderBoard.setBackground(getResources().getDrawable(R.drawable.ic_leaderboard_new));
            mImageMore.setBackground(getResources().getDrawable(R.drawable.ic_more));

            // Default Text Color
            mTextHome.setTextColor(getResources().getColor(R.color.default_text_color));
            mTextVideos.setTextColor(getResources().getColor(R.color.default_text_color));
            mTextLeaderBoard.setTextColor(getResources().getColor(R.color.default_text_color));
            mTextMore.setTextColor(getResources().getColor(R.color.default_text_color));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (mViewBottomSheetBackground.getVisibility() == View.VISIBLE) {
            mBottomSheetHowTo.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mBottomSheetProfileBadges.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mViewBottomSheetBackground.setVisibility(View.GONE);
        } else {
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed();
                return;
            } else {
                Common.setCustomToast(getApplicationContext(), getString(R.string.text_press_back_again_to_exit));
            }
            mBackPressed = System.currentTimeMillis();
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void OnBadgeClickListener(GamerProfileModel.Data.LevelsData.LevelData badgesList) {
        mTextBottomSheetProfileBadgeMessage.setText(badgesList.EndingPoint);
        mTextBottomSheetProfileBadgePoints.setText("Points  " + badgesList.StartingPoint + "  -  " +
                badgesList.EndingPoint);
        Glide.with(BottomNavigationActivity.this).load(badgesList.FeaturedIcon).into(mImageBottomSheetProfileBadge);
        mBottomSheetProfileBadges.setState(BottomSheetBehavior.STATE_EXPANDED);
    }
}
