package esports.gamerji.com.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.hbb20.CountryCodePicker;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import esports.gamerji.com.BuildConfig;
import esports.gamerji.com.R;
import esports.gamerji.com.model.CustomerCareIssuesModel;
import esports.gamerji.com.model.CustomerIssuesModel;
import esports.gamerji.com.model.GetAllGamesModel;
import esports.gamerji.com.utils.Common;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.CustomWheelView;
import esports.gamerji.com.utils.FileUtils;
import esports.gamerji.com.utils.Pref;
import esports.gamerji.com.utils.datepicker.SpinnerDatePickerDialogBuilder;
import esports.gamerji.com.webServices.APIClient;
import esports.gamerji.com.webServices.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCustomerCare extends AppCompatActivity implements IPickResult {

    APIInterface apiInterface;
    TextView txt_category, txt_sub_category, txt_game_name, txt_game_type, txt_contest_date, txt_contest_time, txt_help;
    CardView cv_sub_cat, cv_game, cv_game_type, cv_contest_date, cv_contest_time;
    EditText et_name, et_phn, et_email, et_issue;
    CountryCodePicker ccp;
    CardView cv_ss, proceed;
    LinearLayout mLinearCCeRootView;
    PickResult pickResult;
    ImageView img_back;
    TextView txt_ss;
    CardView cv_name, cv_phone, cv_email;
    ArrayList<CustomerCareIssuesModel> customerCareIssuesModels = new ArrayList<>();
    ArrayList<CustomerIssuesModel.Data.IssuesData> issuesDataArrayList = new ArrayList();
    String game_id = "", date = "", game_type_id = "", contest_id = "", Val_Issue = "", Val_Subissueid = "";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_customer_care);
        AdView adView = new AdView(this, getString(R.string.fb_placement_id), AdSize.BANNER_HEIGHT_50);
        LinearLayout adContainer = findViewById(R.id.banner_container);
        adContainer.addView(adView);
        adView.loadAd();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        txt_category = findViewById(R.id.txt_category);
        txt_help = findViewById(R.id.txt_help);
        txt_sub_category = findViewById(R.id.txt_sub_category);
        txt_game_name = findViewById(R.id.txt_game_name);
        txt_game_type = findViewById(R.id.txt_game_type);
        txt_contest_date = findViewById(R.id.txt_contest_date);
        txt_contest_time = findViewById(R.id.txt_contest_time);
        cv_sub_cat = findViewById(R.id.cv_sub_cat);
        cv_game = findViewById(R.id.cv_game);
        cv_game_type = findViewById(R.id.cv_game_type);
        cv_contest_date = findViewById(R.id.cv_contest_date);
        cv_contest_time = findViewById(R.id.cv_contest_time);

        et_phn = findViewById(R.id.et_phn);
        txt_ss = findViewById(R.id.txt_ss);
        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);
        et_issue = findViewById(R.id.et_issue);
        img_back = findViewById(R.id.img_back);
        cv_name = findViewById(R.id.cv_name);
        cv_phone = findViewById(R.id.cv_phone);
        cv_email = findViewById(R.id.cv_email);
        img_back.setOnClickListener(v -> finish());
        ccp = findViewById(R.id.ccp);
        cv_ss = findViewById(R.id.cv_ss);

        et_name.setText(Pref.getValue(this, Constants.firstname, "", Constants.FILENAME) + " " +
                Pref.getValue(this, Constants.lastname, "", Constants.FILENAME));

        et_phn.setText(Pref.getValue(this, Constants.MobileNumber, "", Constants.FILENAME));
        et_email.setText(Pref.getValue(this, Constants.EmailAddress, "", Constants.FILENAME));

        if (!(Pref.getValue(this, Constants.CountryCode, "", Constants.FILENAME).equalsIgnoreCase("")))
            ccp.setCountryForPhoneCode(Integer.parseInt(Pref.getValue(this, Constants.CountryCode, "", Constants.FILENAME)));

        mLinearCCeRootView = findViewById(R.id.mLinearCCeRootView);
        proceed = findViewById(R.id.proceed);


        cv_ss.setOnClickListener(v ->
                PickImageDialog.build(new PickSetup().setPickTypes(EPickType.GALLERY)).show(this));
        proceed.setOnClickListener(v ->
        {
            try {
                validate();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        cv_name.setOnClickListener(v -> FileUtils.requestfoucs(ActivityCustomerCare.this, et_name));

        cv_phone.setOnClickListener(v -> FileUtils.requestfoucs(ActivityCustomerCare.this, et_phn));

        cv_email.setOnClickListener(v -> FileUtils.requestfoucs(ActivityCustomerCare.this, et_email));

        txt_contest_date.setOnClickListener(v ->
        {
            if (game_id.equalsIgnoreCase("")) {
                Constants.SnakeMessageYellow(mLinearCCeRootView, "Select Game First.");
            } else if (game_type_id.equalsIgnoreCase("")) {
                Constants.SnakeMessageYellow(mLinearCCeRootView, "Select Game Type First.");
            } else {
                try {
                    Calendar calendar = Calendar.getInstance();
                    Calendar mMaxDate = Calendar.getInstance();
                    Calendar mMinDate = Calendar.getInstance();
                    mMaxDate.add(Calendar.YEAR, 0);
                    mMaxDate.setTimeInMillis(mMaxDate.getTimeInMillis());
                    mMinDate.set(Calendar.YEAR, 1920);
                    mMinDate.set(Calendar.MONTH, 1);
                    mMinDate.set(Calendar.DATE, 1);

                    int mMaxDateYear = mMaxDate.get(Calendar.YEAR);
                    int mMaxDateMonth = mMaxDate.get(Calendar.MONTH);
                    int mMaxDateDay = mMaxDate.get(Calendar.DAY_OF_MONTH);

                    int mMinDateYear = mMinDate.get(Calendar.YEAR);
                    int mMinDateMonth = mMinDate.get(Calendar.MONTH);
                    int mMinDateDay = mMinDate.get(Calendar.DAY_OF_MONTH);


                    new SpinnerDatePickerDialogBuilder().context(ActivityCustomerCare.this).callback((view, year, monthOfYear, dayOfMonth) ->
                    {
                        txt_contest_date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

                        if ((monthOfYear + 1) < 10) {
                            date = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;
                        } else {
                            date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }

                    }).spinnerTheme(R.style.NumberPickerStyle)
                            .showTitle(true).showDaySpinner(true).defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                            .maxDate(mMaxDateYear, mMaxDateMonth, mMaxDateDay)
                            .minDate(mMinDateYear, mMinDateMonth, mMinDateDay).build().show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        getAllIssues();
        getAllGames();
    }

    private void getAllGames() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityCustomerCare.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GetAllGamesModel> getAllGamesModelCall = apiInterface.getAllGames(Constants.GET_ALL_GAMES, Pref.getValue(ActivityCustomerCare.this, Constants.UserID, "", Constants.FILENAME));

        getAllGamesModelCall.enqueue(new Callback<GetAllGamesModel>() {
            @Override
            public void onResponse(@NonNull Call<GetAllGamesModel> call, @NonNull Response<GetAllGamesModel> response) {
                progressDialog.dismiss();
                GetAllGamesModel getAllGamesModel = response.body();
                assert getAllGamesModel != null;
                if (getAllGamesModel.status.equalsIgnoreCase("success")) {
                    txt_game_name.setOnClickListener(v -> selectGame(getAllGamesModel.GamesData.gamesDataArrayList));
                } else {
                    Constants.SnakeMessageYellow(mLinearCCeRootView, getAllGamesModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetAllGamesModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void selectGame(ArrayList<GetAllGamesModel.Data.GamesData> gamesDataArrayList) {
        List<String> gamesList = new ArrayList<>();
        for (int i = 0; i < gamesDataArrayList.size(); i++) {
            gamesList.add(gamesDataArrayList.get(i).Name);
        }

        Dialog dialog = new Dialog(ActivityCustomerCare.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View outerView = LayoutInflater.from(ActivityCustomerCare.this).inflate(R.layout.wheelview, null);
        dialog.setContentView(outerView);
        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        CustomWheelView wv = outerView.findViewById(R.id.wh_view);
        TextView txt_ok = outerView.findViewById(R.id.txt_ok);
        TextView title = outerView.findViewById(R.id.title);
        title.setText("Select Game");
        TextView txt_cancel = outerView.findViewById(R.id.txt_cancel);
        txt_ok.setOnClickListener(v ->
        {
            txt_game_name.setText(wv.getSeletedItem().toString());
            game_id = gamesDataArrayList.get(wv.getSeletedIndex()).GameID;

            if (Integer.parseInt(gamesDataArrayList.get(wv.getSeletedIndex()).TypesCount) > 0) {
                txt_game_type.setOnClickListener(v1 -> selectGameType(gamesDataArrayList.get(wv.getSeletedIndex()).typesRecordArrayList));
            }
            dialog.dismiss();

        });

        txt_cancel.setOnClickListener(v -> dialog.dismiss());
        wv.setOffset(1);
        wv.setItems(gamesList);
        dialog.show();
    }

    private void selectGameType(ArrayList<GetAllGamesModel.Data.GamesData.TypesRecord> typesRecordArrayList) {
        List<String> game_type_list = new ArrayList<>();
        for (int i = 0; i < typesRecordArrayList.size(); i++) {
            game_type_list.add(typesRecordArrayList.get(i).Name);
        }

        Dialog dialog = new Dialog(ActivityCustomerCare.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View outerView = LayoutInflater.from(ActivityCustomerCare.this).inflate(R.layout.wheelview, null);
        dialog.setContentView(outerView);
        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        CustomWheelView wv = outerView.findViewById(R.id.wh_view);
        TextView txt_ok = outerView.findViewById(R.id.txt_ok);
        TextView title = outerView.findViewById(R.id.title);
        title.setText("Select Game Type");
        TextView txt_cancel = outerView.findViewById(R.id.txt_cancel);
        txt_ok.setOnClickListener(v ->
        {
            txt_game_type.setText(wv.getSeletedItem().toString());
            game_type_id = typesRecordArrayList.get(wv.getSeletedIndex()).TypeID;
            dialog.dismiss();
        });

        txt_cancel.setOnClickListener(v -> dialog.dismiss());
        wv.setOffset(1);
        wv.setItems(game_type_list);
        wv.setSeletion(0);
        dialog.show();
    }

    private void getAllIssues() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityCustomerCare.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<CustomerIssuesModel> signUpModelCall = apiInterface.getGetCustomerIssues(Constants.GET_CUSTOMER_ISSUES);

        signUpModelCall.enqueue(new Callback<CustomerIssuesModel>() {
            @Override
            public void onResponse(@NonNull Call<CustomerIssuesModel> call, @NonNull Response<CustomerIssuesModel> response) {
                progressDialog.dismiss();
                CustomerIssuesModel customerIssuesModel = response.body();
                assert customerIssuesModel != null;
                if (customerIssuesModel.status.equalsIgnoreCase("success")) {

                    txt_help.setText(customerIssuesModel.issuesDataClass.HelpText);
                    if (customerIssuesModel.issuesDataClass.issuesDataArrayList.size() > 0) {
                        issuesDataArrayList = customerIssuesModel.issuesDataClass.issuesDataArrayList;
                    }

                    txt_category.setOnClickListener(v ->
                    {
                        for (int i = 0; i < issuesDataArrayList.size(); i++) {
                            CustomerCareIssuesModel customerCareIssuesModel = new CustomerCareIssuesModel();
                            customerCareIssuesModel.setCat_issue_id(issuesDataArrayList.get(i).IssueID);
                            customerCareIssuesModel.setCategory_Name(issuesDataArrayList.get(i).Name);
                            customerCareIssuesModel.setIsContest_dependent(issuesDataArrayList.get(i).IsContestDependent);
                            customerCareIssuesModel.setSubIssuesDataArrayList(issuesDataArrayList.get(i).SubIssuesData);
                            customerCareIssuesModel.setSub_issues_count(issuesDataArrayList.get(i).SubIssuesCount);
                            customerCareIssuesModels.add(customerCareIssuesModel);
                        }
                        selectCategory(customerCareIssuesModels);
                    });

                } else {
                    Constants.SnakeMessageYellow(mLinearCCeRootView, customerIssuesModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CustomerIssuesModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void selectCategory(ArrayList<CustomerCareIssuesModel> customerIssuesModel) {
        List<String> cateogryList = new ArrayList<>();
        for (int i = 0; i < customerIssuesModel.size(); i++) {
            cateogryList.add(customerIssuesModel.get(i).getCategory_Name());
        }

        Dialog dialog = new Dialog(ActivityCustomerCare.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View outerView = LayoutInflater.from(ActivityCustomerCare.this).inflate(R.layout.wheelview, null);
        dialog.setContentView(outerView);
        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        CustomWheelView wv = outerView.findViewById(R.id.wh_view);
        TextView txt_ok = outerView.findViewById(R.id.txt_ok);
        TextView title = outerView.findViewById(R.id.title);
        title.setText("Select Category");
        TextView txt_cancel = outerView.findViewById(R.id.txt_cancel);
        txt_ok.setOnClickListener(v ->
        {
            txt_category.setText(wv.getSeletedItem().toString());
            Val_Issue = customerIssuesModel.get(wv.getSeletedIndex()).getCat_issue_id();
            reseltallsubcategory();
            if (Integer.parseInt(customerIssuesModel.get(wv.getSeletedIndex()).getSub_issues_count()) > 0) {
                cv_sub_cat.setVisibility(View.VISIBLE);
                txt_sub_category.setOnClickListener(v1 -> selectSubCategory(customerIssuesModel.get(wv.getSeletedIndex()).getSubIssuesDataArrayList()));
            } else {
                cv_sub_cat.setVisibility(View.GONE);
                cv_contest_date.setVisibility(View.GONE);
                cv_contest_time.setVisibility(View.GONE);
                cv_game.setVisibility(View.GONE);
                cv_game_type.setVisibility(View.GONE);
            }

            if (customerIssuesModel.get(wv.getSeletedIndex()).getIsContest_dependent().equalsIgnoreCase("1")) {
                cv_contest_date.setVisibility(View.GONE);
                cv_contest_time.setVisibility(View.GONE);
                cv_game.setVisibility(View.GONE);
                cv_game_type.setVisibility(View.GONE);
            } else {
                cv_contest_date.setVisibility(View.VISIBLE);
                cv_contest_time.setVisibility(View.VISIBLE);
                cv_game.setVisibility(View.VISIBLE);
                cv_game_type.setVisibility(View.VISIBLE);
            }

            dialog.dismiss();

        });

        txt_cancel.setOnClickListener(v -> dialog.dismiss());
        wv.setOffset(1);
        wv.setItems(cateogryList);
        dialog.show();
    }

    private void reseltallsubcategory() {
        contest_id = "";
        game_type_id = "";
        game_id = "";
        txt_sub_category.setText("");
        txt_game_name.setText("");
        txt_game_type.setText("");
        Val_Subissueid = "";
        date = "";
        txt_contest_date.setText("");
        txt_contest_time.setText("");

    }

    private void selectSubCategory(ArrayList<CustomerIssuesModel.Data.IssuesData.SubIssuesData> subIssuesDataArrayList) {
        List<String> sub_cateogryList = new ArrayList<>();
        for (int i = 0; i < subIssuesDataArrayList.size(); i++) {
            sub_cateogryList.add(subIssuesDataArrayList.get(i).Name);
        }

        Dialog dialog = new Dialog(ActivityCustomerCare.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View outerView = LayoutInflater.from(ActivityCustomerCare.this).inflate(R.layout.wheelview, null);
        dialog.setContentView(outerView);
        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        CustomWheelView wv = outerView.findViewById(R.id.wh_view);
        TextView txt_ok = outerView.findViewById(R.id.txt_ok);
        TextView title = outerView.findViewById(R.id.title);
        title.setText("Select Sub Category");
        TextView txt_cancel = outerView.findViewById(R.id.txt_cancel);
        txt_ok.setOnClickListener(v ->
        {
            txt_sub_category.setText(wv.getSeletedItem().toString());
            Val_Subissueid = subIssuesDataArrayList.get(wv.getSeletedIndex()).IssueID;
            dialog.dismiss();
            if (subIssuesDataArrayList.get(wv.getSeletedIndex()).IsContestDependent.equalsIgnoreCase("1")) {
                cv_contest_date.setVisibility(View.GONE);
                cv_contest_time.setVisibility(View.GONE);
                cv_game.setVisibility(View.GONE);
                cv_game_type.setVisibility(View.GONE);
            } else {
                cv_contest_date.setVisibility(View.VISIBLE);
                cv_contest_time.setVisibility(View.VISIBLE);
                cv_game.setVisibility(View.VISIBLE);
                cv_game_type.setVisibility(View.VISIBLE);
            }
        });

        txt_cancel.setOnClickListener(v -> dialog.dismiss());
        wv.setOffset(1);
        wv.setItems(sub_cateogryList);
        wv.setSeletion(0);
        dialog.show();
    }

    private void validate() throws IOException {
        if (!Patterns.EMAIL_ADDRESS.matcher(et_email.getText()).matches())
            Constants.SnakeMessageYellow(mLinearCCeRootView, "Enter Valid Email");
        else if (et_name.getText().length() < 3)
            Constants.SnakeMessageYellow(mLinearCCeRootView, "Enter valid name.");
        else if (et_issue.getText().length() < 2)
            Constants.SnakeMessageYellow(mLinearCCeRootView, "Enter issue properly.");
        else if (txt_category.getText().toString().isEmpty())
            Constants.SnakeMessageYellow(mLinearCCeRootView, "Select Category properly.");
        else if (cv_sub_cat.getVisibility() == View.VISIBLE && Val_Subissueid.equalsIgnoreCase(""))
            Constants.SnakeMessageYellow(mLinearCCeRootView, "Enter sub issue properly.");
        else if (cv_game.getVisibility() == View.VISIBLE && game_id.equalsIgnoreCase(""))
            Constants.SnakeMessageYellow(mLinearCCeRootView, "Enter Game properly.");
        else if (cv_game_type.getVisibility() == View.VISIBLE && game_type_id.equalsIgnoreCase(""))
            Constants.SnakeMessageYellow(mLinearCCeRootView, "Enter Game Type.");
        else if (cv_contest_date.getVisibility() == View.VISIBLE && date.equalsIgnoreCase(""))
            Constants.SnakeMessageYellow(mLinearCCeRootView, "Enter date.");
        else if (cv_contest_time.getVisibility() == View.VISIBLE && contest_id.equalsIgnoreCase(""))
            Constants.SnakeMessageYellow(mLinearCCeRootView, "Enter date.");
        else
            sendToServer();
    }

    private void sendToServer() throws IOException {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityCustomerCare.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        File file = null;
        if (pickResult != null) {
            file = new File(pickResult.getPath());
            OutputStream os = null;
            try {
                os = new BufferedOutputStream(new FileOutputStream(file));
                pickResult.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, os);
                os.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        RequestParams requestParams = new RequestParams();
        try {
            requestParams.put("Action", Constants.SUBMIT_REQUEST);
            requestParams.put("Val_Userid", Pref.getValue(this, Constants.UserID, "", Constants.FILENAME));
            requestParams.put("Val_Username", et_name.getText().toString());
            requestParams.put("Val_Mobilenumber", et_phn.getText().toString());
            requestParams.put("Val_Emailaddress", et_email.getText().toString());

            if (!Val_Issue.equalsIgnoreCase(""))
                requestParams.put("Val_Issueid", Val_Issue);

            if (!Val_Subissueid.equalsIgnoreCase(""))
                requestParams.put("Val_Subissueid", Val_Subissueid);

            if (!game_id.equalsIgnoreCase(""))
                requestParams.put("Val_Gameid", game_id);

            if (!game_type_id.equalsIgnoreCase(""))
                requestParams.put("Val_GTypeid", game_type_id);

            if (!date.equalsIgnoreCase(""))
                requestParams.put("Val_Date", date);

            if (!contest_id.equalsIgnoreCase(""))
                requestParams.put("Val_Contestid", contest_id);

            requestParams.put("Val_Message", et_issue.getText().toString());
            if (pickResult != null)
                requestParams.put("Val_Screenshotimage", file);
            requestParams.put("Val_Appversion", BuildConfig.VERSION_NAME);
            requestParams.put("Val_Devicetype", "Android");

            Common.insertLog("Request:::> " + requestParams.toString());
            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            asyncHttpClient.setTimeout(40 * 1000);

            asyncHttpClient.post(this, BuildConfig.BASE_URL + Constants.GENERAL_DETAILS, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Common.insertLog("Response:::> " + response.toString());
                    super.onSuccess(statusCode, headers, response);
                    progressDialog.dismiss();
//                    Constants.SnakeMessageYellow(mLinearCCeRootView, response.optString("message"));

                    final Dialog dialog = new Dialog(ActivityCustomerCare.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.success_dialog);
                    Window window = dialog.getWindow();
                    assert window != null;
                    window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                    TextView mTextMessage = dialog.findViewById(R.id.txt_success_message);
                    mTextMessage.setText(response.optString("message"));
                    RelativeLayout lyt_done = dialog.findViewById(R.id.lyt_done);
                    lyt_done.setOnClickListener(v ->
                    {
                        dialog.cancel();
                        finish();
                    });
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(mLinearCCeRootView, "Something went wrong.Please try again.");
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(mLinearCCeRootView, "Something went wrong.Please try again.");
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(mLinearCCeRootView, "Something went wrong.Please try again.");
                    super.onFailure(statusCode, headers, responseString, throwable);
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            this.pickResult = pickResult;
            txt_ss.setText("Image file attached.");
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Constants.SnakeMessageYellow(mLinearCCeRootView, pickResult.getError().getMessage());
        }

    }
}
