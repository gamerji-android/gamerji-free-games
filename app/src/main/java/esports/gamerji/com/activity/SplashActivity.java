package esports.gamerji.com.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.facebook.ads.AudienceNetworkAds;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;

import cz.msebera.android.httpclient.Header;
import esports.gamerji.com.BuildConfig;
import esports.gamerji.com.R;
import esports.gamerji.com.model.AppMaintenanceStatusModel;
import esports.gamerji.com.utils.AnalyticsSocket;
import esports.gamerji.com.utils.Common;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.Pref;
import esports.gamerji.com.webServices.APIClient;
import esports.gamerji.com.webServices.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static esports.gamerji.com.utils.Constants.Apptype;
import static esports.gamerji.com.utils.Constants.PLAYSTORE;

public class SplashActivity extends AppCompatActivity {

    private RelativeLayout mRelativeMain, mRelativeSplashView;
    private AnalyticsSocket analyticsSocket;
    private LinearLayout mLinearMaintenanceView;
    private TextView mTextMaintenanceTitle, mTextMaintenanceMessage;

    @SuppressLint({"PackageManagerGetSignatures", "BatteryLife"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        AudienceNetworkAds.initialize(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        analyticsSocket = new AnalyticsSocket();

        System.out.println("---------------------------------------------");
        System.out.println("Build Type:::> " + BuildConfig.BUILD_TYPE);
        System.out.println("Flavor:::> " + BuildConfig.FLAVOR);
        System.out.println("Version Code:::> " + BuildConfig.VERSION_CODE);
        System.out.println("Version Name:::> " + BuildConfig.VERSION_NAME);
        System.out.println("---------------------------------------------");
        getIds();
        callToGetAppMaintenanceStatusAPI();
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layouts
            mRelativeMain = findViewById(R.id.parent_layout);
            mRelativeSplashView = findViewById(R.id.relative_splash_main_view);

            // Linear Layouts
            mLinearMaintenanceView = findViewById(R.id.linear_splash_maintenance_view);

            // Text Views
            mTextMaintenanceTitle = findViewById(R.id.relative_splash_maintenance_title);
            mTextMaintenanceMessage = findViewById(R.id.relative_splash_maintenance_message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should get the App Maintenance Status
     */
    private void callToGetAppMaintenanceStatusAPI() {
        try {
//            final ProgressDialog progressDialog = new ProgressDialog(SplashActivity.this);
//            progressDialog.setMessage("Please wait...."); // Setting Message
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
//            progressDialog.show(); // Display Progress Dialog
//            progressDialog.setCancelable(false);

            Call<AppMaintenanceStatusModel> call = APIClient.getClient().create(APIInterface.class).getAppMaintenanceStatus();
            call.enqueue(new Callback<AppMaintenanceStatusModel>() {
                @Override
                public void onResponse(@NonNull Call<AppMaintenanceStatusModel> call, @NonNull Response<AppMaintenanceStatusModel> response) {

//                    progressDialog.dismiss();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {
                            mRelativeSplashView.setVisibility(View.GONE);
                            mLinearMaintenanceView.setVisibility(View.VISIBLE);
                            mTextMaintenanceTitle.setText(response.body().getData().getTitle());
                            mTextMaintenanceMessage.setText(response.body().getData().getSubTitle());
                        } else {
                            mRelativeSplashView.setVisibility(View.VISIBLE);
                            mLinearMaintenanceView.setVisibility(View.GONE);
                            checkPlaystoreVersion();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AppMaintenanceStatusModel> call, @NonNull Throwable t) {
//                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void checkPlaystoreVersion() {
        Apptype = PLAYSTORE;
        RequestParams requestParams = new RequestParams();

        requestParams.put("Action", Constants.APP_VERSION);

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

        asyncHttpClient.post(this, BuildConfig.BASE_URL + Constants.GENERAL_FETCH, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Common.insertLog("RESPONSE" + response.toString());
                if (response.optString("status").equalsIgnoreCase("success")) {
                    JSONObject data = response.optJSONObject("data");
                    JSONArray jsonArray = data.optJSONArray("VersionsData");
                    JSONObject versionData = null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            versionData = jsonArray.getJSONObject(i);
                            if (versionData.optString("OSType").equalsIgnoreCase("android-playstore")) {
                                String version = "";
                                try {
                                    PackageInfo info = SplashActivity.this.getPackageManager().getPackageInfo(SplashActivity.this.getPackageName(), PackageManager.GET_ACTIVITIES);
                                    version = info.versionName;
                                } catch (PackageManager.NameNotFoundException e) {
                                    e.printStackTrace();
                                }

                                if (!versionData.opt("Version").equals(version)) {
                                    boolean forceful;
                                    forceful = Integer.parseInt(versionData.optString("ForcefullyUpdate")) == 0;

                                    showPlaystoreAlert(versionData.optString("DownloadLink"), versionData.optString("NewUpdateDetail"), forceful, versionData.optString("DownloadLink"));
                                } else {
                                    checkFirebaseDynamicLinks();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //jsonObject =jsonArray.optJSONObject(1);
                        }
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                checkFirebaseDynamicLinks();
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                checkFirebaseDynamicLinks();
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                checkFirebaseDynamicLinks();
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        });
    }

    private void showPlaystoreAlert(String mUrl, String mNewUpdateDetails, boolean hasCancel, String downloadLink) {
        Dialog dialog = new Dialog(SplashActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.app_update_dialog);
        // checkFirebaseDynamicLinks();
        dialog.setOnCancelListener(DialogInterface::dismiss);
        dialog.setCancelable(hasCancel);
        dialog.setCanceledOnTouchOutside(hasCancel);
        ImageView mImgBackUpdateDialog = dialog.findViewById(R.id.mImgBackUpdateDialog);
        TextView mTxtNewUpdateDetails = dialog.findViewById(R.id.mTxtNewUpdateDetails);
        mTxtNewUpdateDetails.setText(mNewUpdateDetails);
        if (hasCancel) {
            mImgBackUpdateDialog.setVisibility(View.VISIBLE);
        } else {
            mImgBackUpdateDialog.setVisibility(View.GONE);
        }

        mImgBackUpdateDialog.setOnClickListener(v ->
        {
            dialog.dismiss();
            checkFirebaseDynamicLinks();
        });

        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        CardView card_View_update_now = dialog.findViewById(R.id.card_View_update_now);

        if (downloadLink.equalsIgnoreCase("")) {
            card_View_update_now.setVisibility(View.GONE);
        } else {
            card_View_update_now.setVisibility(View.VISIBLE);
        }

        card_View_update_now.setOnClickListener(v ->
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=esports.gamerji.com&hl=en")));
        });
        dialog.show();

        dialog.setOnCancelListener(dialog1 ->
        {
            checkFirebaseDynamicLinks();
        });
    }

    void checkFirebaseDynamicLinks() {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, pendingDynamicLinkData ->
                {
                    Uri deepLink = null;
                    String contest_id = null;
                    if (pendingDynamicLinkData != null) {
                        deepLink = pendingDynamicLinkData.getLink();
                        assert deepLink != null;
                        Set<String> key = deepLink.getQueryParameterNames();
                        contest_id = deepLink.getQueryParameter(key.iterator().next());
                    }
                    if (deepLink == null) {
                        getIntroScreen();
                    } else {
                        if (!Pref.getValue(SplashActivity.this, Constants.IS_LOGIN, false, Constants.FILENAME)) {
                            try {
                                Constants.SnakeMessageYellow(mRelativeMain, "Please login to join a league");
                                getIntroScreen();
                            } catch (Exception e) {
                                Constants.SnakeMessageYellow(mRelativeMain, "Please login to join a league");
                                getIntroScreen();
                            }
                        }
                    }
                })
                .addOnFailureListener(this, e -> getIntroScreen());
    }

    void getIntroScreen() {
        if (Pref.getValue(SplashActivity.this, Constants.IS_LOGIN, false, Constants.FILENAME)) {
            analyticsSocket.connect();
//            Intent mainIntent = new Intent(SplashActivity.this, ActivityMain.class);
            Intent mainIntent = new Intent(SplashActivity.this, BottomNavigationActivity.class);
            startActivity(mainIntent);
            finish();
        } else {
            Intent mainIntent = new Intent(SplashActivity.this, Activity_Login.class);
            startActivity(mainIntent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
