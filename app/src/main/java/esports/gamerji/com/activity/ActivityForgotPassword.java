package esports.gamerji.com.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.hbb20.CountryCodePicker;

import esports.gamerji.com.R;
import esports.gamerji.com.model.SignInModel;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.FileUtils;
import esports.gamerji.com.utils.Pref;
import esports.gamerji.com.webServices.APIClient;
import esports.gamerji.com.webServices.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityForgotPassword extends AppCompatActivity
{

    CountryCodePicker ccp;
    EditText et_phn;
    RelativeLayout lyt_send;
    APIInterface apiInterface;
    ImageView img_back;
    LinearLayout parent_layout;
    CardView cv_phone;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_frorgot_password);
        ccp=findViewById(R.id.ccp);
        cv_phone=findViewById(R.id.cv_phone);
        parent_layout=findViewById(R.id.parent_layout);
        et_phn=findViewById(R.id.et_phn);
        lyt_send=findViewById(R.id.lyt_send);
        img_back=findViewById(R.id.img_back);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        lyt_send.setOnClickListener(v -> {
            if (et_phn.getText().length()<10)
                Constants.SnakeMessageYellow(parent_layout, "Please enter valid phone number.");
            else
                sendToServer();
        });
        cv_phone.setOnClickListener(v -> FileUtils.requestfoucs(ActivityForgotPassword.this,et_phn));
        img_back.setOnClickListener(v -> finish());
    }

    private void sendToServer()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityForgotPassword.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SignInModel> resendOTPModelCall = apiInterface.forgotOTP(Constants.GENERATE_OTP,ccp.getSelectedCountryCodeWithPlus(),et_phn.getText().toString());

        resendOTPModelCall.enqueue(new Callback<SignInModel>()
        {
            @Override
            public void onResponse(@NonNull Call<SignInModel> call, @NonNull Response<SignInModel> response)
            {
                progressDialog.dismiss();
                SignInModel  resendOTPModel = response.body();
                assert resendOTPModel != null;
                if (resendOTPModel.status.equalsIgnoreCase("success"))
                {
                    Intent i = new Intent(ActivityForgotPassword.this,ActivityOTP.class);
                    Pref.setValue(ActivityForgotPassword.this, Constants.UserID,resendOTPModel.userDataClass.UserID,Constants.FILENAME);
                    i.putExtra("OTP",resendOTPModel.userDataClass.OTP);
                    i.putExtra("country_code",ccp.getSelectedCountryCodeWithPlus());
                    i.putExtra("phone",et_phn.getText().toString());
                    i.putExtra("from_login",false);
                    i.putExtra("is_social",false);
                    i.putExtra("fgt_pass",true);
                    startActivity(i);
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_layout, resendOTPModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignInModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });
    }
}
