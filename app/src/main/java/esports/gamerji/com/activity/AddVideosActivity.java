package esports.gamerji.com.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import esports.gamerji.com.R;
import esports.gamerji.com.model.AddVideosModel;
import esports.gamerji.com.utils.AppCallbackListener;
import esports.gamerji.com.utils.AppUtil;
import esports.gamerji.com.utils.Common;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.SessionManager;
import esports.gamerji.com.webServices.APIClient;
import esports.gamerji.com.webServices.APICommonMethods;
import esports.gamerji.com.webServices.APIInterface;
import esports.gamerji.com.webServices.WebFields;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddVideosActivity extends AppCompatActivity implements AppCallbackListener.CallBackListener {

    private RelativeLayout mRelativeMainView;
    private ImageView mImageBack;
    private EditText mEditVideosTitle, mEditVideosLink;
    private Button mButtonSubmit;
    private String mVideosTitle, mVideosLink;
    private AppUtil mAppUtils;
    private SessionManager mSessionManager;
    private AdView mAdView;
    private LinearLayout mLinearAdContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_videos);
        mAppUtils = new AppUtil(this);
        mSessionManager = new SessionManager();

        getIds();
        setAdView();
        setRegListeners();
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layout
            mRelativeMainView = findViewById(R.id.relative_add_videos_main_view);

            // Image View
            mImageBack = findViewById(R.id.image_add_videos_back);
            mEditVideosTitle = findViewById(R.id.edit_add_videos_title);
            mEditVideosLink = findViewById(R.id.edit_add_videos_link);

            // Buttons
            mButtonSubmit = findViewById(R.id.button_add_videos_submit);

            // Linear Layout
            mLinearAdContainer = findViewById(R.id.linear_add_videos_ads_container);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the Ad View
     */
    private void setAdView() {
        try {
            mAdView = new AdView(AddVideosActivity.this, getResources().getString(R.string.fb_placement_id), AdSize.BANNER_HEIGHT_50);
            mLinearAdContainer.addView(mAdView);
            mAdView.loadAd();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mImageBack.setOnClickListener(clickListener);
            mButtonSubmit.setOnClickListener(clickListener);

            // ToDo: Facebook Ad Listener
            mAdView.setAdListener(adListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_add_videos_back:
                    finish();
                    break;

                case R.id.button_add_videos_submit:
                    doAddVideos();
                    break;
            }
        }
    };

    /**
     * Facebook Ad listeners
     */
    private AdListener adListener = new AdListener() {
        @Override
        public void onError(Ad ad, AdError adError) {

        }

        @Override
        public void onAdLoaded(Ad ad) {
            mLinearAdContainer.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAdClicked(Ad ad) {

        }

        @Override
        public void onLoggingImpression(Ad ad) {

        }
    };

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    /**
     * This method checks the validation first and then call the API
     */
    @SuppressLint("SetTextI18n")
    private void doAddVideos() {
        hideSoftKeyboard();
        if (checkValidation()) {
            if (!mAppUtils.getConnectionState()) {
                mAppUtils.displayNoInternetSnackBar(mRelativeMainView, new AppCallbackListener(this));
            } else {
                callToAddVideosAPI();
            }
        }
    }

    /**
     * Hide the Soft Keyboard
     */
    public void hideSoftKeyboard() {
        try {
            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        mVideosTitle = mEditVideosTitle.getText().toString().trim();
        mVideosLink = mEditVideosLink.getText().toString().trim();

        mEditVideosTitle.setError(null);
        mEditVideosLink.setError(null);

        if (TextUtils.isEmpty(mVideosTitle)) {
            status = false;
            mEditVideosTitle.setError(getResources().getString(R.string.error_field_required));
        }

        if (TextUtils.isEmpty(mVideosLink)) {
            status = false;
            mEditVideosLink.setError(getResources().getString(R.string.error_field_required));
        }

        if (status) {
            if (!Patterns.WEB_URL.matcher(mVideosLink).matches()) {
                status = false;
                Constants.SnakeMessageYellow(mRelativeMainView, getResources().getString(R.string.error_valid_youtube_url));
            }
        }
        return status;
    }

    /**
     * This should add videos
     */
    private void callToAddVideosAPI() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(AddVideosActivity.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAddVideosJson(mSessionManager.getUserId(AddVideosActivity.this), mVideosTitle, mVideosLink));

            Call<AddVideosModel> call = APIClient.getClient().create(APIInterface.class).addVideos(body);
            call.enqueue(new Callback<AddVideosModel>() {
                @Override
                public void onResponse(@NonNull Call<AddVideosModel> call, @NonNull Response<AddVideosModel> response) {

                    progressDialog.dismiss();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);

                        String mStatus = jsonObject.getString(WebFields.STATUS);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);

                        assert response.body() != null;
                        if (mStatus.equalsIgnoreCase(Constants.API_SUCCESS)) {
                            finish();
                            Constants.SnakeMessageYellow(mRelativeMainView, mMessage);
                        } else {
                            finish();
                            Constants.SnakeMessageYellow(mRelativeMainView, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddVideosModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAppCallback(int Code) {
        doAddVideos();
    }
}