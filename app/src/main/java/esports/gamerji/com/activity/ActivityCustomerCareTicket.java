package esports.gamerji.com.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import esports.gamerji.com.R;
import esports.gamerji.com.adapter.CustomerCareTicketAdapter;
import esports.gamerji.com.model.CustomerCareModel;
import esports.gamerji.com.model.CustomerTicketsModel;
import esports.gamerji.com.utils.Common;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.Pref;
import esports.gamerji.com.utils.SessionManager;
import esports.gamerji.com.webServices.APIClient;
import esports.gamerji.com.webServices.APICommonMethods;
import esports.gamerji.com.webServices.APIInterface;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCustomerCareTicket extends AppCompatActivity {

    private RelativeLayout mRelativeMain, mRelativeChatBotView;
    private ImageView mImageBack;
    private CardView mCardRaiseComplain;
    private RecyclerView mRecyclerView;
    private ArrayList<CustomerTicketsModel.Data.TicketsData> mArrCustomerCareTickets = new ArrayList<>();
    private CustomerCareTicketAdapter mAdapterCustomerCareTickets;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager linearLayoutManager;
    private boolean isLastPage = false;
    private int page = 1;
    private AdView mAdView;
    private LinearLayout mLinearAdContainer, mLinearRaiseComplain;
    private WebView mWebView;
    private ProgressBar mProgressBar;
    private SessionManager mSessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_care_ticket);
        mSessionManager = new SessionManager();

        getIds();
        setAdView();
        setRegListeners();
        callToGetCustomerCareAPI();
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Linear Layout
            mRelativeMain = findViewById(R.id.relative_customer_care_main_view);
            mRelativeChatBotView = findViewById(R.id.relative_customer_care_chat_bot_view);

            // Image Views
            mImageBack = findViewById(R.id.image_customer_care_back);

            // Card View
            mCardRaiseComplain = findViewById(R.id.card_view_customer_care_raise_a_complain);

            // Recycler View
            mRecyclerView = findViewById(R.id.recycler_view_customer_care_complaints);

            // Linear Layout
            mLinearAdContainer = findViewById(R.id.linear_customer_care_ads_container);
            mLinearRaiseComplain = findViewById(R.id.linear_customer_care_raise_a_complain);

            // Web View
            mWebView = findViewById(R.id.web_view_customer_care);

            // Progress Bar
            mProgressBar = findViewById(R.id.progress_bar_customer_care);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the Ad View
     */
    private void setAdView() {
        try {
            mAdView = new AdView(ActivityCustomerCareTicket.this, getResources().getString(R.string.fb_placement_id), AdSize.BANNER_HEIGHT_50);
            mLinearAdContainer.addView(mAdView);
            mAdView.loadAd();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mImageBack.setOnClickListener(clickListener);
            mCardRaiseComplain.setOnClickListener(clickListener);

            // ToDo: Facebook Ad Listener
            mAdView.setAdListener(adListener);

            // ToDo: On Scroll Listener
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        visibleItemCount = linearLayoutManager.getChildCount();
                        totalItemCount = linearLayoutManager.getItemCount();
                        pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                        if (!isLastPage) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                getTickets(++page);
                            }
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_customer_care_back:
                    finish();
                    break;

                case R.id.card_view_customer_care_raise_a_complain:
                    callToCustomerCareActivity();
                    break;
            }
        }
    };

    /**
     * Facebook Ad listeners
     */
    private AdListener adListener = new AdListener() {
        @Override
        public void onError(Ad ad, AdError adError) {

        }

        @Override
        public void onAdLoaded(Ad ad) {
            mLinearAdContainer.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAdClicked(Ad ad) {

        }

        @Override
        public void onLoggingImpression(Ad ad) {

        }
    };

    /**
     * This method redirects you to the Customer Care activity
     */
    private void callToCustomerCareActivity() {
        try {
            Intent i = new Intent(ActivityCustomerCareTicket.this, ActivityCustomerCare.class);
            startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    /**
     * This should get all the Customer Care Tickets
     *
     * @param page - Page
     */
    private void getTickets(int page) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityCustomerCareTicket.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<CustomerTicketsModel> resendOTPModelCall = APIClient.getClient().create(APIInterface.class).getCustomerTickets(Constants.GET_CUSTOMER_TICKETS, Pref.getValue(ActivityCustomerCareTicket.this,
                Constants.UserID, "", Constants.FILENAME), page, 20);

        resendOTPModelCall.enqueue(new Callback<CustomerTicketsModel>() {
            @Override
            public void onResponse(@NonNull Call<CustomerTicketsModel> call, @NonNull Response<CustomerTicketsModel> response) {
                progressDialog.dismiss();
                CustomerTicketsModel customerTicketsModel = response.body();
                assert customerTicketsModel != null;
                if (customerTicketsModel.status.equalsIgnoreCase("success")) {
                    mArrCustomerCareTickets.addAll(customerTicketsModel.DataClass.ticketsDataArrayList);
                    linearLayoutManager = new LinearLayoutManager(ActivityCustomerCareTicket.this);
                    mRecyclerView.setLayoutManager(linearLayoutManager);
                    mAdapterCustomerCareTickets = new CustomerCareTicketAdapter(ActivityCustomerCareTicket.this, mArrCustomerCareTickets);
                    mRecyclerView.setAdapter(mAdapterCustomerCareTickets);
                    mAdapterCustomerCareTickets.notifyDataSetChanged();
                    isLastPage = customerTicketsModel.DataClass.IsLast;
                } else {
                    Constants.SnakeMessageYellow(mRelativeMain, customerTicketsModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CustomerTicketsModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mArrCustomerCareTickets.clear();
        getTickets(page);
    }

    /**
     * This should get the Customer Care
     */
    private void callToGetCustomerCareAPI() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ActivityCustomerCareTicket.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setGetCustomerCareJson(mSessionManager.getUserId(ActivityCustomerCareTicket.this)));

            Call<CustomerCareModel> call = APIClient.getClient().create(APIInterface.class).getCustomerCare(body);
            call.enqueue(new Callback<CustomerCareModel>() {
                @Override
                public void onResponse(@NonNull Call<CustomerCareModel> call, @NonNull Response<CustomerCareModel> response) {

                    progressDialog.dismiss();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {
                            if (!response.body().getData().getChatBotURLFlag()) {
                                mLinearRaiseComplain.setVisibility(View.VISIBLE);
                                mRelativeChatBotView.setVisibility(View.GONE);
                            } else {
                                mLinearRaiseComplain.setVisibility(View.GONE);
                                mRelativeChatBotView.setVisibility(View.VISIBLE);
                                loadWebView(response.body().getData().getChatBotURLAndroid());
                            }
                        } else {
                            Constants.SnakeMessageYellow(mRelativeMain, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CustomerCareModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should load the data to the web view
     *
     * @param mChatBotURL - Chat Bot URL
     */
    private void loadWebView(String mChatBotURL) {
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setDomStorageEnabled(true);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressBar.setVisibility(ProgressBar.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressBar.setVisibility(ProgressBar.GONE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                mProgressBar.setVisibility(ProgressBar.GONE);
            }
        });
        mWebView.loadUrl(mChatBotURL);
    }
}
