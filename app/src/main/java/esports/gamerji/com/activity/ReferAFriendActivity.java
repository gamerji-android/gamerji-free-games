package esports.gamerji.com.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.google.gson.Gson;

import esports.gamerji.com.R;
import esports.gamerji.com.model.ReferAFriendModel;
import esports.gamerji.com.utils.Common;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.FileUtils;
import esports.gamerji.com.utils.SessionManager;
import esports.gamerji.com.webServices.APIClient;
import esports.gamerji.com.webServices.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReferAFriendActivity extends AppCompatActivity {

    private LinearLayout mLinearViewMainView, mLinearAdContainer;
    private ImageView mImageBack;
    private TextView mTextTopMessage, mTextReferMessage, mTextInviteMessage, mTextReferralCode, mTextCopyCode;
    private CardView mCardViewInviteViaWhatsApp, mCardViewReferFriends;
    private String mInviteeBonus;
    private AdView mAdView;
    private SessionManager mSessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_a_friend);
        mSessionManager = new SessionManager();

        getIds();
        setAdView();
        setRegListeners();
        callToGetReferAFriendAPI();
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layout
            mLinearViewMainView = findViewById(R.id.linear_refer_a_friend_main_view);

            // Image View
            mImageBack = findViewById(R.id.image_refer_a_friend_back);

            // Text Views
            mTextTopMessage = findViewById(R.id.text_refer_a_friend_top_message);
            mTextReferMessage = findViewById(R.id.text_refer_a_friend_refer_message);
            mTextInviteMessage = findViewById(R.id.text_refer_a_friend_invite_message);
            mTextReferralCode = findViewById(R.id.text_refer_a_friend_referral_code);
            mTextCopyCode = findViewById(R.id.text_refer_a_friend_copy_code);

            // Card Views
            mCardViewInviteViaWhatsApp = findViewById(R.id.card_view_refer_a_friend_invite_via_whats_app);
            mCardViewReferFriends = findViewById(R.id.card_view_refer_a_friend_refer_friends);

            // Linear Layout
            mLinearAdContainer = findViewById(R.id.linear_refer_a_friend_ads_container);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the Ad View
     */
    private void setAdView() {
        try {
            mAdView = new AdView(ReferAFriendActivity.this, getResources().getString(R.string.fb_placement_id), AdSize.BANNER_HEIGHT_50);
            mLinearAdContainer.addView(mAdView);
            mAdView.loadAd();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mImageBack.setOnClickListener(clickListener);
            mTextCopyCode.setOnClickListener(clickListener);
            mCardViewInviteViaWhatsApp.setOnClickListener(clickListener);
            mCardViewReferFriends.setOnClickListener(clickListener);

            // ToDo: Facebook Ad Listener
            mAdView.setAdListener(adListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_refer_a_friend_back:
                    finish();
                    break;

                case R.id.text_refer_a_friend_copy_code:
                    FileUtils.setClipboard(ReferAFriendActivity.this, mTextReferralCode.getText().toString());
                    break;

                case R.id.card_view_refer_a_friend_invite_via_whats_app:
                    inviteViaWhatsApp();
                    break;

                case R.id.card_view_refer_a_friend_refer_friends:
                    shareViaReferFriends();
                    break;
            }
        }
    };

    /**
     * Facebook Ad listeners
     */
    private AdListener adListener = new AdListener() {
        @Override
        public void onError(Ad ad, AdError adError) {

        }

        @Override
        public void onAdLoaded(Ad ad) {
            mLinearAdContainer.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAdClicked(Ad ad) {

        }

        @Override
        public void onLoggingImpression(Ad ad) {

        }
    };

    /**
     * This should open up the Whats App to share the Referral Code and other details.
     */
    private void inviteViaWhatsApp() {
        try {
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setType("text/plain");
            whatsappIntent.setPackage("com.whatsapp");
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, "Hey, download this fantastic app GamerJi  and get ₹ 100 bonus using my referral code. " + mTextReferralCode.getText().toString() + " You can download it by visiting https://www.gamerji.com");
            try {
                startActivity(whatsappIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                Constants.SnakeMessageYellow(mLinearViewMainView, "Whatsapp have not been installed.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should open up the default share options to share the Referral Code and other details.
     */
    private void shareViaReferFriends() {
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey, download this fantastic app GamerJi  and get ₹ " + mInviteeBonus + " bonus using my referral code " + mTextReferralCode.getText().toString() + ". You can download it by visiting https://www.gamerji.com");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Refer A Friend API
     */
    private void callToGetReferAFriendAPI() {
        try {
            ProgressDialog progressDialog = new ProgressDialog(ReferAFriendActivity.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            String mUserId = mSessionManager.getUserId(ReferAFriendActivity.this);

            Call<ReferAFriendModel> callRepos = APIClient.getClient().create(APIInterface.class).getUserRefferal(Constants.GET_REFERRAL, mUserId);
            callRepos.enqueue(new Callback<ReferAFriendModel>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<ReferAFriendModel> call, @NonNull Response<ReferAFriendModel> response) {
                    progressDialog.dismiss();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {

                            mTextTopMessage.setText(response.body().getData().getTitle());
                            mTextReferMessage.setText(response.body().getData().getReferText());
                            mTextInviteMessage.setText(response.body().getData().getInviteText());
                            mTextReferralCode.setText(response.body().getData().getReferralCode());
                            mInviteeBonus = response.body().getData().getInviteeBonus();
                        } else {
                            Constants.SnakeMessageYellow(mLinearViewMainView, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReferAFriendModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
