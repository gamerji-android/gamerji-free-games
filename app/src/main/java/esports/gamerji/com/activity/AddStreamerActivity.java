package esports.gamerji.com.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.google.gson.Gson;
import com.hbb20.CountryCodePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import esports.gamerji.com.R;
import esports.gamerji.com.adapter.GamesAdapter;
import esports.gamerji.com.model.AddStreamerModel;
import esports.gamerji.com.model.GetGamesModel;
import esports.gamerji.com.utils.AppCallbackListener;
import esports.gamerji.com.utils.AppConstants;
import esports.gamerji.com.utils.AppUtil;
import esports.gamerji.com.utils.Common;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.Pref;
import esports.gamerji.com.webServices.APIClient;
import esports.gamerji.com.webServices.APICommonMethods;
import esports.gamerji.com.webServices.APIInterface;
import esports.gamerji.com.webServices.WebFields;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddStreamerActivity extends AppCompatActivity implements AppCallbackListener.CallBackListener, GamesAdapter.SingleClickListener {

    private RelativeLayout mRelativeMainView;
    private ImageView mImageBack, mImageStatus;
    private TextView mTextStreamHeader;
    private ScrollView mScrollView;
    private EditText mEditName, mEditMobile, mEditEmail, mEditYoutubeChannelName, mEditYoutubeChannelLink, mEditOtherGameName;
    private CountryCodePicker mCountryCodePicker;
    private RadioGroup mRadioGroup;
    private RecyclerView mRecyclerView;
    private CheckBox mCheckBoxYoutube, mCheckBoxTwitch, mCheckBoxFacebook;
    private CardView mCardViewOtherGameName;
    private Button mButtonSubmit;
    private AdView mAdView;
    private LinearLayout mLinearStatus, mLinearAdContainer;
    private TextView mTextStatus, mTextReason;
    private String mUserId, mName, mMobileNo, mEmail, mYoutubeChannelName, mYoutubeChannelLink, mOtherGameName, mGender, mStreamName, mStatus, mStreamerReason, mStreamerDisplayStatus;
    private int mCountryCode, mStreamerStatus;
    private AppUtil mAppUtils;
    private ArrayList<GetGamesModel.Data.GamesData> mArrGames;
    private ArrayList<String> mArrSelectedGames;
    private ArrayList<String> mArrSelectedPlatforms;
    private boolean mOthersSelected;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_streamer);
        mAppUtils = new AppUtil(this);
        mArrGames = new ArrayList<>();
        mArrSelectedGames = new ArrayList<>();
        mArrSelectedPlatforms = new ArrayList<>();

        getBundle();
        getIds();
        setAdView();
        setRegListeners();
        setData();
        callToGetGamesAPI();
    }

    /**
     * Get the keys from the more fragment
     */
    private void getBundle() {
        try {
            mStreamName = getIntent().getStringExtra(AppConstants.BUNDLE_STREAM_NAME);
            mStatus = getIntent().getStringExtra(AppConstants.BUNDLE_STATUS);
            mStreamerStatus = getIntent().getIntExtra(AppConstants.BUNDLE_STREAMER_STATUS, 0);
            mStreamerReason = getIntent().getStringExtra(AppConstants.BUNDLE_STREAMER_REASON);
            mStreamerDisplayStatus = getIntent().getStringExtra(AppConstants.BUNDLE_STREAMER_DISPLAY_STATUS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layout
            mRelativeMainView = findViewById(R.id.relative_add_streamer_main_view);

            // Image View
            mImageBack = findViewById(R.id.image_add_streamer_back);
            mImageStatus = findViewById(R.id.image_add_streamer_status);

            // Text Views
            mTextStreamHeader = findViewById(R.id.text_add_streamer_header);

            // Scroll View
            mScrollView = findViewById(R.id.scroll_view_add_streamer);

            // Edit Texts
            mEditName = findViewById(R.id.edit_add_streamer_name);
            mEditMobile = findViewById(R.id.edit_add_streamer_mobile_no);
            mEditEmail = findViewById(R.id.edit_add_streamer_email);
            mEditYoutubeChannelName = findViewById(R.id.edit_add_streamer_youtube_channel_name);
            mEditYoutubeChannelLink = findViewById(R.id.edit_add_streamer_youtube_channel_link);
            mEditOtherGameName = findViewById(R.id.edit_add_streamer_other_game_name);

            // Country Code Picker
            mCountryCodePicker = findViewById(R.id.country_code_picker_add_streamer);

            // Radio Group
            mRadioGroup = findViewById(R.id.radio_group_add_streamer_gender);

            // Recycler View
            mRecyclerView = findViewById(R.id.recycler_view_add_streamer_games);

            // Check Boxes
            mCheckBoxYoutube = findViewById(R.id.checkbox_add_streamer_platforms_youtube);
            mCheckBoxTwitch = findViewById(R.id.checkbox_add_streamer_platforms_twitch);
            mCheckBoxFacebook = findViewById(R.id.checkbox_add_streamer_platforms_facebook);

            // Card Views
            mCardViewOtherGameName = findViewById(R.id.card_view_add_streamer_other_game_name);

            // Button
            mButtonSubmit = findViewById(R.id.button_add_streamer_submit);

            // Linear Layout
            mLinearStatus = findViewById(R.id.linear_add_streamer_status);
            mLinearAdContainer = findViewById(R.id.linear_add_streamer_ads_container);

            // Text View
            mTextStatus = findViewById(R.id.text_add_streamer_status);
            mTextReason = findViewById(R.id.text_add_streamer_reason);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the Ad View
     */
    private void setAdView() {
        try {
            mAdView = new AdView(AddStreamerActivity.this, getResources().getString(R.string.fb_placement_id), AdSize.BANNER_HEIGHT_50);
            mLinearAdContainer.addView(mAdView);
            mAdView.loadAd();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mImageBack.setOnClickListener(clickListener);
            mButtonSubmit.setOnClickListener(clickListener);

            // ToDo: On Checked Change Listener
            mRadioGroup.setOnCheckedChangeListener(checkedChangeListener);

            // ToDo: Facebook Ad Listener
            mAdView.setAdListener(adListener);

            // ToDo: Check Box On Click Listener
            mCheckBoxYoutube.setOnClickListener(checkBoxClickListener);
            mCheckBoxTwitch.setOnClickListener(checkBoxClickListener);
            mCheckBoxFacebook.setOnClickListener(checkBoxClickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            mTextStreamHeader.setText(mStreamName);

            if (mStatus.equalsIgnoreCase(Constants.API_SUCCESS)) {
                mScrollView.setVisibility(View.GONE);
                mLinearStatus.setVisibility(View.VISIBLE);

                mTextStatus.setText(mStreamerDisplayStatus);
                mTextReason.setText(mStreamerReason);
                if (mStreamerStatus == 1) {
                    // ToDo: Streamer Status -> Pending
                    mTextStatus.setTextColor(getResources().getColor(R.color.color_yellow));
                    mImageStatus.setBackgroundResource(R.drawable.ic_pending);
                } else if (mStreamerStatus == 3) {
                    // ToDo: Streamer Status -> Under Review
                    mTextStatus.setTextColor(getResources().getColor(R.color.color_blue));
                    mImageStatus.setBackgroundResource(R.drawable.ic_under_review);
                } else if (mStreamerStatus == 4) {
                    // ToDo: Streamer Status -> Rejected
                    mTextStatus.setTextColor(getResources().getColor(R.color.color_red));
                    mImageStatus.setBackgroundResource(R.drawable.ic_rejected);
                }
            } else {
                mScrollView.setVisibility(View.VISIBLE);
                mLinearStatus.setVisibility(View.GONE);
            }

            mCountryCodePicker.setCcpClickable(false);
            mUserId = Pref.getValue(AddStreamerActivity.this, Constants.UserID, "", Constants.FILENAME);
            mName = Pref.getValue(this, Constants.firstname, "", Constants.FILENAME) + " " + Pref.getValue(this, Constants.lastname, "", Constants.FILENAME);
            mCountryCode = Integer.parseInt(Pref.getValue(this, Constants.CountryCode, "", Constants.FILENAME));
            mMobileNo = Pref.getValue(this, Constants.MobileNumber, "", Constants.FILENAME);
            mEmail = Pref.getValue(this, Constants.EmailAddress, "", Constants.FILENAME);

            mEditName.setText(mName);
            mEditMobile.setText(mMobileNo);
            mEditEmail.setText(mEmail);

            if (!(Pref.getValue(this, Constants.CountryCode, "", Constants.FILENAME).equalsIgnoreCase(""))) {
                mCountryCodePicker.setCountryForPhoneCode(mCountryCode);
            }

            mGender = getResources().getString(R.string.text_male);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_add_streamer_back:
                    finish();
                    break;

                case R.id.button_add_streamer_submit:
                    doAddStreamer();
                    break;
            }
        }
    };

    /**
     * Checked change listener
     */
    private RadioGroup.OnCheckedChangeListener checkedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {

            if (mRadioGroup.getCheckedRadioButtonId() == R.id.radio_add_streamer_male) {
                mGender = getResources().getString(R.string.text_male);
            } else if (mRadioGroup.getCheckedRadioButtonId() == R.id.radio_add_streamer_female) {
                mGender = getResources().getString(R.string.text_female);
            }
            Common.insertLog("Gender:::> " + mGender);
        }
    };

    /**
     * Facebook Ad listeners
     */
    private AdListener adListener = new AdListener() {
        @Override
        public void onError(Ad ad, AdError adError) {

        }

        @Override
        public void onAdLoaded(Ad ad) {
            mLinearAdContainer.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAdClicked(Ad ad) {

        }

        @Override
        public void onLoggingImpression(Ad ad) {

        }
    };

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    /**
     * Checkbox click listener
     */
    private View.OnClickListener checkBoxClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            boolean checked = ((CheckBox) v).isChecked();
            switch (v.getId()) {
                case R.id.checkbox_add_streamer_platforms_youtube:
                    addRemoveCheckboxValue(getResources().getString(R.string.text_platforms), v,
                            getResources().getString(R.string.text_youtube));
                    break;

                case R.id.checkbox_add_streamer_platforms_twitch:
                    addRemoveCheckboxValue(getResources().getString(R.string.text_platforms), v,
                            getResources().getString(R.string.text_twitch));
                    break;

                case R.id.checkbox_add_streamer_platforms_facebook:
                    addRemoveCheckboxValue(getResources().getString(R.string.text_platforms), v,
                            getResources().getString(R.string.text_facebook));
                    break;
            }
        }
    };

    /**
     * This should check if the checkbox is checked or unchecked, so the value sets after that
     *
     * @param mIsFrom       - Is From (Games or Platforms)
     * @param v             - View
     * @param mCheckBoxName - Check Box Field Name
     */
    private void addRemoveCheckboxValue(String mIsFrom, View v, String mCheckBoxName) {
        boolean checked = ((CheckBox) v).isChecked();
        if (checked) {
            mArrSelectedPlatforms.add(mCheckBoxName);
        } else {
            mArrSelectedPlatforms.remove(mCheckBoxName);
        }
    }

    /**
     * This method checks the validation first and then call the API
     */
    @SuppressLint("SetTextI18n")
    private void doAddStreamer() {
        hideSoftKeyboard();
        if (checkValidation()) {
            if (!mAppUtils.getConnectionState()) {
                mAppUtils.displayNoInternetSnackBar(mRelativeMainView, new AppCallbackListener(this));
            } else {
                callToAddStreamerAPI();
            }
        }
    }

    /**
     * Hide the Soft Keyboard
     */
    public void hideSoftKeyboard() {
        try {
            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should get all the Platforms when checked
     *
     * @return - Return Selected Platform Data
     */
    private JSONArray getGames() {
        JSONArray jsonArrayGames;

        jsonArrayGames = new JSONArray();
        for (int i = 0; i < mArrSelectedGames.size(); i++) {
            jsonArrayGames.put(mArrSelectedGames.get(i));
        }
        return jsonArrayGames;
    }

    /**
     * This should get all the Platforms when checked
     *
     * @return - Return Selected Platform Data
     */
    private JSONArray getPlatforms() {
        JSONArray jsonArrayPlatforms;

        jsonArrayPlatforms = new JSONArray();
        for (int i = 0; i < mArrSelectedPlatforms.size(); i++) {
            jsonArrayPlatforms.put(mArrSelectedPlatforms.get(i));
        }
        return jsonArrayPlatforms;
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        mName = mEditName.getText().toString().trim();
        mYoutubeChannelName = mEditYoutubeChannelName.getText().toString().trim();
        mYoutubeChannelLink = mEditYoutubeChannelLink.getText().toString().trim();
        mOtherGameName = mEditOtherGameName.getText().toString().trim();

        mEditName.setError(null);
        mEditYoutubeChannelName.setError(null);
        mEditYoutubeChannelLink.setError(null);

        if (TextUtils.isEmpty(mName)) {
            status = false;
            mEditName.setError(getResources().getString(R.string.error_field_required));
        }

        if (TextUtils.isEmpty(mYoutubeChannelName)) {
            status = false;
            mEditYoutubeChannelName.setError(getResources().getString(R.string.error_field_required));
        }

        if (TextUtils.isEmpty(mYoutubeChannelLink)) {
            status = false;
            mEditYoutubeChannelLink.setError(getResources().getString(R.string.error_field_required));
        }

        if (mCardViewOtherGameName.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(mOtherGameName)) {
                status = false;
                mEditOtherGameName.setError(getResources().getString(R.string.error_field_required));
            }
        } else {
            mEditOtherGameName.setError(null);
        }

        if (status) {

            if (!mYoutubeChannelLink.contains("https://www.youtube.com/") && !mYoutubeChannelLink.contains("https://m.youtube.com/")) {
                status = false;
                Constants.SnakeMessageYellow(mRelativeMainView, getResources().getString(R.string.error_valid_youtube_url));
            }

            if (!mOthersSelected) {
                if (mArrSelectedGames.size() == 0) {
                    status = false;
                    Constants.SnakeMessageYellow(mRelativeMainView, getResources().getString(R.string.error_game_required));
                }
            }

            if (mArrSelectedPlatforms.size() == 0) {
                status = false;
                Constants.SnakeMessageYellow(mRelativeMainView, getResources().getString(R.string.error_valid_youtube_url));
            }
        }
        return status;
    }

    /**
     * This should add or change username
     */
    private void callToGetGamesAPI() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setGetGamesJson(mUserId));

            Call<GetGamesModel> call = APIClient.getClient().create(APIInterface.class).getGames(body);
            call.enqueue(new Callback<GetGamesModel>() {
                @Override
                public void onResponse(@NonNull Call<GetGamesModel> call, @NonNull Response<GetGamesModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);

                        String mStatus = jsonObject.getString(WebFields.STATUS);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);

                        assert response.body() != null;
                        if (mStatus.equalsIgnoreCase(Constants.API_SUCCESS)) {
                            ArrayList<GetGamesModel.Data.GamesData> gameTypesModels = response.body().getData().getGamesData();
                            mArrGames.addAll(gameTypesModels);

                            GetGamesModel.Data.GamesData gamesData = new GetGamesModel.Data.GamesData();
                            gamesData.setName(getResources().getString(R.string.text_others));
                            mArrGames.add(gamesData);
                            setAdapterData();
                        } else {
                            Constants.SnakeMessageYellow(mRelativeMainView, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetGamesModel> call, @NonNull Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setNestedScrollingEnabled(true);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(AddStreamerActivity.this));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());

            GamesAdapter mAdapterGames = new GamesAdapter(AddStreamerActivity.this, mArrGames);
            mRecyclerView.setAdapter(mAdapterGames);
            mAdapterGames.setOnCheckboxItemClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should add streamer
     */
    private void callToAddStreamerAPI() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(AddStreamerActivity.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            String mNewCountryCode = "+" + mCountryCode;

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAddStreamerJson(mUserId, mName, mNewCountryCode, mMobileNo, mEmail, mGender,
                            mYoutubeChannelName, mYoutubeChannelLink, mOtherGameName, getGames(), getPlatforms()));

            Call<AddStreamerModel> call = APIClient.getClient().create(APIInterface.class).addStreamer(body);
            call.enqueue(new Callback<AddStreamerModel>() {
                @Override
                public void onResponse(@NonNull Call<AddStreamerModel> call, @NonNull Response<AddStreamerModel> response) {

                    progressDialog.dismiss();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);

                        String mStatus = jsonObject.getString(WebFields.STATUS);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);

                        assert response.body() != null;
                        if (mStatus.equalsIgnoreCase(Constants.API_SUCCESS)) {
                            openPopup(mMessage);
                        } else {
                            Constants.SnakeMessageYellow(mRelativeMainView, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddStreamerModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should open the popup and set the message accordingly.
     */
    private void openPopup(String mMessage) {
        try {
            final Dialog dialog = new Dialog(AddStreamerActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.success_dialog);
            Window window = dialog.getWindow();
            assert window != null;
            window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            TextView mTextMessage = dialog.findViewById(R.id.txt_success_message);
            mTextMessage.setText(mMessage);
            RelativeLayout lyt_done = dialog.findViewById(R.id.lyt_done);
            lyt_done.setOnClickListener(v ->
            {
                dialog.cancel();
                finish();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAppCallback(int Code) {
        doAddStreamer();
    }

    @Override
    public void onCheckboxItemClickListener(ArrayList<String> mArrSelGames, boolean mIsOthersChecked) {
        mOthersSelected = mIsOthersChecked;

        if (mArrSelectedGames != null) {
            mArrSelectedGames = mArrSelGames;
        }

        if (mIsOthersChecked) {
            mCardViewOtherGameName.setVisibility(View.VISIBLE);
        } else {
            mEditOtherGameName.setText("");
            mCardViewOtherGameName.setVisibility(View.GONE);
        }
    }
}