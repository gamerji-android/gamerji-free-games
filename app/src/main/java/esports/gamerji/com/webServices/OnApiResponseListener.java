package esports.gamerji.com.webServices;

public interface OnApiResponseListener<T> {

    /**
     * On response complete.
     *
     * @param clsGson     the cls gson
     * @param requestCode the request code
     */
    void onResponseComplete(T clsGson, int requestCode);

    /**
     * On response error.
     *
     * @param errorMessage the error message
     * @param requestCode  the request code
     */
    void onResponseError(String errorMessage, int requestCode, int responseCode);
}