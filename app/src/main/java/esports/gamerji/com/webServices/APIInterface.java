package esports.gamerji.com.webServices;

import com.google.gson.JsonElement;

import esports.gamerji.com.BuildConfig;
import esports.gamerji.com.model.AddStreamerModel;
import esports.gamerji.com.model.AddVideosModel;
import esports.gamerji.com.model.AppMaintenanceStatusModel;
import esports.gamerji.com.model.CustomerCareModel;
import esports.gamerji.com.model.CustomerIssuesModel;
import esports.gamerji.com.model.CustomerTicketsModel;
import esports.gamerji.com.model.DashboardModel;
import esports.gamerji.com.model.GamerProfileModel;
import esports.gamerji.com.model.GamerjiProPopupModel;
import esports.gamerji.com.model.GeneralResponseModel;
import esports.gamerji.com.model.GetAllGamesModel;
import esports.gamerji.com.model.GetGamesModel;
import esports.gamerji.com.model.GetMessagesModel;
import esports.gamerji.com.model.GetStreamerStatusModel;
import esports.gamerji.com.model.HostStreamVideosModel;
import esports.gamerji.com.model.LeaderboardModel;
import esports.gamerji.com.model.PopularVideosModel;
import esports.gamerji.com.model.QuickGameEndGameModel;
import esports.gamerji.com.model.QuickGameJoinGameModel;
import esports.gamerji.com.model.QuickGameJoinGamePopupModel;
import esports.gamerji.com.model.QuickGameSubTypesModel;
import esports.gamerji.com.model.QuickGameTypesModel;
import esports.gamerji.com.model.ReferAFriendModel;
import esports.gamerji.com.model.ResendOTPModel;
import esports.gamerji.com.model.SearchModel;
import esports.gamerji.com.model.SignInModel;
import esports.gamerji.com.model.SignUpModel;
import esports.gamerji.com.model.StatesListModel;
import esports.gamerji.com.model.TCModel;
import esports.gamerji.com.model.ViewAllPopularVideosModel;
import esports.gamerji.com.utils.Constants;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface APIInterface {
    @POST(Constants.SIGNUP_USER)
    @FormUrlEncoded
    Call<SignInModel> doSignUp(@Field("Action") String Action, @Field("Val_Ucountrycode") String Val_Ucountrycode, @Field("Val_Umobilenumber") String Val_Umobilenumber,
                               @Field("Val_Uemailaddress") String Val_Uemailaddress, @Field("Val_Upassword") String Val_Upassword, @Field("Val_Ufirstname") String Val_Ufirstname, @Field("Val_Ulastname") String Val_Ulastname,
                               @Field("Val_Signupcode") String Val_Coupon, @Field("Val_Type") String Val_Type, @Field("Val_Ufacebookid") String Val_Ufacebookid, @Field("Val_Ugoogleid") String Val_Ugoogleid);

    @POST(Constants.CHECK_MOBILE)
    @FormUrlEncoded
    Call<SignUpModel> checkMobile(@Field("Action") String Action, @Field("Val_Ucountrycode") String Val_Ucountrycode, @Field("Val_Umobilenumber") String Val_Umobilenumber,
                                  @Field("Val_Uemailaddress") String Val_Uemailaddress);

    @POST(Constants.CHECK_MOBILE)
    @FormUrlEncoded
    Call<SignUpModel> applyPromo(@Field("Action") String Action, @Field("Val_Signupcode") String Val_Signupcode);

    @POST(Constants.UPDATE_MOBILE)
    @FormUrlEncoded
    Call<SignInModel> updateMobile(@Field("Action") String Action, @Field("Val_Umobilenumber") String Val_Umobilenumber, @Field("Val_Ucountrycode") String Val_Ucountrycode,
                                   @Field("Val_Userid") String Val_Userid, @Field("Val_Signupcode") String Val_Signupcode);

    @POST(Constants.SIGNIN_USER)
    @FormUrlEncoded
    Call<SignInModel> doEmailSignIn(@Field("Val_Type") int Val_Type, @Field("Val_Uemailaddress") String Val_Uemailaddress, @Field("Val_Upassword") String Val_Upassword);

    @POST(Constants.SIGNIN_USER)
    @FormUrlEncoded
    Call<SignInModel> doMobileSignIn(@Field("Val_Type") int Val_Type, @Field("Val_Ucountrycode") String Val_Ucountrycode, @Field("Val_Umobilenumber") String Val_Umobilenumber);

    @POST(Constants.V3_DASHBOARD)
    @FormUrlEncoded
    Call<DashboardModel> getDashboard(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Apptype") String Val_Apptype, @Field("Val_Appversion") String Val_Appversion);

    @POST(Constants.RESEND_OTP)
    @FormUrlEncoded
    Call<ResendOTPModel> resendOTP(@Field("Action") String Action, @Field("Val_Ucountrycode") String Val_Ucountrycode,
                                   @Field("Val_Umobilenumber") String Val_Umobilenumber);

    @POST(Constants.FORGOT_OTP)
    @FormUrlEncoded
    Call<SignInModel> forgotOTP(@Field("Action") String Action, @Field("Val_Ucountrycode") String Val_Ucountrycode,
                                @Field("Val_Umobilenumber") String Val_Umobilenumber);

    @POST(Constants.GENERAL_FETCH)
    @FormUrlEncoded
    Call<StatesListModel> getAllStates(@Field("Action") String Action);

    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<CustomerIssuesModel> getGetCustomerIssues(@Field("Action") String Action);

    @POST(Constants.GAME_FETCH_V3)
    @FormUrlEncoded
    Call<GetAllGamesModel> getAllGames(@Field("Action") String Action, @Field("Val_Userid") String value);

    @POST(Constants.LEADERBOARD_V2)
    @FormUrlEncoded
    Call<LeaderboardModel> getLeaderBoard(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Page") String Val_Page, @Field("Val_Limit") String Val_Limit, @Field("Val_Type") String Val_Type,
                                          @Field("Val_Rank") String Val_Rank, @Field("Val_Points") String Val_Points);

    @POST(Constants.SEARCH)
    @FormUrlEncoded
    Call<SearchModel> searchUser(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Keyword") String Val_Keyword);

    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<CustomerTicketsModel> getCustomerTickets(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Page") int Val_Page, @Field("Val_Limit") int Val_Limit);

    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<GetMessagesModel> getMessages(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Ticketid") String Val_Ticketid, @Field("Val_Page") int Val_Page, @Field("Val_Limit") int Val_Limit);

    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<GetMessagesModel> getNewCustomerCareMessage(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Ticketid") String Val_Ticketid, @Field("Val_Messageid") String Val_Messageid);

    @POST(Constants.GENERAL_DETAILS)
    @FormUrlEncoded
    Call<GetMessagesModel.Data> sendMessages(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Ticketid") String Val_Ticketid, @Field("Val_Message") String Val_Message, @Field("Val_Appversion") String Val_Appversion,
                                             @Field("Val_Devicetype") String Val_Devicetype);

    @POST(Constants.V3_GENERAL_FETCH)
    @FormUrlEncoded
    Call<TCModel> getTerms(@Field("Action") String Action);

    @POST(Constants.V3_USER_FETCH)
    @FormUrlEncoded
    Call<GamerProfileModel> getGamerProfile(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid);

    @POST(Constants.RESET_PASSWORD)
    @FormUrlEncoded
    Call<SignInModel> resetPassword(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                    @Field("Val_Upassword") String Val_Upassword);

    @POST(Constants.V3_USER_FETCH)
    @FormUrlEncoded
    Call<ReferAFriendModel> getUserRefferal(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid);

    @POST(Constants.USER_FETCH)
    @FormUrlEncoded
    Call<SignInModel> getProfile(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid);

    @GET()
    Call<JsonElement> getJsonFile(@Url String url);

    @POST(Constants.UPDATE_MOBILE)
    @FormUrlEncoded
    Call<GeneralResponseModel> sendToken(@Field("Action") String action, @Field("Val_Userid") String token, @Field("Val_Udevicetype") String Val_Udevicetype, @Field("Val_Ufirebasetoken") String Val_Ufirebasetoken);

    // Get Games API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.GAME_FETCH_RAW)
    Call<GetGamesModel> getGames(@Body RequestBody body);

    // Add Streamer API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.STREAMER_DETAILS)
    Call<AddStreamerModel> addStreamer(@Body RequestBody body);

    // Get Streamer Status API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.STREAMER_FETCH)
    Call<GetStreamerStatusModel> getStreamerStatus(@Body RequestBody body);

    // Get Host Stream Videos API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.GENERAL_FETCH_RAW)
    Call<HostStreamVideosModel> getHostStreamVideos(@Body RequestBody body);

    // Add Videos API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.STREAMER_DETAILS)
    Call<AddVideosModel> addVideos(@Body RequestBody body);

    // Get Customer Care API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.GENERAL_FETCH_RAW)
    Call<CustomerCareModel> getCustomerCare(@Body RequestBody body);

    // App Maintenance Status API Interface
    @Headers({"Content-Type: application/json"})
    @POST(BuildConfig.MAINTENANCE_URL)
    Call<AppMaintenanceStatusModel> getAppMaintenanceStatus();

    // GamerJi Pro Popup API Interface
    @POST(Constants.V3_DASHBOARD)
    @FormUrlEncoded
    Call<GamerjiProPopupModel> getGamerJiProPopup(@Field(WebFields.GAMERJI_PRO_POPUP.REQUEST_ACTION) String Action, @Field(WebFields.GAMERJI_PRO_POPUP.REQUEST_VAL_USER_ID) String Val_Userid);

    // Quick Game Types API Interface
    @POST(Constants.H5_GAMES_FETCH)
    @FormUrlEncoded
    Call<QuickGameTypesModel> getQuickGameTypes(@Field(WebFields.QUICK_GAME_TYPES.REQUEST_ACTION) String Action, @Field(WebFields.QUICK_GAME_TYPES.REQUEST_VAL_USER_ID) String Val_Userid);

    // Quick Game Sub Types API Interface
    @POST(Constants.H5_GAMES_FETCH)
    @FormUrlEncoded
    Call<QuickGameSubTypesModel> getQuickGameSubTypes(@Field(WebFields.QUICK_GAME_SUB_TYPES.REQUEST_ACTION) String Action, @Field(WebFields.QUICK_GAME_SUB_TYPES.REQUEST_VAL_USER_ID) String Val_Userid,
                                                      @Field(WebFields.QUICK_GAME_SUB_TYPES.REQUEST_CATEGORY_ID) String Val_Categoryid, @Field(WebFields.QUICK_GAME_SUB_TYPES.REQUEST_VAL_PAGE) String Val_Page,
                                                      @Field(WebFields.QUICK_GAME_SUB_TYPES.REQUEST_VAL_LIMIT) String Val_Limit);

    // Quick Game - Join Game Popup API Interface
    @POST(Constants.V3_USER_FETCH)
    @FormUrlEncoded
    Call<QuickGameJoinGamePopupModel> quickGameJoinGamePopup(@Field(WebFields.QUICK_GAME_JOIN_GAME_POPUP.REQUEST_ACTION) String Action, @Field(WebFields.QUICK_GAME_JOIN_GAME_POPUP.REQUEST_VAL_USER_ID) String Val_Userid,
                                                             @Field(WebFields.QUICK_GAME_JOIN_GAME_POPUP.REQUEST_VAL_H5_GAME_ID) String Val_H5Gameid);

    // Quick Game - Join Game API Interface
    @POST(Constants.H5_GAMES_DETAILS)
    @FormUrlEncoded
    Call<QuickGameJoinGameModel> quickGameJoinGame(@Field(WebFields.QUICK_GAME_JOIN_GAME.REQUEST_ACTION) String Action, @Field(WebFields.QUICK_GAME_JOIN_GAME.REQUEST_VAL_USER_ID) String Val_Userid,
                                                   @Field(WebFields.QUICK_GAME_JOIN_GAME.REQUEST_VAL_H5_GAME_ID) String Val_H5Gameid, @Field(WebFields.QUICK_GAME_JOIN_GAME.REQUEST_VAL_H5_CATEGORY_ID) String Val_H5Categoryid);

    // Quick Game - End Game API Interface
    @POST(Constants.H5_GAMES_DETAILS)
    @FormUrlEncoded
    Call<QuickGameEndGameModel> quickGameEndGame(@Field(WebFields.QUICK_GAME_END_GAME.REQUEST_ACTION) String Action, @Field(WebFields.QUICK_GAME_END_GAME.REQUEST_VAL_USER_ID) String Val_Userid,
                                                 @Field(WebFields.QUICK_GAME_END_GAME.REQUEST_VAL_H5_GAME_ID) String Val_H5Gameid, @Field(WebFields.QUICK_GAME_END_GAME.REQUEST_VAL_H5_CATEGORY_ID) String Val_H5Categoryid,
                                                 @Field(WebFields.QUICK_GAME_END_GAME.REQUEST_VAL_JOINED_ID) String Val_Joinedid);

    // Get Popular Videos API Interface
    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<PopularVideosModel> getPopularVideos(@Field(WebFields.GET_POPULAR_VIDEOS.REQUEST_ACTION) String Action, @Field(WebFields.GET_POPULAR_VIDEOS.REQUEST_VAL_USER_ID) String Val_Userid,
                                              @Field(WebFields.GET_POPULAR_VIDEOS.REQUEST_VAL_TYPE) String Val_Type, @Field(WebFields.GET_POPULAR_VIDEOS.REQUEST_VAL_PAGE) String Val_Page,
                                              @Field(WebFields.GET_POPULAR_VIDEOS.REQUEST_VAL_LIMIT) String Val_Limit);


    // Get View All Popular Videos API Interface
    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<ViewAllPopularVideosModel> getViewAllPopularVideos(@Field(WebFields.GET_VIEW_ALL_POPULAR_VIDEOS.REQUEST_ACTION) String Action, @Field(WebFields.GET_VIEW_ALL_POPULAR_VIDEOS.REQUEST_VAL_USER_ID) String Val_Userid,
                                                           @Field(WebFields.GET_VIEW_ALL_POPULAR_VIDEOS.REQUEST_VAL_CHANNEL_ID) String Val_YTChannelid, @Field(WebFields.GET_VIEW_ALL_POPULAR_VIDEOS.REQUEST_VAL_TYPE) String Val_Type,
                                                           @Field(WebFields.GET_VIEW_ALL_POPULAR_VIDEOS.REQUEST_VAL_PAGE) String Val_Page, @Field(WebFields.GET_VIEW_ALL_POPULAR_VIDEOS.REQUEST_VAL_LIMIT) String Val_Limit);
}