package esports.gamerji.com.utils;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import esports.gamerji.com.R;

public class AppUtil {

    private Context mContext;

    public AppUtil(Context Context) {
        mContext = Context;
    }

    /**
     * Display snack bar for network error
     *
     * @param View - View
     */
    public void displayNoInternetSnackBar(View View, final AppCallbackListener listener) {
        final Snackbar snackbar = Snackbar
                .make(View, mContext.getResources().getString(R.string.alert_no_internet_connection), Snackbar.LENGTH_LONG);

        snackbar.setAction(mContext.getResources().getString(R.string.action_retry), new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                snackbar.dismiss();
                listener.onAppCallback(Constants.SNACK_BAR_RETRY);
            }
        });

        // Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        android.view.View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    /***
     * Display snack bar for network error
     *
     * @param View - View
     */
    public void displayNoInternetSnackBar(View View) {
        try {
            Snackbar snackbar = Snackbar
                    .make(View, mContext.getResources().getString(R.string.alert_no_internet_connection), Snackbar.LENGTH_LONG)
                    /*.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    })*/;

            // Changing message text color
            snackbar.setActionTextColor(Color.RED);

            // Changing action button text color
            android.view.View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * Display message
     *
     * @param view - View
     * @param Message - Message
     */
    public static void displaySnackBarWithMessage(View view, String Message) {
        try {
            Snackbar.make(view, Message, Snackbar.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * To check the internet connection
     *
     * @return - Return boolean value according to the network visibility
     */
    public boolean getConnectionState() {
        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        return ni != null;
    }
}

