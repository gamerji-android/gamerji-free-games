package esports.gamerji.com.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.androidadvance.topsnackbar.TSnackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import esports.gamerji.com.R;
import esports.gamerji.com.model.SignInModel;

public class Constants {
    public static String PLAYSTORE = "Playstore";
    public static String Apptype;
    public static final String SIGNUP_USER = "Authentication/SignUp/";
    public static final String SIGNIN_USER = "Authentication/SignIn/";
    public static final String CHECK_MOBILE = "Authentication/UserSettings";
    public static final String UPDATE_MOBILE = "Authentication/UserSettings/";
    public static final String V3_DASHBOARD = "v3/User/Dashboard/";
    public static final String RESEND_OTP = "Authentication/ResendOTP/";
    public static final String FORGOT_OTP = "Authentication/Forgot/";
    public static final String GENERAL_FETCH = "General/Fetch";
    public static final String V3_GENERAL_FETCH = "v3/General/Fetch";
    public static final String UPDATE_PROFILE_V3 = "v3/User/Profile";
    public static final String LEADERBOARD_V2 = "v2/User/Leaderboard/";
    public static final String SEARCH = "v2/User/Fetch";
    public static final String USER_FETCH = "User/Fetch";
    public static final String V3_USER_FETCH = "v3/User/Fetch";
    public static final String RESET_PASSWORD = "Authentication/Forgot/";
    public static final String GENERAL_DETAILS = "v3/General/Details";
    public static final String GENERAL_FETCH_V3 = "v3/General/Fetch";
    public static final String GAME_FETCH_V3 = "v3/Game/Fetch";
    public static final String REGISTER_DEVICE = "RegisterDevice";
    public static final String GAME_FETCH_RAW = "v3/Game/FetchRaw";
    public static final String STREAMER_FETCH = "v3/Streamer/Fetch";
    public static final String STREAMER_DETAILS = "v3/Streamer/Details";
    public static final String GENERAL_FETCH_RAW = "v3/General/FetchRaw";
    public static final String H5_GAMES_FETCH = "v3/H5G/Fetch";
    public static final String H5_GAMES_DETAILS = "v3/H5G/Details";

    public static final String UserID = "UserID";
    public static final String CountryCode = "CountryCode";
    public static final String FullName = "FullName";
    public static final String firstname = "firstname";
    public static final String lastname = "lastName";
    public static final String MobileNumber = "MobileNumber";
    public static final String EmailAddress = "EmailAddress";
    public static final String LevelIcon = "LevelIcon";
    public static final String TeamName = "TeamName";
    public static final String State = "State";
    public static final String BirthDate = "BirthDate";
    public static final String ProfileImage = "ProfileImage";
    public static final String Status = "Status";
    public static final String CanPlayGames = "CanPlayGames";
    public static final String Check = "Check";
    public static final String ValidateSignupCode = "ValidateSignupCode";
    public static final String IS_LOGIN = "login";
    public static final String SOCIAL_LOGIN = "social_login";
    public static final String FILENAME = "FantasyJieSport";
    public static final String REQUEST = "Request";
    public static final String GET_ALL_GAMES = "GetAllGames";
    public static final String GET_ALL_STATES = "GetAllStates";
    public static final String GET_CUSTOMER_ISSUES = "GetCustomerIssues";
    public static final String USER_GAME_DATA = "UserGameData";
    public static final String RESET_PWD = "ResetPassword";
    public static final String UPDATEMOBILE = "UpdateMobile";
    public static final String UPDATE_USER_PROFILE = "UpdateProfile";
    public static final String GET_GAMER_PROFILE = "GetGamerProfile";
    public static final String GENERATE_OTP = "GenerateOTP";
    public static final String GET_REFERRAL = "GetReferral";
    public static final String SUBMIT_REQUEST = "SubmitRequest";
    public static final String GET_TERMS = "GetTerms";
    public static final String GET_PRIVACY = "GetPrivacyPolicy";
    public static final String ADD_HISTORY = "addHistory";
    public static final String GET_CUSTOMER_TICKETS = "GetCustomerTickets";
    public static final String GET_CUSTOMER_TICKET_HISTORY = "GetCustomerTicketHistory";
    public static final String GET_CUSTOMER_TICKET_NEW_MSG = "GetCustomerTicketNewHistory";
    public static final String CUSTOMER_SEND_MESSAGE = "CCTSendMessage";
    public static final String APP_VERSION = "CheckAppVersion";
    public static final String GET_PROFILE = "GetProfile";
    public static final String SEARCH_USER = "searchUsers";
    public static final String GETDATA = "GetData";
    public static final String FACEBOOK = "2";
    public static final String GOOGLE = "3";
    public static final String BASIC = "1";
    public static final String MOBILE_VERIFICATION_PENDING = "3";

    public static String API_SUCCESS = "success";

    public static String VIDEOS_POPULAR = "2";
    public static String VIDEOS_HOST_STREAMS = "3";

    public static final String TODAY = "1";
    public static final String WEEKLY = "2";
    public static final String MONTHLY = "3";
    public static final String ALLTIME = "4";

    public static final String AD_IMPRESSION = "1";
    public static final String AD_CLICK = "2";
    public static final String AD_PLAY = "3";
    public static int SNACK_BAR_RETRY = 101;

    public static ArrayList<SignInModel.UserData.GamesData.GameData> getUserData(Context context) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = sharedPrefs.getString(USER_GAME_DATA, "");
        Type type = new TypeToken<List<SignInModel.UserData.GamesData.GameData>>() {
        }.getType();
        //ArrayList<SignInModel.UserData.GamesData.GameData> userGameDataarrayList = gson.fromJson(json, type);
        return gson.fromJson(json, type);
    }

    public static void setUserGameData(ArrayList<SignInModel.UserData.GamesData.GameData> gamesDataArrayList, Context context) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        Gson gson = new Gson();

        String json = gson.toJson(gamesDataArrayList);

        editor.putString(USER_GAME_DATA, json);
        editor.apply();
        editor.commit();
        // getUserData();
    }

    public static void SnakeMessageYellow(View view, String mMessage) {
        try {
            if (view == null || mMessage == null || mMessage.isEmpty())
                return;
            TSnackbar snackbar = TSnackbar.make(view, mMessage, TSnackbar.LENGTH_SHORT);

            snackbar.getView().setBackgroundColor(Color.parseColor("#E29618"));
            TextView textView = snackbar.getView().findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
            Typeface typeface = ResourcesCompat.getFont(view.getContext(), R.font.poppins);
            textView.setMaxLines(4);
            textView.setTypeface(typeface);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /*@Override
    public void onLowMemory() {
        super.onLowMemory();
        AppLog.e(TAG, "onLowMemory()");
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        AppLog.e(TAG, "onTrimMemory()");
        AppLog.e(TAG, "level: " + level);
    }*/
}
