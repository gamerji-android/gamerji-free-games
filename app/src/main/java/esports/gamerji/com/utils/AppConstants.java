package esports.gamerji.com.utils;

public class AppConstants {

    // Games
    public final static String GAME_P_MOBILE = "P MOBILE";
    public final static String GAME_CLASH_ROYALE = "Clash Royale";

    // Bundle Keys
    public final static String BUNDLE_STREAM_NAME = "StreamName";
    public final static String BUNDLE_STATUS = "Status";
    public final static String BUNDLE_STREAMER_STATUS = "StreamerStatus";
    public final static String BUNDLE_STREAMER_REASON = "StreamerReason";
    public final static String BUNDLE_STREAMER_DISPLAY_STATUS = "StreamerDisplayStatus";
    public final static String BUNDLE_QUICK_GAME_TYPE = "QuickGameType";
    public final static String BUNDLE_QUICK_GAME_CATEGORY_ID = "QuickGameCategoryId";
    public final static String BUNDLE_QUICK_GAME_ID = "QuickGameId";
    public final static String BUNDLE_QUICK_GAME_URL = "QuickGameURL";
    public final static String BUNDLE_QUICK_GAME_DISPLAY_MODE = "QuickGameDisplayMode";
    public final static String BUNDLE_QUICK_GAME_JOINED_ID = "QuickGamejoinedId";
    public final static String BUNDLE_VIEW_ALL_VIDEOS_CHANNEL_ID = "ViewAllVideosChannelId";
}
