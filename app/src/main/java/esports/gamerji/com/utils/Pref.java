package esports.gamerji.com.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Pref {

//	private static SharedPreferences sharedPreferences = null;


    public static SharedPreferences openPref_(Context contex, String mPrefName) {
        SharedPreferences sharedPreferences = contex.getSharedPreferences(mPrefName,
                Context.MODE_PRIVATE);

        return sharedPreferences;

    }

    public static String getValue(Context context, String key,
                                  String defaultValue, String mFileName) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(mFileName,
                Context.MODE_PRIVATE);
        String result = sharedPreferences.getString(key, defaultValue);
        return result;
    }

    public static void setValue(Context context, String key, String value, String mFileName) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(mFileName,
                Context.MODE_PRIVATE);
        Editor prefsPrivateEditor = sharedPreferences.edit();
        prefsPrivateEditor.putString(key, value);
        prefsPrivateEditor.commit();
//		prefsPrivateEditor = null;
//		Pref.sharedPreferences = null;
    }

    public static boolean getValue(Context context, String key,
                                   boolean defaultValue, String mFileName) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(mFileName,
                Context.MODE_PRIVATE);
        boolean result = sharedPreferences.getBoolean(key, defaultValue);
//		Pref.sharedPreferences = null;
        return result;
    }

    public static void setValue(Context context, String key, boolean value, String mFileName) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(mFileName,
                Context.MODE_PRIVATE);
        Editor prefsPrivateEditor = sharedPreferences.edit();
        prefsPrivateEditor.putBoolean(key, value);
        prefsPrivateEditor.commit();
//		prefsPrivateEditor = null;
//		Pref.sharedPreferences = null;
    }


    public static boolean hasKey(Context _Context, String mKeyName, String PrefFileName, String mFileName) {
        SharedPreferences sharedPreferences = _Context.getSharedPreferences(mFileName,
                Context.MODE_PRIVATE);

        boolean isContain = sharedPreferences.contains(mKeyName);

        sharedPreferences = null;

        return isContain;
    }


    public static void setValue(Context context, String key, int value, String mFileName) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(mFileName,
                Context.MODE_PRIVATE);
        Editor prefsPrivateEditor = sharedPreferences.edit();
        prefsPrivateEditor.putInt(key, value);
        prefsPrivateEditor.commit();
//		prefsPrivateEditor = null;
//		Pref.sharedPreferences = null;
    }

    public static void setValueLong(Context context, String key, long value, String mFileName) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(mFileName,
                Context.MODE_PRIVATE);
        Editor prefsPrivateEditor = sharedPreferences.edit();
        prefsPrivateEditor.putLong(key, value);
        prefsPrivateEditor.commit();
//		prefsPrivateEditor = null;
//		Pref.sharedPreferences = null;
    }

    public static long getValueLong(Context context, String key,
                                    long defaultValue, String mFileName) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(mFileName,
                Context.MODE_PRIVATE);
        long result = sharedPreferences.getLong(key, 1l);
//		Pref.sharedPreferences = null;
        return result;
    }


    public static int getValue(Context context, String key,
                               int defaultValue, String mFileName) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(mFileName,
                Context.MODE_PRIVATE);
        int result = sharedPreferences.getInt(key, defaultValue);
//		Pref.sharedPreferences = null;
        return result;
    }

    public static void ClearAllPref(Context context, String mFileName) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(mFileName,
                Context.MODE_PRIVATE);
        Editor prefsPrivateEditor = sharedPreferences.edit();
        prefsPrivateEditor.clear();
        prefsPrivateEditor.apply();
    }

}
