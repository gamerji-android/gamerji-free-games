package esports.gamerji.com.utils;

import android.content.Context;

public class SessionManager {

    /**
     * This should get the User Id from Session Manager
     *
     * @param context - Context
     * @return - Return user Id
     */
    public String getUserId(Context context) {
        return Pref.getValue(context, Constants.UserID, "", Constants.FILENAME);
    }
}
