package esports.gamerji.com.utils;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class Common {

    /**
     * Insert Log
     *
     * @param mMessage - Message of the defined log
     */
    public static void insertLog(String mMessage) {
        Log.e("Tag", mMessage);
    }

    /**
     * Sets the toast message
     *
     * @param context - Context
     * @param message - Message for showing the toast
     */
    public static void setCustomToast(Context context, String message) {
        final Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 5000);
    }
}
