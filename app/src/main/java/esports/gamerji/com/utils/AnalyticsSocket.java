package esports.gamerji.com.utils;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import esports.gamerji.com.BuildConfig;

public class AnalyticsSocket
{
    private Socket mSocket;

    public  AnalyticsSocket()
    {
        try
        {
            mSocket = IO.socket(BuildConfig.SOCKET_URL);
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
    }

    public void off()
    {
    }

    public void on()
    {

    }


    public void connect()
    {
        mSocket.connect();
    }

    public boolean connected()
    {
        return mSocket.connected();
    }

    public void sendAnalytics(String Val_Adtype, String Val_Adid, String Val_Adattachment, String Val_Type, String Val_Userid, int Val_Playtime)
    {
        try
        {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Val_Adtype",Val_Adtype );
            jsonObject.put("Val_Adid",Val_Adid);
            jsonObject.put("Val_Adattachment",Val_Adattachment);
            jsonObject.put("Val_Type",Val_Type);
            jsonObject.put("Val_Userid",Val_Userid);
            jsonObject.put("Val_Playtime", Val_Playtime);
            mSocket.emit(Constants.ADD_HISTORY,jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
}
