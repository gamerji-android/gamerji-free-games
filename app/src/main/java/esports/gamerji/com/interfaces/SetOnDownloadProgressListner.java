package esports.gamerji.com.interfaces;

public interface SetOnDownloadProgressListner
{
    void OnDownloadStarted();

    void OnProgressUpdate(Integer mValue);

    void OnDownloadComplete(String mOutPutPath);

    void OnErrorOccure(String mMessage);
}
