package esports.gamerji.com.interfaces;

import esports.gamerji.com.model.GamerProfileModel;

public interface ProfileGameInfoClickListener {
    void OnBadgeClickListener(GamerProfileModel.Data.LevelsData.LevelData badgesList);
}
