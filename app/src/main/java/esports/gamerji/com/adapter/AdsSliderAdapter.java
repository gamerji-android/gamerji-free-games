package esports.gamerji.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import esports.gamerji.com.R;
import esports.gamerji.com.model.AdsDataModel;
import esports.gamerji.com.utils.AnalyticsSocket;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.Pref;

public class AdsSliderAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<AdsDataModel> adsDataModelArrayList;
    private LayoutInflater inflater;
    private AnalyticsSocket analyticsSocket;

    public AdsSliderAdapter(Context context, ArrayList<AdsDataModel> adsDataModelArrayList, AnalyticsSocket analyticsSocket) {
        this.context = context;
        this.adsDataModelArrayList = adsDataModelArrayList;
        this.analyticsSocket = analyticsSocket;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View textLayout = inflater.inflate(R.layout.ads_image_slider, view, false);

        assert textLayout != null;
        ImageView img_main = textLayout.findViewById(R.id.img_banner);
        img_main.setOnClickListener(v ->
        {
            if (analyticsSocket.connected()) {
                analyticsSocket.sendAnalytics(adsDataModelArrayList.get(position).AdType, adsDataModelArrayList.get(position).AdID,
                        adsDataModelArrayList.get(position).AdImageName, Constants.AD_CLICK, Pref.getValue(context, Constants.UserID, "", Constants.FILENAME),
                        0);
            }

            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(adsDataModelArrayList.get(position).AdURL));
            context.startActivity(i);
        });
        Glide.with(context).load(adsDataModelArrayList.get(position).AdImage).into(img_main);
        view.addView(textLayout, 0);

        return textLayout;
    }

    @Override
    public int getCount() {
        return adsDataModelArrayList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
