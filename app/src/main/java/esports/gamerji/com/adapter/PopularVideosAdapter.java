package esports.gamerji.com.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import esports.gamerji.com.R;
import esports.gamerji.com.activity.ViewAllPopularVideosActivity;
import esports.gamerji.com.model.PopularVideosModel;
import esports.gamerji.com.utils.AppConstants;

public class PopularVideosAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<PopularVideosModel.Data.YTChannelsData> mArrPopularVideos;
    private ArrayList<PopularVideosModel.Data.YTChannelsData.VideosData> mArrPopularChildVideos;

    /**
     * Adapter contains the data to be displayed
     */
    public PopularVideosAdapter(Activity mActivity, ArrayList<PopularVideosModel.Data.YTChannelsData> mArrPopularVideos) {
        this.mActivity = mActivity;
        this.mArrPopularVideos = mArrPopularVideos;
        mArrPopularChildVideos = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .row_popular_videos_item, parent, false);
        return new CustomViewHolder(itemView);
    }

    @SuppressLint({"SetJavaScriptEnabled", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final PopularVideosModel.Data.YTChannelsData popularVideosParentModel = mArrPopularVideos.get(position);

        if (position % 2 == 0) {
            ((CustomViewHolder) holder).mRelativeMainView.setBackgroundResource(R.drawable.rounded_corners_grey_video);
        } else {
            ((CustomViewHolder) holder).mRelativeMainView.setBackgroundResource(R.drawable.rounded_corners_orange_video);
        }

        ((CustomViewHolder) holder).mTextChannelName.setText(popularVideosParentModel.getYTChannelName());
        ((CustomViewHolder) holder).mTextTotalVideos.setText(popularVideosParentModel.getTotalVideosCount() > 1 ?
                popularVideosParentModel.getTotalVideosCount() + " " + mActivity.getResources().getString(R.string.text_videos) :
                popularVideosParentModel.getTotalVideosCount() + " " + mActivity.getResources().getString(R.string.text_video));

        // ToDo: Subscribe Button Click
        ((CustomViewHolder) holder).mButtonSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                redirectToYoutubeSubscribe(popularVideosParentModel.getYTChannelLink());
            }
        });

        // ToDo: View More Click
        ((CustomViewHolder) holder).mButtonViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callToViewAllPopularVideosActivity(popularVideosParentModel);
            }
        });

        // ToDo; Binds the Dynamic View
        mArrPopularChildVideos = (ArrayList<PopularVideosModel.Data.YTChannelsData.VideosData>) popularVideosParentModel.getVideosData();

        if (mArrPopularChildVideos.size() == 1) {
            ((CustomViewHolder) holder).mLinearVideosSecond.setVisibility(View.GONE);
            ((CustomViewHolder) holder).mTextVideoTitleFirst.setSelected(true);
            ((CustomViewHolder) holder).mTextVideoTitleFirst.setText(mArrPopularChildVideos.get(0).getTitle());

            setWebViewProperties(((CustomViewHolder) holder).mWebViewFirst);
            ((CustomViewHolder) holder).mWebViewFirst.loadUrl(mArrPopularChildVideos.get(0).getYoutubeLink());
        } else {
            if (mArrPopularChildVideos.size() > 1) {
                ((CustomViewHolder) holder).mLinearVideosSecond.setVisibility(View.VISIBLE);
                ((CustomViewHolder) holder).mTextVideoTitleFirst.setSelected(true);
                ((CustomViewHolder) holder).mTextVideoTitleSecond.setSelected(true);
                ((CustomViewHolder) holder).mTextVideoTitleFirst.setText(mArrPopularChildVideos.get(0).getTitle());
                ((CustomViewHolder) holder).mTextVideoTitleSecond.setText(mArrPopularChildVideos.get(1).getTitle());

                setWebViewProperties(((CustomViewHolder) holder).mWebViewFirst);
                setWebViewProperties(((CustomViewHolder) holder).mWebViewSecond);
                ((CustomViewHolder) holder).mWebViewFirst.loadUrl(mArrPopularChildVideos.get(0).getYoutubeLink());
                ((CustomViewHolder) holder).mWebViewSecond.loadUrl(mArrPopularChildVideos.get(1).getYoutubeLink());
            }
        }
    }

    /**
     * Sets the web view properties
     *
     * @param mWebView - Web View
     */
    @SuppressLint("SetJavaScriptEnabled")
    private void setWebViewProperties(WebView mWebView) {
        try {
            mWebView.setWebChromeClient(new WebChromeClient());
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.getSettings().setLoadsImagesAutomatically(true);
            mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
            mWebView.getSettings().setAllowFileAccess(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirects you to the Youtube Channel Subscribe Page
     */
    private void redirectToYoutubeSubscribe(String mChannelURL) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mChannelURL));
            intent.setPackage("com.google.android.youtube");
            mActivity.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirects you to the View All Popular Videos Activity
     */
    private void callToViewAllPopularVideosActivity(PopularVideosModel.Data.YTChannelsData popularVideosParentModel) {
        try {
            Intent intent = new Intent(mActivity, ViewAllPopularVideosActivity.class);
            intent.putExtra(AppConstants.BUNDLE_VIEW_ALL_VIDEOS_CHANNEL_ID, popularVideosParentModel.getYTChannelID());
            mActivity.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mArrPopularVideos.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout mRelativeMainView;
        TextView mTextChannelName, mTextTotalVideos, mTextVideoTitleFirst, mTextVideoTitleSecond;
        Button mButtonSubscribe, mButtonViewAll;
        LinearLayout mLinearVideosSecond;
        WebView mWebViewFirst, mWebViewSecond;

        @SuppressLint("SetJavaScriptEnabled")
        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Relative Layouts
            mRelativeMainView = itemView.findViewById(R.id.relative_row_popular_videos_main_view);

            // Text Views
            mTextChannelName = itemView.findViewById(R.id.text_row_popular_videos_channel_name);
            mTextTotalVideos = itemView.findViewById(R.id.text_row_popular_videos_total_videos);
            mTextVideoTitleFirst = itemView.findViewById(R.id.text_row_popular_videos_title_first);
            mTextVideoTitleSecond = itemView.findViewById(R.id.text_row_popular_videos_title_second);

            // Buttons
            mButtonSubscribe = itemView.findViewById(R.id.button_row_popular_videos_channel_subscribe);
            mButtonViewAll = itemView.findViewById(R.id.button_row_popular_videos_view_all);

            // Linear Layout
            mLinearVideosSecond = itemView.findViewById(R.id.linear_row_popular_videos_second);

            // Web Views
            mWebViewFirst = itemView.findViewById(R.id.web_view_row_popular_videos_title_first);
            mWebViewSecond = itemView.findViewById(R.id.web_view_row_popular_videos_title_second);
        }
    }
}