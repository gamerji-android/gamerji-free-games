package esports.gamerji.com.adapter;

import android.Manifest;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import esports.gamerji.com.R;
import esports.gamerji.com.activity.ActivityCustomerCareTicket;
import esports.gamerji.com.interfaces.SetOnDownloadProgressListner;
import esports.gamerji.com.model.DashboardModel;
import esports.gamerji.com.utils.Common;
import esports.gamerji.com.utils.FileUtils;

public class ImageSliderAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<DashboardModel.BannersData.Bdata> bdataArrayList;
    private LayoutInflater inflater;
    ProgressBar pb_download_dialog;
    TextView txt_progress_update;
    Dialog mDownloadDialog;

    public ImageSliderAdapter(Context context, ArrayList<DashboardModel.BannersData.Bdata> bdataArrayList) {
        this.context = context;
        this.bdataArrayList = bdataArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View textLayout = inflater.inflate(R.layout.image_slider, view, false);

        assert textLayout != null;
        ImageView img_main = textLayout.findViewById(R.id.img_banner);
        img_main.setOnClickListener(v ->
        {
            Common.insertLog(bdataArrayList.get(position).Name
                    + " " + bdataArrayList.get(position).Type);
            if (bdataArrayList.get(position).Type.equalsIgnoreCase("6")) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(bdataArrayList.get(position).RedirectURL)));
            } if (bdataArrayList.get(position).Type.equalsIgnoreCase("8")) {
                Intent i = new Intent(context, ActivityCustomerCareTicket.class);
                context.startActivity(i);
            } else if (bdataArrayList.get(position).Type.equalsIgnoreCase("12")) {
                DownloadApk(bdataArrayList.get(position).RedirectURL);
//                RedirectPlayStore(bdataArrayList.get(position).RedirectURL);
            }
        });
        Glide.with(context).load(bdataArrayList.get(position).FeaturedImage).into(img_main);
        view.addView(textLayout, 0);

        return textLayout;
    }

    /*private void RedirectPlayStore(String redirectURL) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=esports.gamerji.com&hl=en")));
    }*/

    public void DownloadApk(String redirectURL) {
        TedPermission.with(context)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        File mDirectory = new File(Environment.getExternalStorageDirectory() + "/Download");

                        if (!mDirectory.exists()) {
                            if (!mDirectory.mkdir())
                                return;
                        }
                        File mOutputPath = new File(mDirectory, "gamerji.apk");
                        if (mOutputPath.exists())
                            mOutputPath.delete();

                        new FileUtils.DownloadAPK(redirectURL, mOutputPath.getAbsolutePath(), new SetOnDownloadProgressListner() {
                            @Override
                            public void OnDownloadStarted() {
                                DownloadDialog();
                            }

                            @Override
                            public void OnProgressUpdate(Integer mValue) {

                                if (pb_download_dialog != null && txt_progress_update != null) {
                                    pb_download_dialog.setProgress(mValue);
                                    txt_progress_update.setText("" + mValue + " %");
                                }
                            }

                            @Override
                            public void OnDownloadComplete(String mOutPutPath) {
                                if (mDownloadDialog != null && mDownloadDialog.isShowing())
                                    mDownloadDialog.dismiss();
                                context.startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));
                                FileUtils.InstallAPK(context.getApplicationContext(), mOutPutPath);
                            }

                            @Override
                            public void OnErrorOccure(String mMessage) {
                                if (mDownloadDialog != null && mDownloadDialog.isShowing())
                                    mDownloadDialog.dismiss();
                                Toast.makeText(context, "" + mMessage, Toast.LENGTH_SHORT).show();
                            }
                        }).execute();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {

                    }
                })
                .setDeniedMessage("To Download the update you must have to allow us a permission")
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check();
    }

    void DownloadDialog() {
        mDownloadDialog = new Dialog(context);
        mDownloadDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDownloadDialog.setContentView(R.layout.download_progress_dialog);
        mDownloadDialog.setCanceledOnTouchOutside(false);
        mDownloadDialog.setCancelable(false);
        if (mDownloadDialog.getWindow() != null)
            mDownloadDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        txt_progress_update = mDownloadDialog.findViewById(R.id.txt_progress_update);
        pb_download_dialog = mDownloadDialog.findViewById(R.id.pb_download_dialog);
        pb_download_dialog.setMax(100);
        mDownloadDialog.show();
    }

    @Override
    public int getCount() {
        return bdataArrayList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
