package esports.gamerji.com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import esports.gamerji.com.R;
import esports.gamerji.com.model.LeaderboardModel;

import java.util.ArrayList;

public class LeaderboardAdapter extends RecyclerView.Adapter<LeaderboardAdapter.ViewHolder> {
    Context context;
    private ArrayList<LeaderboardModel.Data.UserLevelsData> userLevelsData;

    private boolean isLoaderVisible = false;

    public LeaderboardAdapter(Context context, ArrayList<LeaderboardModel.Data.UserLevelsData> userLevelsData) {
        this.context = context;
        this.userLevelsData = userLevelsData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_leaderboard, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txt_name.setText(userLevelsData.get(position).Name);

        /*if (position==userLevelsData.get(position))
        {
            holder.lyt_leaderboasrd_item.setBackgroundColor(context.getResources().getColor(R.color.orange_color));
        }*/
        if (!userLevelsData.get(position).MobileNumber.isEmpty() && userLevelsData.get(position).MobileNumber.length() > 5)
            holder.txt_acc_number.setText("*****" + userLevelsData.get(position).MobileNumber.substring(9));

        holder.txt_kill.setText(userLevelsData.get(position).Points);
        holder.txt_rank.setText(userLevelsData.get(position).LevelNumber);
        holder.txt_points.setText(userLevelsData.get(position).Rank);
        Glide.with(context).load(userLevelsData.get(position).FeaturedIcon).into(holder.img);

        // Profile Hide
        /*holder.itemView.setOnClickListener(v ->
        {
            Intent i = new Intent(context, PlayerProfileActivity.class);
            i.putExtra(AppConstants.BUNDLE_PLAYER_NAME, userLevelsData.get(position).Name);
            context.startActivity(i);
        });*/

        /*holder.itemView.setOnClickListener(v ->
        {
            Intent i = new Intent(context, ActivityViewPlayerProfile.class);
            i.putExtra("player_id",userLevelsData.get(position).UserID);
            context.startActivity(i);
        });*/
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return userLevelsData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name, txt_kill, txt_rank, txt_acc_number, txt_points;
        ImageView img;
        LinearLayout lyt_leaderboasrd_item;
        private int mCurrentPosition;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_name = itemView.findViewById(R.id.txt_name);
            lyt_leaderboasrd_item = itemView.findViewById(R.id.lyt_leaderboasrd_item);
            txt_kill = itemView.findViewById(R.id.txt_kill);
            txt_rank = itemView.findViewById(R.id.txt_rank);
            txt_acc_number = itemView.findViewById(R.id.txt_acc_number);
            txt_points = itemView.findViewById(R.id.txt_points);
            img = itemView.findViewById(R.id.img);
            txt_points.setVisibility(View.VISIBLE);
        }
    }
}
