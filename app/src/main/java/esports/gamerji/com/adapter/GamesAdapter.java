package esports.gamerji.com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import esports.gamerji.com.R;
import esports.gamerji.com.model.GetGamesModel;

public class GamesAdapter extends RecyclerView.Adapter {

    private Context context;
    private ArrayList<GetGamesModel.Data.GamesData> mArrGames;
    private ArrayList<String> mArrSelectedGames;
    private static SingleClickListener sClickListener;
    private boolean mIsOthersChecked;

    /**
     * Adapter contains the data to be displayed
     */
    public GamesAdapter(Context context, ArrayList<GetGamesModel.Data.GamesData> mArrGames) {
        this.context = context;
        this.mArrGames = mArrGames;
        mArrSelectedGames = new ArrayList<>();
    }

    /**
     * Create an interface for the recycler view click events
     */
    public void setOnCheckboxItemClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    public interface SingleClickListener {
        void onCheckboxItemClickListener(ArrayList<String> mArrSelectedGames, boolean mIsOthersChecked);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .row_games_item, parent, false);
        return new CustomViewHolder(view);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        GetGamesModel.Data.GamesData gamesData = mArrGames.get(position);

        ((CustomViewHolder) holder).mCheckBox.setText(gamesData.getName());
        ((CustomViewHolder) holder).mCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addRemoveCheckboxValue(v, gamesData.getGameID(), gamesData.getName());
            }
        });
    }

    /**
     * This should check if the checkbox is checked or unchecked, so the value sets after that
     *
     * @param v       - View
     * @param mGameId - Game Id
     */
    private void addRemoveCheckboxValue(View v, String mGameId, String mGameName) {
        boolean checked = ((CheckBox) v).isChecked();

        if (mGameName.contains(context.getResources().getString(R.string.text_others))) {
            mIsOthersChecked = checked;
        } else {
            if (checked) {
                mArrSelectedGames.add(mGameId);
            } else {
                mArrSelectedGames.remove(mGameId);
            }
        }
        sClickListener.onCheckboxItemClickListener(mArrSelectedGames, mIsOthersChecked);
    }

    @Override
    public int getItemCount() {
        return mArrGames.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * Id declarations
     */
    public static class CustomViewHolder extends RecyclerView.ViewHolder {

        CheckBox mCheckBox;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Check Box
            this.mCheckBox = itemView.findViewById(R.id.checkbox_row_games);
        }
    }
}
