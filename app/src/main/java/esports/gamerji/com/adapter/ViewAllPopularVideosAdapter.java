package esports.gamerji.com.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import esports.gamerji.com.R;
import esports.gamerji.com.model.ViewAllPopularVideosModel;

public class ViewAllPopularVideosAdapter extends RecyclerView.Adapter {


    private Activity mActivity;
    private ArrayList<ViewAllPopularVideosModel.Data.VideosData> mArrViewAllPopularVideos;

    /**
     * Adapter contains the data to be displayed
     */
    public ViewAllPopularVideosAdapter(Activity mActivity, ArrayList<ViewAllPopularVideosModel.Data.VideosData> mArrViewAllPopularVideos) {
        this.mActivity = mActivity;
        this.mArrViewAllPopularVideos = mArrViewAllPopularVideos;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .row_view_all_popular_videos_item, parent, false);
        return new CustomViewHolder(itemView);
    }

    @SuppressLint({"SetJavaScriptEnabled", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final ViewAllPopularVideosModel.Data.VideosData viewAllPopularVideosModel = mArrViewAllPopularVideos.get(position);

        ((CustomViewHolder) holder).mWebView.setWebChromeClient(new WebChromeClient());
        ((CustomViewHolder) holder).mWebView.getSettings().setJavaScriptEnabled(true);
        ((CustomViewHolder) holder).mWebView.getSettings().setLoadsImagesAutomatically(true);
        ((CustomViewHolder) holder).mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        ((CustomViewHolder) holder).mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        ((CustomViewHolder) holder).mWebView.getSettings().setAllowFileAccess(true);
        ((CustomViewHolder) holder).mWebView.loadUrl(viewAllPopularVideosModel.getYoutubeLink());

        ((CustomViewHolder) holder).mTextTitle.setSelected(true);
        ((CustomViewHolder) holder).mTextTitle.setText(viewAllPopularVideosModel.getTitle());
    }

    @Override
    public int getItemCount() {
        return mArrViewAllPopularVideos.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    /**
     * Id declarations
     */
    public static class CustomViewHolder extends RecyclerView.ViewHolder {

        WebView mWebView;
        TextView mTextTitle;

        @SuppressLint("SetJavaScriptEnabled")
        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Web View
            this.mWebView = itemView.findViewById(R.id.web_view_row_view_all_popular_video);

            // Text Views
            this.mTextTitle = itemView.findViewById(R.id.text_row_view_all_popular_video_title);
        }
    }
}
