package esports.gamerji.com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import esports.gamerji.com.R;
import esports.gamerji.com.fragments.ProfileFragment;
import esports.gamerji.com.model.GamerProfileModel;

public class MyBadgesAdapter extends RecyclerView.Adapter<MyBadgesAdapter.ViewHolder>
{

    Context context;
    private  ArrayList<GamerProfileModel.Data.LevelsData.LevelData> badgesList;
    public MyBadgesAdapter(Context context, ArrayList<GamerProfileModel.Data.LevelsData.LevelData> badgesList)
    {
        this.context=context;
        this.badgesList=badgesList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_badges, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Glide.with(context).load(badgesList.get(position).FeaturedIcon).into(holder.img_item_badge);
            holder.img_item_badge.setOnClickListener(v ->
            {
                ProfileFragment.profileBadgeClickListener.OnBadgeClickListener(badgesList.get(position));
                /*if (context instanceof ActivityProfile)
                {
                    ((ActivityProfile)context).txt_badge_msg.setText(badgesList.get(position).Name);
                    ((ActivityProfile)context).txt_points.setText("Points  "+badgesList.get(position).StartingPoint+"  -  "+
                            badgesList.get(position).EndingPoint);
                    Glide.with(context).load(badgesList.get(position).FeaturedIcon).into(((ActivityProfile)context).img_badge);
                    ((ActivityProfile)context).badgesheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                else
                {
                    ((ActivityViewPlayerProfile)context).txt_badge_msg.setText(badgesList.get(position).Name);
                    ((ActivityViewPlayerProfile)context).txt_points.setText("Points  "+badgesList.get(position).StartingPoint+"  -  "+
                            badgesList.get(position).EndingPoint);
                    Glide.with(context).load(badgesList.get(position).FeaturedIcon).into(((ActivityViewPlayerProfile)context).img_badge);
                    ((ActivityViewPlayerProfile)context).badgesheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                }*/
            });
    }

    @Override
    public int getItemCount()
    {
        return badgesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView img_item_badge;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            img_item_badge=itemView.findViewById(R.id.img_item_badge);
        }
    }
}
