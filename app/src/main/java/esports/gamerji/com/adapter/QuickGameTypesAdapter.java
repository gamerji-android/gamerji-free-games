package esports.gamerji.com.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import esports.gamerji.com.R;
import esports.gamerji.com.activity.QuickGameSubTypesActivity;
import esports.gamerji.com.model.QuickGameTypesModel;
import esports.gamerji.com.utils.AppConstants;

public class QuickGameTypesAdapter extends RecyclerView.Adapter {


    private Activity mActivity;
    private ArrayList<QuickGameTypesModel.Data.CategoriesData> mArrQuickGameTypes;

    /**
     * Adapter contains the data to be displayed
     */
    public QuickGameTypesAdapter(Activity mActivity, ArrayList<QuickGameTypesModel.Data.CategoriesData> mArrQuickGameTypes) {
        this.mActivity = mActivity;
        this.mArrQuickGameTypes = mArrQuickGameTypes;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .row_quick_game_types_item, parent, false);
        return new CustomViewHolder(itemView);
    }

    @SuppressLint({"SetJavaScriptEnabled", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final QuickGameTypesModel.Data.CategoriesData quickGameTypesModel = mArrQuickGameTypes.get(position);

        ((CustomViewHolder) holder).mTextGameType.setText(quickGameTypesModel.getName());
        Glide.with(mActivity).load(quickGameTypesModel.getFeaturedImage()).into(((CustomViewHolder) holder).mImageGameType);

        // ToDo: Subscribe Button Click
        ((CustomViewHolder) holder).mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, QuickGameSubTypesActivity.class);
                intent.putExtra(AppConstants.BUNDLE_QUICK_GAME_TYPE, quickGameTypesModel.getName());
                intent.putExtra(AppConstants.BUNDLE_QUICK_GAME_CATEGORY_ID, quickGameTypesModel.getCategoryID());
                mActivity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrQuickGameTypes.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * Id declarations
     */
    public static class CustomViewHolder extends RecyclerView.ViewHolder {

        CardView mCardView;
        ImageView mImageGameType;
        TextView mTextGameType;

        @SuppressLint("SetJavaScriptEnabled")
        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Card Views
            mCardView = itemView.findViewById(R.id.card_view_row_quick_game_types);

            // Image Views
            mImageGameType = itemView.findViewById(R.id.image_row_quick_game_type);

            // Text Views
            this.mTextGameType = itemView.findViewById(R.id.text_row_quick_game_type);
        }
    }
}
