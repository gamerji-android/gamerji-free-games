package esports.gamerji.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import esports.gamerji.com.R;
import esports.gamerji.com.model.SearchModel;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder>
{
    Context context;
    private ArrayList<SearchModel.Data.UserLevelsData> userLevelsData;

    public SearchResultAdapter(Context context, ArrayList<SearchModel.Data.UserLevelsData> userLevelsData)
    {
        this.context=context;
        this.userLevelsData=userLevelsData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_leaderboard, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.txt_name.setText(userLevelsData.get(position).Name);
        if (!userLevelsData.get(position).MobileNumber.isEmpty())
            holder.txt_acc_number.setText("*****"+userLevelsData.get(position).MobileNumber.substring(9));
        Glide.with(context).load(userLevelsData.get(position).FeaturedIcon).into(holder.img);

        // Profile Hide
        /*holder.itemView.setOnClickListener(v -> {
            Intent i = new Intent(context, ActivityViewPlayerProfile.class);
            i.putExtra("player_id",userLevelsData.get(position).UserID);
            context.startActivity(i);
        });*/
    }

    @Override
    public int getItemCount()
    {
        return userLevelsData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_name,txt_kill,txt_rank,txt_acc_number,txt_points;
        ImageView img;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            txt_name=itemView.findViewById(R.id.txt_name);
            txt_kill=itemView.findViewById(R.id.txt_kill);
            txt_rank=itemView.findViewById(R.id.txt_rank);
            txt_acc_number=itemView.findViewById(R.id.txt_acc_number);
            txt_points=itemView.findViewById(R.id.txt_points);
            img=itemView.findViewById(R.id.img);
            txt_points.setVisibility(View.VISIBLE);

        }
    }
}
