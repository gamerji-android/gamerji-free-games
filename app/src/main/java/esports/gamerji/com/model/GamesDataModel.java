package esports.gamerji.com.model;

public  class GamesDataModel
{
    public String getUserGameID() {
        return UserGameID;
    }

    public void setUserGameID(String userGameID) {
        UserGameID = userGameID;
    }

    public String getGameID() {
        return GameID;
    }

    public void setGameID(String gameID) {
        GameID = gameID;
    }

    public String getUniqueName() {
        return UniqueName;
    }

    public void setUniqueName(String uniqueName) {
        UniqueName = uniqueName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String UserGameID;

    public String GameID;

    public String UniqueName;
    public String Status;
}