package esports.gamerji.com.model;

import com.google.gson.annotations.SerializedName;

public class GeneralResponseModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data DataClass;

    public class Data
    {
        @SerializedName("UserID")
        public String UserID;

        @SerializedName("ReferralCode")
        public String ReferralCode;

        @SerializedName("ReferralBonus")
        public String ReferralBonus;

        @SerializedName("InviteeBonus")
        public String InviteeBonus;

        @SerializedName("OrderID")
        public String OrderID;

        @SerializedName("PaymentToken")
        public String PaymentToken;

        @SerializedName("PaymentStatus")
        public String PaymentStatus;
    }
}
