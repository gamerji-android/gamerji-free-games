package esports.gamerji.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetGamesModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {

        @SerializedName("GamesCount")
        @Expose
        private String gamesCount;
        @SerializedName("GamesData")
        @Expose
        private ArrayList<GamesData> gamesData = null;

        public String getGamesCount() {
            return gamesCount;
        }

        public void setGamesCount(String gamesCount) {
            this.gamesCount = gamesCount;
        }

        public ArrayList<GamesData> getGamesData() {
            return gamesData;
        }

        public void setGamesData(ArrayList<GamesData> gamesData) {
            this.gamesData = gamesData;
        }

        public static class GamesData {

            @SerializedName("GameID")
            @Expose
            private String gameID;
            @SerializedName("Name")
            @Expose
            private String name;
            private boolean isSelected;

            public String getGameID() {
                return gameID;
            }

            public void setGameID(String gameID) {
                this.gameID = gameID;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public boolean isSelected() {
                return isSelected;
            }

            public void setSelected(boolean isSelected) {
                this.isSelected = isSelected;
            }
        }
    }
}
