package esports.gamerji.com.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetMessagesModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data DataClass;

    public  class Data
    {

        @SerializedName("IsLast")
        public boolean IsLast;

        @SerializedName("MessagesCount")
        public String MessagesCount;

        @SerializedName("status")
        public String status;

        @SerializedName("flag")
        public String flag;

        @SerializedName("message")
        public String message;

        @SerializedName("data")
        public MessagesData  messagesData;

        @SerializedName("MessagesData")
        public ArrayList<MessagesData> messagesDataArrayList= new ArrayList<>();

        public  class MessagesData
        {
            @SerializedName("MessageID")
            public String MessageID;

            @SerializedName("Message")
            public String Message;

            @SerializedName("IsUser")
            public String IsUser;

            @SerializedName("Date")
            public String Date;

            @SerializedName("DateAgo")
            public String DateAgo;
        }
    }


}
