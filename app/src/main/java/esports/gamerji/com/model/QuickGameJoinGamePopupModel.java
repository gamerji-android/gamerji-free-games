package esports.gamerji.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuickGameJoinGamePopupModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("UserID")
        @Expose
        private String userID;
        @SerializedName("WalletBalance")
        @Expose
        private String walletBalance;
        @SerializedName("EntryFee")
        @Expose
        private String entryFee;
        @SerializedName("CashBalance")
        @Expose
        private String cashBalance;
        @SerializedName("ToPay")
        @Expose
        private String toPay;
        @SerializedName("JoinFlag")
        @Expose
        private Boolean joinFlag;
        @SerializedName("HelpText")
        @Expose
        private String helpText;

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getWalletBalance() {
            return walletBalance;
        }

        public void setWalletBalance(String walletBalance) {
            this.walletBalance = walletBalance;
        }

        public String getEntryFee() {
            return entryFee;
        }

        public void setEntryFee(String entryFee) {
            this.entryFee = entryFee;
        }

        public String getCashBalance() {
            return cashBalance;
        }

        public void setCashBalance(String cashBalance) {
            this.cashBalance = cashBalance;
        }

        public String getToPay() {
            return toPay;
        }

        public void setToPay(String toPay) {
            this.toPay = toPay;
        }

        public Boolean getJoinFlag() {
            return joinFlag;
        }

        public void setJoinFlag(Boolean joinFlag) {
            this.joinFlag = joinFlag;
        }

        public String getHelpText() {
            return helpText;
        }

        public void setHelpText(String helpText) {
            this.helpText = helpText;
        }
    }
}
