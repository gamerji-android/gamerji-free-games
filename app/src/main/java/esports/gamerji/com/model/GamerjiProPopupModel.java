package esports.gamerji.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GamerjiProPopupModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("Title")
        @Expose
        private String title;
        @SerializedName("Content")
        @Expose
        private String content;
        @SerializedName("ShowPopup")
        @Expose
        private String showPopup;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getShowPopup() {
            return showPopup;
        }

        public void setShowPopup(String showPopup) {
            this.showPopup = showPopup;
        }
    }
}
