package esports.gamerji.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetStreamerStatusModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("StreamerID")
        @Expose
        private String streamerID;
        @SerializedName("Reason")
        @Expose
        private String reason;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("DisplayStatus")
        @Expose
        private String displayStatus;

        public String getStreamerID() {
            return streamerID;
        }

        public void setStreamerID(String streamerID) {
            this.streamerID = streamerID;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDisplayStatus() {
            return displayStatus;
        }

        public void setDisplayStatus(String displayStatus) {
            this.displayStatus = displayStatus;
        }
    }
}
