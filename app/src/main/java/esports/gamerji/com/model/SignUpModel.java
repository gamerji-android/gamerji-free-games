package esports.gamerji.com.model;

import com.google.gson.annotations.SerializedName;

public class SignUpModel {
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public UserData userDataClass;

    public class UserData {
        @SerializedName("UserID")
        public String UserID;

        @SerializedName("CountryCode")
        public String CountryCode;

        @SerializedName("MobileNumber")
        public String MobileNumber;

        @SerializedName("EmailAddress")
        public String EmailAddress;

        @SerializedName("OTPCode")
        public String OTPCode;

        @SerializedName("OTP")
        public String OTP;

        @SerializedName("Status")
        public String Status;
    }
}
