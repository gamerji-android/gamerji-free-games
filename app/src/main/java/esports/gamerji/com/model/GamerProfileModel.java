package esports.gamerji.com.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GamerProfileModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data DataClass;

    public class Data
    {
        @SerializedName("UserID")
        public String UserID;

        @SerializedName("TeamName")
        public String TeamName;

        @SerializedName("TotalPoints")
        public String TotalPoints;

        @SerializedName("GamesData")
        public  GamesData gamesData;

        @SerializedName("LevelNumber")
        public String LevelNumber;

        @SerializedName("LevelName")
        public  String LevelName;

        @SerializedName("LevelIcon")
        public  String LevelIcon;

        public class GamesData
        {
            @SerializedName("GamesCount")
            public String GamesCount;

            @SerializedName("GamesData")
            public ArrayList<GameData> gamesDataArrayList;

            public class GameData
            {
                @SerializedName("UserGameID")
                public String UserGameID;

                @SerializedName("GameID")
                public String GameID;

                @SerializedName("GameName")
                public String GameName;

                @SerializedName("GameFeaturedImage")
                public String GameFeaturedImage;

                @SerializedName("GamePlayed")
                public String GamePlayed;

                @SerializedName("GameWon")
                public String GameWon;

                @SerializedName("GamePoints")
                public String GamePoints    ;

                @SerializedName("UniqueName")
                public String UniqueName;

                @SerializedName("Status")
                public String Status;
            }
        }

        @SerializedName("LevelsData")
        public  LevelsData LevelsData;

        public class LevelsData
        {
            @SerializedName("LevelsCount")
            public String LevelsCount;

            @SerializedName("LevelsData")
            public ArrayList<LevelData> levelsDataArrayList;

            public class LevelData
            {
                @SerializedName("LevelID")
                public String LevelID;

                @SerializedName("Name")
                public String Name;

                @SerializedName("FeaturedIcon")
                public String FeaturedIcon;

                @SerializedName("StartingPoint")
                public String StartingPoint;

                @SerializedName("EndingPoint")
                public String EndingPoint;

                @SerializedName("Information")
                public String Information;
            }
        }

        @SerializedName("CurrentLevelPoints")
        public  String CurrentLevelPoints;

        @SerializedName("StartLevelNumber")
        public  String StartLevelNumber;

        @SerializedName("StartLevelName")
        public  String StartLevelName;

        @SerializedName("StartLevelPoints")
        public  String StartLevelPoints;

        @SerializedName("EndLevelNumber")
        public  String EndLevelNumber;

        @SerializedName("EndLevelName")
        public  String EndLevelName;

        @SerializedName("EndLevelPoints")
        public  String EndLevelPoints;

        @SerializedName("AdsCount")
        public int AdsCount;

        @SerializedName("AdsData")
        public ArrayList<AdsDataModel> adsDataArrayList;

    }
}
