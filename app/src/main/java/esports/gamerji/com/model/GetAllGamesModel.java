package esports.gamerji.com.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetAllGamesModel {
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data GamesData;

    public class Data
    {
        @SerializedName("GamesData")
        public ArrayList<GamesData> gamesDataArrayList;

        @SerializedName("GamesCount")
        public String GamesCount;

        public class GamesData
        {
            @SerializedName("GameID")
            public String GameID;

            @SerializedName("Name")
            public String Name;

            @SerializedName("TypesCount")
            public String TypesCount;

            @SerializedName("TypesRecords")
            public ArrayList<TypesRecord> typesRecordArrayList;

            public class TypesRecord
            {
                @SerializedName("TypeID")
                public String TypeID;

                @SerializedName("Name")
                public String Name;
            }
        }

    }
}
