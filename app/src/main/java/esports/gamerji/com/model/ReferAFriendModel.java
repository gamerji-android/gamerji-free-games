package esports.gamerji.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReferAFriendModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("UserID")
        @Expose
        private String userID;
        @SerializedName("Title")
        @Expose
        private String title;
        @SerializedName("ReferText")
        @Expose
        private String referText;
        @SerializedName("InviteText")
        @Expose
        private String inviteText;
        @SerializedName("ReferralBonus")
        @Expose
        private String referralBonus;
        @SerializedName("InviteeBonus")
        @Expose
        private String inviteeBonus;
        @SerializedName("ReferralCode")
        @Expose
        private String referralCode;

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getReferText() {
            return referText;
        }

        public void setReferText(String referText) {
            this.referText = referText;
        }

        public String getInviteText() {
            return inviteText;
        }

        public void setInviteText(String inviteText) {
            this.inviteText = inviteText;
        }

        public String getReferralBonus() {
            return referralBonus;
        }

        public void setReferralBonus(String referralBonus) {
            this.referralBonus = referralBonus;
        }

        public String getInviteeBonus() {
            return inviteeBonus;
        }

        public void setInviteeBonus(String inviteeBonus) {
            this.inviteeBonus = inviteeBonus;
        }

        public String getReferralCode() {
            return referralCode;
        }

        public void setReferralCode(String referralCode) {
            this.referralCode = referralCode;
        }
    }
}
