package esports.gamerji.com.model;

import java.util.ArrayList;

public class CustomerCareIssuesModel
{
    public String getCategory_Name()
    {
        return Category_Name;
    }

    public void setCategory_Name(String category_Name)
    {
        Category_Name = category_Name;
    }

    public String getIsContest_dependent() {
        return isContest_dependent;
    }

    public void setIsContest_dependent(String isContest_dependent) {
        this.isContest_dependent = isContest_dependent;
    }

    public String getCat_issue_id() {
        return cat_issue_id;
    }

    public void setCat_issue_id(String cat_issue_id)
    {
        this.cat_issue_id = cat_issue_id;
    }

    public ArrayList<CustomerIssuesModel.Data.IssuesData.SubIssuesData> getSubIssuesDataArrayList()
    {
        return subIssuesDataArrayList;
    }

    public void setSubIssuesDataArrayList(ArrayList<CustomerIssuesModel.Data.IssuesData.SubIssuesData> subIssuesDataArrayList)
    {
        this.subIssuesDataArrayList = subIssuesDataArrayList;
    }

    private String Category_Name;
    private String isContest_dependent;
    private String cat_issue_id;

    public String getSub_issues_count() {
        return sub_issues_count;
    }

    public void setSub_issues_count(String sub_issues_count) {
        this.sub_issues_count = sub_issues_count;
    }

    private String sub_issues_count;
     private ArrayList<CustomerIssuesModel.Data.IssuesData.SubIssuesData> subIssuesDataArrayList;


}
