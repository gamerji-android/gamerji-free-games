package esports.gamerji.com.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class StatesListModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data userDataClass;

    public class Data
    {
        @SerializedName("StatesCount")
        public String StatesCount;

        @SerializedName("StatesData")
        public ArrayList<StatesData> statesDataArrayList;

        @SerializedName("IssuesCount")
        public String IssuesCount;

        @SerializedName("IssuesData")
        public ArrayList<IssuesData> issuesDataArrayList;

        public class StatesData
        {
            @SerializedName("StateID")
            public String StateID;

            @SerializedName("Name")
            public String Name;

            @SerializedName("IsRestricted")
            public String IsRestricted;
        }

        public class IssuesData
        {
            @SerializedName("IssueID")
            public String IssueID;

            @SerializedName("Name")
            public String Name;
        }
    }
}
