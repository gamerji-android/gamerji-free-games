package esports.gamerji.com.notification_services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import esports.gamerji.com.R;
import esports.gamerji.com.activity.BottomNavigationActivity;
import esports.gamerji.com.activity.SplashActivity;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.Pref;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    public static int badge_count = 0;
    private static final String NOTIFICATION_CHANNEL_ID = "10001";
    private NotificationCompat.Builder mBuilder;
    private NotificationManager mNotificationManager;
    Random random = new Random();

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getData() == null) {
            if (remoteMessage.getNotification() != null) {
                if (remoteMessage.getNotification().getBody() != null) {
                    sendNotification(remoteMessage.getNotification().getBody(), "", "",
                            remoteMessage.getNotification().getTitle());
                }
            }
            return;
        }

        if (remoteMessage.getData().size() > 0) {
        }

        Intent intent = new Intent("NotificationReceived");
        try {
            sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getData().get("RelationID"),
                    remoteMessage.getData().get("Type"),
                    remoteMessage.getNotification().getTitle());
        } catch (NullPointerException e) {
            e.printStackTrace();
            if (remoteMessage.getNotification() != null) {
                if (remoteMessage.getNotification().getBody() != null) {
                    sendNotification(remoteMessage.getNotification().getBody(), "", "",
                            remoteMessage.getNotification().getTitle());
                }
            }
        }


    }

    private void sendNotification(String messageBody, String relation_id, String type, String title) {
        Intent resultIntent = null;

        if (!Pref.getValue(getApplicationContext(), Constants.IS_LOGIN, false, Constants.FILENAME)) {
            resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
        } else {
            if (!relation_id.isEmpty()) {
                if (type.equalsIgnoreCase("1")) {
//                    resultIntent = new Intent(getApplicationContext(), ActivityContestDetails.class);
//                    resultIntent.putExtra("contest_id", relation_id);
//                    resultIntent.putExtra("invite", true);
                } else {
//                    resultIntent = new Intent(getApplicationContext(), ActivityProfilee.class);
                    resultIntent = new Intent(getApplicationContext(), BottomNavigationActivity.class);
                    BottomNavigationActivity.currentTabPosition = 3;
                }
            } else {
//                resultIntent = new Intent(getApplicationContext(), ActivityMain.class);
                resultIntent = new Intent(getApplicationContext(), BottomNavigationActivity.class);
            }
        }


        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplicationContext(),
                0  /*Request code*/, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder = new NotificationCompat.Builder(getApplicationContext());
        mBuilder.setSmallIcon(R.drawable.notification);
        mBuilder.setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(getColor(R.color.colorPrimaryDark));
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 100, 100});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mBuilder.setAutoCancel(true);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(random.nextInt(9999 - 1000) + 1000 /*Request Code*/, mBuilder.build());
    }
}
