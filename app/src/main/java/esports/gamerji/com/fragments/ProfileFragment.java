package esports.gamerji.com.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;
import esports.gamerji.com.BuildConfig;
import esports.gamerji.com.R;
import esports.gamerji.com.activity.BottomNavigationActivity;
import esports.gamerji.com.adapter.AdsSliderAdapter;
import esports.gamerji.com.adapter.GameProfileAdapter;
import esports.gamerji.com.adapter.MyBadgesAdapter;
import esports.gamerji.com.interfaces.ProfileGameInfoClickListener;
import esports.gamerji.com.model.GamerProfileModel;
import esports.gamerji.com.model.SignInModel;
import esports.gamerji.com.model.StatesListModel;
import esports.gamerji.com.utils.AnalyticsSocket;
import esports.gamerji.com.utils.Common;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.CustomWheelView;
import esports.gamerji.com.utils.Pref;
import esports.gamerji.com.utils.datepicker.DatePicker;
import esports.gamerji.com.utils.datepicker.DatePickerDialog;
import esports.gamerji.com.utils.datepicker.SpinnerDatePickerDialogBuilder;
import esports.gamerji.com.webServices.APIClient;
import esports.gamerji.com.webServices.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static esports.gamerji.com.utils.Constants.AD_IMPRESSION;
import static esports.gamerji.com.utils.FileUtils.requestfoucs;

public class ProfileFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    private View mView;
    private Activity mActivity;
    private RelativeLayout mRelativeMain;
    private SwipeRefreshLayout mSwipeRefresh;
    private ImageView mImageGameLevel, mImageLeftArrow, mImageRightArrow, mImageProfilePic,
            mImageProfilePicEdit, mImageSelectDateOfBirth, mImageSelectState;
    public static TextView mTextEdit;
    private TextView mTextGamerTab, mTextPersonalTab, mTextLevelRank, mTextLevelName, mTextUserName, mTextTotalPoints, mTextProgressTotalPoints,
            mTextStartingLevel, mTextEndingLevel, mTextStartingPoints, mTextEndingPoints, mTextSelectState, mTextSelectDateOfBirth;
    private SeekBar mSeekBarGameProgress;
    private RecyclerView mRecyclerViewBadges, mRecyclerViewGames;
    private MyBadgesAdapter myBadgesAdapter;
    private LinearLayoutManager badgesLinearLayout;
    private CardView mCardViewFirstName, mCardViewLastName, mCardViewPhoneNo, mCardViewEmail;
    private EditText mEditFirstName, mEditLastName, mEditPhoneNo, mEditEmail;
    private String dob;
    private ArrayList<SignInModel.UserData.GamesData.GameData> gamesDataArrayList = new ArrayList<>();
    private ArrayList<String> stateslist = new ArrayList<>();
    private ArrayList<String> statesId = new ArrayList<>();
    private ArrayList<String> isRestricteed = new ArrayList<>();
    private PickResult pickResult;
    private boolean editable = false;
    private int currentPage = 0, NUM_PAGES = 0;
    private Timer swipeTimer;
    boolean sendSwipeImpression = true;
    private LinearLayout mLinearGamer, mLinearPersonal;
    private AnalyticsSocket analyticsSocket = new AnalyticsSocket();
    public static ProfileGameInfoClickListener profileBadgeClickListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // ToDo: Initialise Game Info Click Listener
        if (context instanceof ProfileGameInfoClickListener) {
            profileBadgeClickListener = (ProfileGameInfoClickListener) context;
        } else {
            throw new ClassCastException(context.toString() + " Please Implement Profile Badge Click Listener");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_profile, container, false);
        mActivity = getActivity();
        swipeTimer = new Timer();
        badgesLinearLayout = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);

        getIds();
        setRegListeners();
        setData();
        getAllStates();
        getGamerProfile();
        return mView;
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layout
            mRelativeMain = mView.findViewById(R.id.relative_profile_main);

            // Relative Layout
            mLinearGamer = mView.findViewById(R.id.linear_profile_gamer);
            mLinearPersonal = mView.findViewById(R.id.linear_profile_personal);

            // Swipe Refresh Layout
            mSwipeRefresh = mView.findViewById(R.id.swipe_refresh_view_profile);

            // Text Views
            mTextEdit = mView.findViewById(R.id.text_profile_edit);
            mTextGamerTab = mView.findViewById(R.id.text_profile_gamer_tab);
            mTextPersonalTab = mView.findViewById(R.id.text_profile_personal_tab);
            mTextLevelRank = mView.findViewById(R.id.text_profile_game_level_rank);
            mTextLevelName = mView.findViewById(R.id.text_profile_game_level_name);
            mTextUserName = mView.findViewById(R.id.text_profile_game_user_name);
            mTextTotalPoints = mView.findViewById(R.id.text_profile_game_total_points);
            mTextProgressTotalPoints = mView.findViewById(R.id.text_profile_game_progress_total_points);
            mTextStartingLevel = mView.findViewById(R.id.text_profile_game_starting_level);
            mTextEndingLevel = mView.findViewById(R.id.text_profile_game_ending_level);
            mTextStartingPoints = mView.findViewById(R.id.text_profile_game_starting_points);
            mTextEndingPoints = mView.findViewById(R.id.text_profile_game_ending_points);
            mTextSelectState = mView.findViewById(R.id.text_profile_personal_select_state);
            mTextSelectDateOfBirth = mView.findViewById(R.id.text_profile_personal_select_date_of_birth);

            // Image Views
            mImageGameLevel = mView.findViewById(R.id.image_profile_game_level);
            mImageLeftArrow = mView.findViewById(R.id.image_profile_game_badges_left_arrow);
            mImageRightArrow = mView.findViewById(R.id.image_profile_game_badges_right_arrow);
            mImageProfilePic = mView.findViewById(R.id.image_profile_personal_profile_pic);
            mImageProfilePicEdit = mView.findViewById(R.id.image_profile_personal_profile_pic_edit);
            mImageSelectDateOfBirth = mView.findViewById(R.id.image_profile_personal_select_date);
            mImageSelectState = mView.findViewById(R.id.image_profile_personal_state_arrow);

            // Seek Bar
            mSeekBarGameProgress = mView.findViewById(R.id.seekbar_profile_game_progress);

            // Card Views
            mCardViewFirstName = mView.findViewById(R.id.card_view_profile_personal_first_name);
            mCardViewLastName = mView.findViewById(R.id.card_view_profile_personal_last_name);
            mCardViewPhoneNo = mView.findViewById(R.id.card_view_profile_personal_phone_number);
            mCardViewEmail = mView.findViewById(R.id.card_view_profile_personal_email);

            // Edit Texts
            mEditFirstName = mView.findViewById(R.id.edit_profile_personal_first_name);
            mEditLastName = mView.findViewById(R.id.edit_profile_personal_last_name);
            mEditPhoneNo = mView.findViewById(R.id.edit_profile_personal_phone_number);
            mEditEmail = mView.findViewById(R.id.edit_profile_personal_email);

            // Recycler Views
            mRecyclerViewBadges = mView.findViewById(R.id.recycler_view_profile_game_badges);
            mRecyclerViewGames = mView.findViewById(R.id.recycler_view_profile_game_games);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mTextGamerTab.setOnClickListener(clickListener);
            mTextPersonalTab.setOnClickListener(clickListener);
            mImageLeftArrow.setOnClickListener(clickListener);
            mImageProfilePic.setOnClickListener(clickListener);
            mImageRightArrow.setOnClickListener(clickListener);
            mTextSelectDateOfBirth.setOnClickListener(clickListener);
            mImageSelectDateOfBirth.setOnClickListener(clickListener);
            mTextSelectState.setOnClickListener(clickListener);
            mImageSelectState.setOnClickListener(clickListener);
            mTextEdit.setOnClickListener(clickListener);

            // ToDo: Swipe Refresh Click Listener
            mSwipeRefresh.setOnRefreshListener(this::getGamerProfile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            mTextUserName.setSelected(true);
            mTextTotalPoints.setSelected(true);
            gamesDataArrayList = Constants.getUserData(mActivity);

            String mUserName = Pref.getValue(mActivity, Constants.firstname, "", Constants.FILENAME)
                    + " " + Pref.getValue(mActivity, Constants.lastname, "", Constants.FILENAME);
            mTextUserName.setText(mUserName);

            mEditEmail.setText(Pref.getValue(mActivity, Constants.EmailAddress, "", Constants.FILENAME));
            mEditPhoneNo.setText(Pref.getValue(mActivity, Constants.MobileNumber, "", Constants.FILENAME));
            mEditFirstName.setText(Pref.getValue(mActivity, Constants.firstname, "", Constants.FILENAME));
            mEditLastName.setText(Pref.getValue(mActivity, Constants.lastname, "", Constants.FILENAME));
            mTextSelectDateOfBirth.setText(Pref.getValue(mActivity, Constants.BirthDate, "", Constants.FILENAME));
            if (!Pref.getValue(mActivity, Constants.State, "", Constants.FILENAME).equalsIgnoreCase(""))
                mTextSelectState.setText(Pref.getValue(mActivity, Constants.State, "", Constants.FILENAME));

            Common.insertLog("File Path:::> " + Pref.getValue(mActivity, Constants.ProfileImage, "",
                    Constants.FILENAME));
            if (!(Pref.getValue(mActivity, Constants.ProfileImage, "", Constants.FILENAME).equalsIgnoreCase(""))
                    || !Pref.getValue(mActivity, Constants.ProfileImage, "", Constants.FILENAME).isEmpty()) {
                Glide.with(mActivity).load(Pref.getValue(mActivity, Constants.ProfileImage, "", Constants.FILENAME)).into(mImageProfilePic);
            }

            // Sets request focus
            mCardViewFirstName.setOnClickListener(v -> requestfoucs(mActivity, mEditFirstName));
            mCardViewLastName.setOnClickListener(v -> requestfoucs(mActivity, mEditLastName));
            mCardViewPhoneNo.setOnClickListener(v -> requestfoucs(mActivity, mEditPhoneNo));
            mCardViewEmail.setOnClickListener(v -> requestfoucs(mActivity, mEditEmail));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.text_profile_gamer_tab:
                    setGamerTabData();
                    break;

                case R.id.text_profile_personal_tab:
                    setPersonalTabData();
                    break;

                case R.id.image_profile_game_badges_left_arrow:
                    Objects.requireNonNull(mRecyclerViewBadges.getLayoutManager()).scrollToPosition(badgesLinearLayout.findFirstVisibleItemPosition() - 1);
                    break;

                case R.id.image_profile_game_badges_right_arrow:
                    Objects.requireNonNull(mRecyclerViewBadges.getLayoutManager()).scrollToPosition(badgesLinearLayout.findLastVisibleItemPosition() + 1);
                    break;

                case R.id.image_profile_personal_profile_pic:
                    callToOpenGallery();
                    break;

                case R.id.text_profile_personal_select_date_of_birth:
                    openDatePicker();
                    break;

                case R.id.image_profile_personal_select_date:
                    openDatePicker();
                    break;

                case R.id.text_profile_personal_select_state:
                    selectState();
                    break;

                case R.id.image_profile_personal_state_arrow:
                    selectState();
                    break;

                case R.id.text_profile_edit:
                    callToEditProgileAPI();
                    break;
            }
        }
    };

    /**
     * This should set the Gamer tab data
     */
    private void setGamerTabData() {
        try {
            mLinearGamer.setVisibility(View.VISIBLE);
            mLinearPersonal.setVisibility(View.GONE);
            mTextGamerTab.setBackground(mActivity.getResources().getDrawable(R.drawable.bottom_rounded));
            mTextGamerTab.setTextColor(Color.WHITE);
            mTextPersonalTab.setTextColor(Color.BLACK);
            mTextPersonalTab.setBackgroundColor(Color.TRANSPARENT);
            mTextEdit.setVisibility(View.GONE);
            setDisableEditText();
            editable = false;
            mSwipeRefresh.setEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should set the Personal tab data
     */
    private void setPersonalTabData() {
        try {
            mLinearGamer.setVisibility(View.GONE);
            mLinearPersonal.setVisibility(View.VISIBLE);
            mTextPersonalTab.setBackground(mActivity.getResources().getDrawable(R.drawable.bottom_rounded));
            mTextPersonalTab.setTextColor(Color.WHITE);
            mTextGamerTab.setTextColor(Color.BLACK);
            mTextGamerTab.setBackgroundColor(Color.TRANSPARENT);
            mTextEdit.setVisibility(View.VISIBLE);
            mTextEdit.setText(mActivity.getResources().getString(R.string.action_edit));
            setDisableEditText();
            mSwipeRefresh.setEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should enabled edit text
     */
    private void setDisableEditText() {
        try {
            // et_pubg_name.setEnabled(false);
            //et_cr_name.setEnabled(false);

            mEditFirstName.setEnabled(false);
            mEditLastName.setEnabled(false);
            mEditEmail.setEnabled(false);
            mEditPhoneNo.setEnabled(false);
            mTextSelectState.setEnabled(false);
            mImageSelectState.setEnabled(false);
            mTextSelectDateOfBirth.setEnabled(false);
            mImageSelectDateOfBirth.setEnabled(false);
            mImageProfilePic.setClickable(false);
            mImageProfilePicEdit.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Call to get the Gamer Profile API
     */
    private void getGamerProfile() {
        final ProgressDialog progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GamerProfileModel> gamerProfileModelCall = APIClient.getClient().create(APIInterface.class).getGamerProfile
                (Constants.GET_GAMER_PROFILE, Pref.getValue(mActivity, Constants.UserID, "", Constants.FILENAME));

        gamerProfileModelCall.enqueue(new Callback<GamerProfileModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<GamerProfileModel> call, @NonNull Response<GamerProfileModel> response) {
                progressDialog.dismiss();

                if (mSwipeRefresh.isRefreshing())
                    mSwipeRefresh.setRefreshing(false);

                GamerProfileModel gamerProfileModel = response.body();
                assert gamerProfileModel != null;
                if (gamerProfileModel.status.equalsIgnoreCase("success")) {
                    mRecyclerViewGames.setLayoutManager(new GridLayoutManager(mActivity, 2));
                    GameProfileAdapter gameProfileAdapter = new GameProfileAdapter(mActivity, gamerProfileModel.DataClass.gamesData.gamesDataArrayList);
                    mRecyclerViewGames.setAdapter(gameProfileAdapter);
                    BottomNavigationActivity.mViewPagerSliderAds.setAdapter(new AdsSliderAdapter(mActivity, gamerProfileModel.DataClass.adsDataArrayList, analyticsSocket));

                    NUM_PAGES = gamerProfileModel.DataClass.AdsCount;
                    final Handler handler = new Handler();
                    final Runnable Update = () ->
                    {
                        if (currentPage == NUM_PAGES) {
                            currentPage = 0;
                            sendSwipeImpression = false;
                            //swipeTimer.cancel();
                        } else {
                            if (sendSwipeImpression) {
                                analyticsSocket.sendAnalytics(gamerProfileModel.DataClass.adsDataArrayList.get(currentPage).AdType, gamerProfileModel.DataClass.adsDataArrayList.get(currentPage).AdID,
                                        gamerProfileModel.DataClass.adsDataArrayList.get(currentPage).AdImageName, AD_IMPRESSION, Pref.getValue(mActivity, Constants.UserID, "", Constants.FILENAME),
                                        0);
                            }
                        }
                        BottomNavigationActivity.mViewPagerSliderAds.setCurrentItem(currentPage++, true);
                    };

                    swipeTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            handler.post(Update);
                        }
                    }, 0, 5000);

                    mTextTotalPoints.setText(gamerProfileModel.DataClass.TotalPoints);
                    Glide.with(mActivity).load(gamerProfileModel.DataClass.LevelIcon).into(mImageGameLevel);
                    mTextLevelName.setText(gamerProfileModel.DataClass.LevelName);
                    mTextLevelRank.setText(gamerProfileModel.DataClass.LevelNumber);
                    mTextProgressTotalPoints.setText(gamerProfileModel.DataClass.CurrentLevelPoints + " Points");
                    setBadges(gamerProfileModel.DataClass.LevelsData.levelsDataArrayList);

                    int difference = Integer.parseInt(gamerProfileModel.DataClass.EndLevelPoints) - Integer.parseInt(gamerProfileModel.DataClass.StartLevelPoints);
                    mSeekBarGameProgress.setMax(difference);
                    mSeekBarGameProgress.setProgress(Integer.parseInt(gamerProfileModel.DataClass.CurrentLevelPoints) - Integer.parseInt(gamerProfileModel.DataClass.StartLevelPoints));

                    mTextStartingLevel.setText(gamerProfileModel.DataClass.StartLevelNumber);
                    mTextEndingLevel.setText(gamerProfileModel.DataClass.EndLevelNumber);
                    mTextStartingPoints.setText("Points:" + gamerProfileModel.DataClass.StartLevelPoints);
                    mTextEndingPoints.setText("Points:" + gamerProfileModel.DataClass.EndLevelPoints);
                } else {
                    Constants.SnakeMessageYellow(mRelativeMain, gamerProfileModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GamerProfileModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void setBadges(ArrayList<GamerProfileModel.Data.LevelsData.LevelData> levelsDataArrayList) {
        mRecyclerViewBadges.setLayoutManager(badgesLinearLayout);
        mRecyclerViewBadges.setItemAnimator(new DefaultItemAnimator());
        myBadgesAdapter = new MyBadgesAdapter(mActivity, levelsDataArrayList);
        mRecyclerViewBadges.setAdapter(myBadgesAdapter);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mTextSelectDateOfBirth.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
        dob = year + "/" + (monthOfYear + 1) + "/" + dayOfMonth;
    }

    private void selectState() {
        if (stateslist.size() == 0) {
            Constants.SnakeMessageYellow(mRelativeMain, "no States found.");
            return;
        }
        Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View outerView = LayoutInflater.from(mActivity).inflate(R.layout.wheelview, null);
        dialog.setContentView(outerView);
        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        CustomWheelView wv = outerView.findViewById(R.id.wh_view);
        TextView txt_ok = outerView.findViewById(R.id.txt_ok);
        TextView title = outerView.findViewById(R.id.title);
        title.setText("Select state");
        TextView txt_cancel = outerView.findViewById(R.id.txt_cancel);
        txt_ok.setOnClickListener(v ->
        {
            mTextSelectState.setText(wv.getSeletedItem().toString());
            dialog.dismiss();

        });

        txt_cancel.setOnClickListener(v -> dialog.dismiss());
        wv.setOffset(1);
        wv.setItems(stateslist);
        wv.setSeletion(0);
        dialog.show();
    }

    private void callToOpenGallery() {
        try {
            PickImageDialog.build(new PickSetup())
                    .setOnPickResult(new IPickResult() {

                        @Override
                        public void onPickResult(PickResult pr) {
                            Common.insertLog("IMAGE onPickResult" + pr.getUri());
                            if (pr.getError() == null) {
                                Glide.with(mActivity).load(pr.getUri()).into(mImageProfilePic);
                                pickResult = pr;
                            } else {
                                //Handle possible errors
                                //TODO: do what you have to do with r.getError();
                                Toast.makeText(mActivity, pr.getError().getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    })
                    .show(getActivity().getSupportFragmentManager());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openDatePicker() {
        try {
            Calendar calendar = Calendar.getInstance();
            Calendar mMaxDate = Calendar.getInstance();
            Calendar mMinDate = Calendar.getInstance();
            mMaxDate.add(Calendar.YEAR, -18);
            mMaxDate.setTimeInMillis(mMaxDate.getTimeInMillis());
            mMinDate.set(Calendar.YEAR, 1920);
            mMinDate.set(Calendar.MONTH, 1);
            mMinDate.set(Calendar.DATE, 1);

            int mMaxDateYear = mMaxDate.get(Calendar.YEAR);
            int mMaxDateMonth = mMaxDate.get(Calendar.MONTH);
            int mMaxDateDay = mMaxDate.get(Calendar.DAY_OF_MONTH);

            int mMinDateYear = mMinDate.get(Calendar.YEAR);
            int mMinDateMonth = mMinDate.get(Calendar.MONTH);
            int mMinDateDay = mMinDate.get(Calendar.DAY_OF_MONTH);

            new SpinnerDatePickerDialogBuilder().context(mActivity).callback(this).spinnerTheme(R.style.NumberPickerStyle)
                    .showTitle(true).showDaySpinner(true).defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                    .maxDate(mMaxDateYear, mMaxDateMonth, mMaxDateDay)
                    .minDate(mMinDateYear, mMinDateMonth, mMinDateDay).build().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAllStates() {
        final ProgressDialog progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<StatesListModel> signUpModelCall = APIClient.getClient().create(APIInterface.class).getAllStates(Constants.GET_ALL_STATES);

        signUpModelCall.enqueue(new Callback<StatesListModel>() {
            @Override
            public void onResponse(@NonNull Call<StatesListModel> call, @NonNull Response<StatesListModel> response) {
                progressDialog.dismiss();
                StatesListModel statesListModel = response.body();
                assert statesListModel != null;
                if (statesListModel.status.equalsIgnoreCase("success")) {
                    for (int i = 0; i < statesListModel.userDataClass.statesDataArrayList.size(); i++) {
                        stateslist.add(statesListModel.userDataClass.statesDataArrayList.get(i).Name);
                        statesId.add(statesListModel.userDataClass.statesDataArrayList.get(i).StateID);
                        isRestricteed.add(statesListModel.userDataClass.statesDataArrayList.get(i).IsRestricted);
                        if (Pref.getValue(mActivity, Constants.State, "", Constants.FILENAME).equalsIgnoreCase("")) {
                            mTextSelectState.setHint("Select a State");
                            mTextSelectState.setEnabled(true);
                            mImageSelectState.setEnabled(true);
                        } else {
                            mTextSelectState.setText(Pref.getValue(mActivity, Constants.State, "", Constants.FILENAME));
                            mTextSelectState.setEnabled(false);
                            mImageSelectState.setEnabled(false);
                        }
                    }

                } else {
                    Constants.SnakeMessageYellow(mRelativeMain, statesListModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<StatesListModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void validate() throws IOException {
       /* if (pickResult==null)
        {
            Toast.makeText(this,"Select image of Profile image.",Toast.LENGTH_SHORT).show();
        }*/
        int position = stateslist.indexOf(mTextSelectState.getText().toString());
        if (mEditFirstName.getText().length() < 3)

            Constants.SnakeMessageYellow(mRelativeMain, "Enter valid first name");

        else if (mEditLastName.getText().length() < 3)
            Constants.SnakeMessageYellow(mRelativeMain, "Enter valid last name.");

        else if (mTextSelectState.getText().toString().isEmpty())
            Constants.SnakeMessageYellow(mRelativeMain, "Select State");

        /*else if (isRestricteed.get(position).equalsIgnoreCase("2"))
             Constants.SnakeMessageYellow(mRelativeMain,getResources().getString(R.string.restricted_states_msg));*/

         /*else if(et_pubg_name.getText().length()<3)
        {
            Constants.SnakeMessageYellow(mRelativeMain,"Enter PUBG name");
        }
        else if(et_cr_name.getText().length()<3)
        {
            Constants.SnakeMessageYellow(mRelativeMain,"Enter Clash Royal name");
        }*/
        else {
            sendToServer();
        }
    }

    private void sendToServer() throws IOException {
        final ProgressDialog progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        File file = null;
        if (pickResult != null) {
            Common.insertLog("Filepath NEW:::> " + pickResult.getPath());
            file = new File(pickResult.getPath());
            OutputStream os = null;
            try {
                os = new BufferedOutputStream(new FileOutputStream(file));
                pickResult.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, os);
                os.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        RequestParams requestParams = new RequestParams();
        try {
            Common.insertLog("Filepath:::> " + file);
            requestParams.put("Action", Constants.UPDATE_USER_PROFILE);
            requestParams.put("Val_Userid", Pref.getValue(mActivity, Constants.UserID, "", Constants.FILENAME));
            if (pickResult != null)
                requestParams.put("Val_Uprofileimage", file);
            requestParams.put("Val_Ufirstname", mEditFirstName.getText().toString());
            requestParams.put("Val_Ulastname", mEditLastName.getText().toString());

            requestParams.put("Val_Ubirthdate", dob);
            requestParams.put("Val_Ustate", statesId.get(stateslist.indexOf(mTextSelectState.getText().toString())));
            requestParams.put("Val_Uemailaddress", mEditEmail.getText().toString());
            Common.insertLog("Request:::> " + requestParams.toString());
            editable = false;
            mTextEdit.setText("EDIT");

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            asyncHttpClient.setTimeout(40 * 1000);
            asyncHttpClient.post(mActivity, BuildConfig.BASE_URL + Constants.UPDATE_PROFILE_V3, requestParams, new JsonHttpResponseHandler() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Common.insertLog("Response:::> " + response.toString());
                    progressDialog.dismiss();
                    if (response.optString("status").equalsIgnoreCase("success")) {

                        Pref.setValue(mActivity, Constants.FullName, response.optJSONObject("data").optString("FullName"), Constants.FILENAME);
                        Pref.setValue(mActivity, Constants.TeamName, response.optJSONObject("data").optString("TeamName"), Constants.FILENAME);
                        Pref.setValue(mActivity, Constants.firstname, response.optJSONObject("data").optString("FirstName"), Constants.FILENAME);
                        Pref.setValue(mActivity, Constants.lastname, response.optJSONObject("data").optString("LastName"), Constants.FILENAME);
                        Pref.setValue(mActivity, Constants.CountryCode, response.optJSONObject("data").optString("CountryCode"), Constants.FILENAME);
                        Pref.setValue(mActivity, Constants.EmailAddress, response.optJSONObject("data").optString("EmailAddress"), Constants.FILENAME);
                        Pref.setValue(mActivity, Constants.MobileNumber, response.optJSONObject("data").optString("MobileNumber"), Constants.FILENAME);
                        Pref.setValue(mActivity, Constants.State, response.optJSONObject("data").optString("State"), Constants.FILENAME);
                        Pref.setValue(mActivity, Constants.BirthDate, mTextSelectDateOfBirth.getText().toString(), Constants.FILENAME);
                        Pref.setValue(mActivity, Constants.ProfileImage, response.optJSONObject("data").optString("ProfileImage"), Constants.FILENAME);
                        Glide.with(mActivity).load(Pref.getValue(mActivity, Constants.ProfileImage, "", Constants.FILENAME)).into(mImageProfilePic);
                        mTextUserName.setText(Pref.getValue(mActivity, Constants.firstname, response.optJSONObject("data").optString("FirstName"), Constants.FILENAME) + " " +
                                Pref.getValue(mActivity, Constants.lastname, response.optJSONObject("data").optString("LastName"), Constants.FILENAME));
                        setDisableEditText();

                        Constants.SnakeMessageYellow(mRelativeMain, response.optString("message"));
                        Constants.getUserData(mActivity);
                    } else {
                        Constants.SnakeMessageYellow(mRelativeMain, response.optString("message"));
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(mRelativeMain, "Something went wrong.Please try again");
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(mRelativeMain, "Something went wrong.Please try again");
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(mRelativeMain, "Something went wrong.Please try again");
                    super.onFailure(statusCode, headers, responseString, throwable);
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Constants.SnakeMessageYellow(mRelativeMain, "Something went wrong.Please try again");
        }
    }

    private void setEnable() {
        mEditFirstName.setEnabled(true);
        mEditLastName.setEnabled(true);
        mTextSelectState.setEnabled(true);
        mImageSelectState.setEnabled(true);
        mImageProfilePic.setClickable(true);
        if (Pref.getValue(mActivity, Constants.EmailAddress, "", Constants.FILENAME).isEmpty())
            mEditEmail.setEnabled(true);

        mTextSelectDateOfBirth.setEnabled(true);
        mImageSelectDateOfBirth.setEnabled(true);
        mImageProfilePicEdit.setVisibility(View.VISIBLE);
    }

    private void callToEditProgileAPI() {
        if (editable) {
            //gameUserNameAdapter.disableEdittext();
            try {
                validate();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            editable = true;
            setEnable();
            mTextEdit.setText("SAVE");
        }
    }
}