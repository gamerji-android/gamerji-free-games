package esports.gamerji.com.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import esports.gamerji.com.R;
import esports.gamerji.com.adapter.SearchResultAdapter;
import esports.gamerji.com.model.SearchModel;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.Pref;
import esports.gamerji.com.webServices.APIClient;
import esports.gamerji.com.webServices.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeaderBoardFragment extends Fragment {
    private View mView;
    private Activity mActivity;
    APIInterface apiInterface;
    TextView txt_title;
    ImageView img_search;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    View bg;
    public LinearLayout parent_layout;
    RelativeLayout lyt_search_et;
    LinearLayout lyt_search;
    EditText et_search;
    RecyclerView rv_search;
    private SearchResultAdapter searchResultAdapter;
    boolean isSearch = false;
    private InterstitialAd interstitialAd;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.new_leaderboard, container, false);
        mActivity = getActivity();
        interstitialAd = new InterstitialAd(mActivity, getString(R.string.fb_interstrial_full_screen_ad_id));
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {

            }

            @Override
            public void onInterstitialDismissed(Ad ad) {

            }

            @Override
            public void onError(Ad ad, AdError adError) {

            }

            @Override
            public void onAdLoaded(Ad ad) {
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });
        interstitialAd.loadAd();

        viewPager = mView.findViewById(R.id.viewPager);
        parent_layout = mView.findViewById(R.id.parent_layout);
        img_search = mView.findViewById(R.id.img_search);
        txt_title = mView.findViewById(R.id.txt_title);
        tabLayout = mView.findViewById(R.id.tabLayout);
        lyt_search_et = mView.findViewById(R.id.lyt_search_et);
        lyt_search = mView.findViewById(R.id.lyt_search);
        et_search = mView.findViewById(R.id.et_search);
        rv_search = mView.findViewById(R.id.rv_search);
        rv_search.setLayoutManager(new LinearLayoutManager(mActivity));
        rv_search.setItemAnimator(new DefaultItemAnimator());
        setupViewPager(viewPager);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(ColorStateList.valueOf(Color.WHITE));
        tabLayout.setSelectedTabIndicatorHeight(10);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.orange_color));

        int wantedTabIndex = 0;
        Typeface font = null;
        font = ResourcesCompat.getFont(mActivity, R.font.poppins_semi_bold);
        TextView tv = (TextView) (((LinearLayout) ((LinearLayout) tabLayout.getChildAt(0)).getChildAt(wantedTabIndex)).getChildAt(1));
        tv.setTypeface(font);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        Typeface font = null;
                        font = ResourcesCompat.getFont(mActivity, R.font.poppins_semi_bold);
                        ((TextView) tabViewChild).setTypeface(font);
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        Typeface font = null;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            font = getResources().getFont(R.font.poppins);
                            ((TextView) tabViewChild).setTypeface(font);
                        }
                    }
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        bg = mView.findViewById(R.id.bg);
        /*img_back.setOnClickListener(v ->
        {
            if (isSearch) {
                isSearch = false;
                tabLayout.setVisibility(View.VISIBLE);
                viewPager.setVisibility(View.VISIBLE);
                lyt_search.setVisibility(View.GONE);
                lyt_search_et.setVisibility(View.GONE);
                img_back.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back));
                img_search.setVisibility(View.VISIBLE);
            } else {
                mActivity.finish();
            }
        });*/

        img_search.setOnClickListener(v ->
        {
            tabLayout.setVisibility(View.GONE);
            viewPager.setVisibility(View.GONE);
            lyt_search.setVisibility(View.VISIBLE);
            lyt_search_et.setVisibility(View.VISIBLE);
            img_search.setVisibility(View.GONE);
            isSearch = true;
//            img_back.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_white));
            et_search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    new Handler().postDelayed(() -> getSearchResult(s.toString()), 1500);
                }
            });
        });
        return mView;
    }

    private void getSearchResult(String s) {

        Call<SearchModel> searchModelCall = apiInterface.searchUser(Constants.SEARCH_USER, Pref.getValue(mActivity, Constants.UserID, "", Constants.FILENAME), s);

        searchModelCall.enqueue(new Callback<SearchModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<SearchModel> call, @NonNull Response<SearchModel> response) {
                SearchModel searchModel = response.body();
                assert searchModel != null;
                if (searchModel.status.equalsIgnoreCase("success")) {
                    searchResultAdapter = new SearchResultAdapter(mActivity, searchModel.DataClass.userLevelsData);
                    rv_search.setAdapter(searchResultAdapter);
                } else {
                    Constants.SnakeMessageYellow(parent_layout, searchModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SearchModel> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new FragmentTodayLeaderboard("Today", rv_search), "Today");
        adapter.addFrag(new FragmentByWeekLeaderboard("Weekly", rv_search), "Weekly");
        adapter.addFrag(new FragmentMonthlyLeaderboard("Monthly", rv_search), "Monthly");
        adapter.addFrag(new FragmentAllTimeLeaderboard("All Time", rv_search), "All Time");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }


        //adding fragments and title method
        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    /*@Override
    public void onBackPressed() {
        if (isSearch) {
            isSearch = false;
            tabLayout.setVisibility(View.VISIBLE);
            viewPager.setVisibility(View.VISIBLE);
            lyt_search.setVisibility(View.GONE);
            lyt_search_et.setVisibility(View.GONE);
            img_back.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back));
            img_search.setVisibility(View.VISIBLE);
        } else {
            mActivity.finish();
        }
    }*/

    @Override
    public void onDestroy() {
        if (interstitialAd != null) {
            interstitialAd.destroy();
        }
        super.onDestroy();
    }
}
