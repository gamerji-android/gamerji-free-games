package esports.gamerji.com.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import esports.gamerji.com.R;
import esports.gamerji.com.adapter.HostStreamVideosAdapter;
import esports.gamerji.com.model.HostStreamVideosModel;
import esports.gamerji.com.utils.Common;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.SessionManager;
import esports.gamerji.com.webServices.APIClient;
import esports.gamerji.com.webServices.APICommonMethods;
import esports.gamerji.com.webServices.APIInterface;
import esports.gamerji.com.webServices.WebFields;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HostStreamVideosFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private RelativeLayout mRelativeMain;
    private SwipeRefreshLayout mSwipeRefreshView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private ArrayList<HostStreamVideosModel.Data.VideosData> mArrHostStreamVideos;
    private HostStreamVideosAdapter mAdapterHostStreamVideos;
    private SessionManager mSessionManager;

    // Load More Listener Variables
    private int pageIndex = 5, page = 1, pastVisibleItems, visibleItemCount, totalItemCount;
    private boolean isLastPage = false, isLoading;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_host_stream_videos, container, false);
        mActivity = getActivity();
        mSessionManager = new SessionManager();
        mArrHostStreamVideos = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);

        getIds();
        setRegListeners();
        setAdapterData();
        callToVideosAPI(page);
        return mView;
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layout
            mRelativeMain = mView.findViewById(R.id.relative_host_stream_videos_main_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view_host_stream_videos);

            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view_host_stream_videos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            page = 1;
            isLastPage = false;
            if (mArrHostStreamVideos != null)
                mArrHostStreamVideos.clear();
            if (mAdapterHostStreamVideos != null)
                mAdapterHostStreamVideos.notifyDataSetChanged();
            callToVideosAPI(page);
        }
    };

    /**
     * This should get all the videos of the Influencer as well as Host
     */
    private void callToVideosAPI(int page) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(mActivity);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setGetVideosJson(mSessionManager.getUserId(mActivity), Constants.VIDEOS_HOST_STREAMS, page, pageIndex));

            Call<HostStreamVideosModel> call = APIClient.getClient().create(APIInterface.class).getHostStreamVideos(body);
            call.enqueue(new Callback<HostStreamVideosModel>() {
                @Override
                public void onResponse(@NonNull Call<HostStreamVideosModel> call, @NonNull Response<HostStreamVideosModel> response) {
                    isLoading = false;
                    mSwipeRefreshView.setRefreshing(false);
                    progressDialog.dismiss();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);

                        String mStatus = jsonObject.getString(WebFields.STATUS);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);

                        if (mStatus.equalsIgnoreCase(Constants.API_SUCCESS)) {

                            ArrayList<HostStreamVideosModel.Data.VideosData> videos = (ArrayList<HostStreamVideosModel.Data.VideosData>) response.body().getData().getVideosData();

                            if (response.body().getData().getVideosCount() > 0) {
                                if (mArrHostStreamVideos != null && mArrHostStreamVideos.size() > 0 &&
                                        mAdapterHostStreamVideos != null) {
                                    mArrHostStreamVideos.addAll(videos);
                                    mAdapterHostStreamVideos.notifyDataSetChanged();
                                } else {
                                    mArrHostStreamVideos = videos;
                                    setAdapterData();
                                    setLoadMoreClickListener();
                                }
                            } else {
                                isLastPage = true;
                            }
                        } else {
//                            Constants.SnakeMessageYellow(mRelativeMain, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<HostStreamVideosModel> call, @NonNull Throwable t) {
                    mSwipeRefreshView.setRefreshing(false);
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());

            mAdapterHostStreamVideos = new HostStreamVideosAdapter(mActivity, mArrHostStreamVideos);
            mRecyclerView.setAdapter(mAdapterHostStreamVideos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!isLastPage && !isLoading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            isLoading = true;
                            Common.insertLog("Last Item Wow !");
                            callToVideosAPI(++page);
                        }
                    }
                }
            }
        });
    }
}
