package esports.gamerji.com.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import esports.gamerji.com.BuildConfig;
import esports.gamerji.com.R;
import esports.gamerji.com.activity.ActivityCustomerCareTicket;
import esports.gamerji.com.activity.ActivityTermsConditions;
import esports.gamerji.com.activity.Activity_Login;
import esports.gamerji.com.activity.AddStreamerActivity;
import esports.gamerji.com.activity.AddVideosActivity;
import esports.gamerji.com.activity.ReferAFriendActivity;
import esports.gamerji.com.model.GetStreamerStatusModel;
import esports.gamerji.com.utils.AppCallbackListener;
import esports.gamerji.com.utils.AppConstants;
import esports.gamerji.com.utils.AppUtil;
import esports.gamerji.com.utils.Common;
import esports.gamerji.com.utils.Constants;
import esports.gamerji.com.utils.Pref;
import esports.gamerji.com.utils.SessionManager;
import esports.gamerji.com.webServices.APIClient;
import esports.gamerji.com.webServices.APICommonMethods;
import esports.gamerji.com.webServices.APIInterface;
import esports.gamerji.com.webServices.WebFields;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoreFragment extends Fragment implements AppCallbackListener.CallBackListener {

    private View mView;
    private Activity mActivity;
    private AppUtil mAppUtils;
    private SwipeRefreshLayout mSwipeRefresh;
    private LinearLayout mLinearStreamer, mLinearReferAFriend, mLinearCustomerCare, mLinearLegality, mLinearMoreArrow;
    private RelativeLayout mRelativeMainView, mRelativeLegalityArrow;
    private TextView mTextStreamer, mTextTermsAndConditions, mTextPrivacy, mTextVersionName;
    private ImageView mImageLegalityArrow;
    private RelativeLayout mRelativeLogout;
    private int mStreamerStatus;
    private String mStatus, mStreamerReason, mStreamerDisplayStatus, mStreamName;
    private SessionManager mSessionManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_more, container, false);
        mActivity = getActivity();
        mAppUtils = new AppUtil(mActivity);
        mSessionManager = new SessionManager();

        getIds();
        setRegListeners();
        setData();
        return mView;
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Swipe Refresh Layout
            mSwipeRefresh = mView.findViewById(R.id.swipe_refresh_view_more);

            // Linear Layouts
            mLinearStreamer = mView.findViewById(R.id.linear_more_streamer);
            mLinearReferAFriend = mView.findViewById(R.id.linear_more_refer_a_friend);
            mLinearCustomerCare = mView.findViewById(R.id.linear_more_customer_care);
            mLinearLegality = mView.findViewById(R.id.linear_more_legality);
            mLinearMoreArrow = mView.findViewById(R.id.linear_more_legality_options);

            // Relative Layout
            mRelativeMainView = mView.findViewById(R.id.relative_more_main_view);
            mRelativeLogout = mView.findViewById(R.id.relative_more_logout);

            // Relative Layouts
            mRelativeLegalityArrow = mView.findViewById(R.id.relative_more_legality_arrow);

            // Image View
            mImageLegalityArrow = mView.findViewById(R.id.image_more_legality_arrow);

            // Text Views
            mTextStreamer = mView.findViewById(R.id.text_more_streamer);
            mTextTermsAndConditions = mView.findViewById(R.id.text_more_terms_and_conditions);
            mTextPrivacy = mView.findViewById(R.id.text_more_privacy);
            mTextVersionName = mView.findViewById(R.id.text_more_version_name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Swipe Refresh Click Listener
            mSwipeRefresh.setOnRefreshListener(this::callToGetStreamerStatusAPI);

            // ToDo: On Click Listener
            mLinearStreamer.setOnClickListener(clickListener);
            mLinearReferAFriend.setOnClickListener(clickListener);
            mLinearCustomerCare.setOnClickListener(clickListener);
            mLinearLegality.setOnClickListener(clickListener);
            mRelativeLegalityArrow.setOnClickListener(clickListener);
            mLinearCustomerCare.setOnClickListener(clickListener);
            mTextTermsAndConditions.setOnClickListener(clickListener);
            mTextPrivacy.setOnClickListener(clickListener);
            mRelativeLogout.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            mStreamName = mActivity.getResources().getString(R.string.text_stream_on) + " " + mActivity.getResources().getString(R.string.app_name);
            mTextVersionName.setText(mActivity.getResources().getString(R.string.text_version_name) + " " + BuildConfig.VERSION_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.linear_more_streamer:
                    callToAddStreamerActivity();
                    break;

                case R.id.linear_more_refer_a_friend:
                    callToReferAFriendActivity();
                    break;

                case R.id.linear_more_customer_care:
                    callToCustomerCareActivity();
                    break;

                case R.id.linear_more_legality:
                    callToShowHideLegality();
                    break;

                case R.id.relative_more_legality_arrow:
                    callToShowHideLegality();
                    break;

                case R.id.text_more_terms_and_conditions:
                    callToMoreLegality(mActivity.getResources().getString(R.string.terms_and_conditions));
                    break;

                case R.id.text_more_privacy:
                    callToMoreLegality(mActivity.getResources().getString(R.string.privacy_policy));
                    break;

                case R.id.relative_more_logout:
                    openLogoutPopup();
                    break;
            }
        }
    };

    /**
     * This method redirects you to the Add Streamer activity
     */
    private void callToAddStreamerActivity() {
        try {
            if (mTextStreamer.getText().toString().equalsIgnoreCase(mStreamName)) {
                Intent intent = new Intent(mActivity, AddStreamerActivity.class);
                intent.putExtra(AppConstants.BUNDLE_STREAM_NAME, mStreamName);
                intent.putExtra(AppConstants.BUNDLE_STATUS, mStatus);
                intent.putExtra(AppConstants.BUNDLE_STREAMER_STATUS, mStreamerStatus);
                intent.putExtra(AppConstants.BUNDLE_STREAMER_REASON, mStreamerReason);
                intent.putExtra(AppConstants.BUNDLE_STREAMER_DISPLAY_STATUS, mStreamerDisplayStatus);
                startActivity(intent);
            } else {
                Intent intent = new Intent(mActivity, AddVideosActivity.class);
                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirects you to the Refer A Friend activity
     */
    private void callToReferAFriendActivity() {
        try {
            Intent intent = new Intent(mActivity, ReferAFriendActivity.class);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirects you to the Customer Care activity
     */
    private void callToCustomerCareActivity() {
        try {
            Intent intent = new Intent(mActivity, ActivityCustomerCareTicket.class);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should show and hide legality view
     */
    private void callToShowHideLegality() {
        try {
            if (mLinearMoreArrow.getVisibility() == View.VISIBLE) {
                mLinearMoreArrow.animate().setDuration(1000);
                new Handler().postDelayed(() ->
                {
                    mLinearMoreArrow.setVisibility(View.GONE);
                }, 100);
                mImageLegalityArrow.setImageResource(R.drawable.ic_next_arrow);
            } else {
                mLinearMoreArrow.setVisibility(View.VISIBLE);
                mImageLegalityArrow.setImageResource(R.drawable.ic_down_arrow);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirects you to the Terms And Conditions activity
     */
    private void callToMoreLegality(String mIsFrom) {
        try {
            Intent intent = new Intent(mActivity, ActivityTermsConditions.class);
            intent.putExtra(mActivity.getResources().getString(R.string.text_from), mIsFrom);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should open the logout popup and on click of OK it should be redirected to the Login activity and
     * on click of OK to dismiss the logout popup.
     */
    private void openLogoutPopup() {
        try {
            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_logout);

            Window window = dialog.getWindow();
            window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

            // Linear Layouts
            LinearLayout mLinearCancel = dialog.findViewById(R.id.linear_dialog_logout_cancel);
            LinearLayout mLinearOK = dialog.findViewById(R.id.linear_dialog_logout_ok);

            // ToDo: Cancel Button On Click Listener
            mLinearCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });

            // ToDo: OK Button On Click Listener
            mLinearOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(mActivity, Activity_Login.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    Pref.ClearAllPref(mActivity, Constants.FILENAME);
                    startActivity(i);
                    mActivity.finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mLinearMoreArrow.getVisibility() == View.VISIBLE) {
            mLinearMoreArrow.setVisibility(View.GONE);
            mImageLegalityArrow.setImageResource(R.drawable.ic_next_arrow);
        }
        checkForNetwork();
    }

    /**
     * This should check for the network and call the API if network is available.
     */
    private void checkForNetwork() {
        try {
            if (!mAppUtils.getConnectionState()) {
                mLinearStreamer.setVisibility(View.GONE);
                mAppUtils.displayNoInternetSnackBar(mRelativeMainView, new AppCallbackListener(this));
            } else {
                callToGetStreamerStatusAPI();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should get the Streamer Status
     */
    private void callToGetStreamerStatusAPI() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(mActivity);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setGetStreamerStatusJson(mSessionManager.getUserId(mActivity)));

            Call<GetStreamerStatusModel> call = APIClient.getClient().create(APIInterface.class).getStreamerStatus(body);
            call.enqueue(new Callback<GetStreamerStatusModel>() {
                @Override
                public void onResponse(@NonNull Call<GetStreamerStatusModel> call, @NonNull Response<GetStreamerStatusModel> response) {

                    progressDialog.dismiss();
                    if (mSwipeRefresh.isRefreshing()) {
                        mSwipeRefresh.setRefreshing(false);
                    }
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);

                        mStatus = jsonObject.getString(WebFields.STATUS);

                        assert response.body() != null;
                        if (mStatus.equalsIgnoreCase(Constants.API_SUCCESS)) {
                            mLinearStreamer.setVisibility(View.VISIBLE);
                            JSONObject jsonData = jsonObject.getJSONObject(WebFields.DATA);
                            mStreamerReason = jsonData.getString(WebFields.GET_STREAMER_STATUS.RESPONSE_REASON);
                            mStreamerStatus = jsonData.getInt(WebFields.GET_STREAMER_STATUS.RESPONSE_STATUS);
                            mStreamerDisplayStatus = jsonData.getString(WebFields.GET_STREAMER_STATUS.RESPONSE_DISPLAY_STATUS);

                            if (mStreamerStatus == 2) {
                                mTextStreamer.setText(mActivity.getResources().getString(R.string.text_add_videos));
                            } else {
                                mTextStreamer.setText(mStreamName);
                            }
                        } else {
                            mLinearStreamer.setVisibility(View.VISIBLE);
                            mTextStreamer.setText(mStreamName);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        mLinearStreamer.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetStreamerStatusModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                    mLinearStreamer.setVisibility(View.GONE);
                    if (mSwipeRefresh.isRefreshing()) {
                        mSwipeRefresh.setRefreshing(false);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAppCallback(int Code) {
        checkForNetwork();
    }
}
